#include "ers/ers.h"
#include "ResourceManager/RM_Client.h"
#include "src/RM_ClientImpl.h"

 daq::rmgr::RM_Client::RM_Client() {
  impl = new RM_ClientImpl();
 }
 daq::rmgr::RM_Client::RM_Client(const char *sid) {
  impl = new RM_ClientImpl(sid);
 }
 daq::rmgr::RM_Client::~RM_Client() {
  delete impl;
 }
 void daq::rmgr::RM_Client::rmConfigurationUpdate ( const std::string& partition ) {
   impl->rmConfigurationUpdate( partition );
 }
 long daq::rmgr::RM_Client::requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, //id in confdb
                        const std::string& computer,
                        const std::string& user,
                        const std::string& application,
                        unsigned long pid) {
   return impl->requestResources( partition, swobjectid, computer, user, application, pid );
 }
//AI 030624
void daq::rmgr::RM_Client::setProcess( long handle,  unsigned long pid) {
    impl->setProcess( handle, pid );
}
/*AI 030624 240125*/
 long daq::rmgr::RM_Client::requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, //id in confdb
                        const std::string& computerName, // name in confdb
                        unsigned long mypid,
                        const std::string& clientName) {
   //return impl->requestResources( partition, swobjectid, computerName, mypid, clientName );
   return impl->requestResources( partition, swobjectid, computerName, clientName, "", mypid);
 }

 long daq::rmgr::RM_Client::requestResourceForMyProcess(
                        const std::string& partition,
                        const std::string& resource,
                        const std::string& computer,
                       const std::string& clientName) {
  return impl->requestResourceForMyProcess( partition, resource, computer, clientName );
 }
 long daq::rmgr::RM_Client::requestResource( const std::string& partition, const std::string& resource, const std::string& computer, const std::string& clientName, int process_id ) {
  return impl->requestResource( partition, resource, computer, clientName, process_id );
 }

 void daq::rmgr::RM_Client::freeResources (long handleId) { //, bool log_this_call) {
  impl->freeResources( handleId); //, log_this_call );
 }
// AI 030824 was deprecated 240125 UNCOMMENTED
 void daq::rmgr::RM_Client::freePartition(const std::string&  p ) {
  impl->freeAllInPartition( p );
 }

 void daq::rmgr::RM_Client::freeAllInPartition(const std::string&  p ) {
  impl->freeAllInPartition( p );
 }


 void daq::rmgr::RM_Client::freeAllOnComputer(const std::string& computer) { //, bool log_this_call) {
  impl->freeAllOnComputer( computer ); //, log_this_call );
 }

 void daq::rmgr::RM_Client::freeResource(const std::string& resource) { //, bool log_this_call) {
  impl->freeResource( resource); //, log_this_call );
 }
 void daq::rmgr::RM_Client::freePartitionResource(const std::string&  p, const std::string& resource) { //, bool log_this_call) {
  impl->freePartitionResource( p, resource); //, log_this_call );
 }
 void daq::rmgr::RM_Client::freeComputerResource(const std::string& resource, const std::string& computer, const std::string&  p) { //, bool log_this_call) {
  impl->freeComputerResource( resource, computer, p ); //, log_this_call );
 }
 void daq::rmgr::RM_Client::freeApplicationResources(const std::string& p, const std::string& swObj, const std::string& computer) { //, bool log_this_call) {
  impl->freeApplicationResources( p, swObj, computer); //, log_this_call );
 }
 void daq::rmgr::RM_Client::freeAllProcessResources( long handleId, 
		 //AI 030624 removed const std::string& partition,
                           const std::string& computer, unsigned long pid) { //, bool log_this_call ) {
  impl->freeAllProcessResources(handleId, /*partition,*/  computer, pid ); //, log_this_call );
 }
 void daq::rmgr::RM_Client::freeProcessResources(
                        const std::string& partition,
                        const std::string& computer,
                        unsigned long pid) {
  impl->freeProcessResources( partition, computer, pid); //, log_this_call );
 }
 std::vector<std::string>  daq::rmgr::RM_Client::getPartitionsList() {
  return impl->getPartitionsList();
 }
 std::vector<std::string>  daq::rmgr::RM_Client::getResourceOwners(const std::string& rname, const std::string& p) {
  return impl->getResourceOwners( rname, p );
 }
 const daq::rmgr::HandleInfo daq::rmgr::RM_Client::getHandleInfo(long handleId) {
   return impl->getHandleInfo( handleId );
 }

 const daq::rmgr::AllocatedInfo daq::rmgr::RM_Client::getPartitionInfo( const std::string& partition ) {
   return impl->getPartitionInfo( partition );
 }

 const daq::rmgr::AllocatedInfo daq::rmgr::RM_Client::getResourceInfo( const std::string& partition, const std::string& rname) {
  return impl->getResourceInfo(partition, rname);
 }
 const daq::rmgr::AllocatedInfo daq::rmgr::RM_Client::getFullResInfo(const std::string& resource) {
  return impl->getFullResInfo( resource );
 }
 const daq::rmgr::AllocatedInfo daq::rmgr::RM_Client::getComputerInfo(const std::string& computer) {
  return impl->getComputerInfo( computer );
 }
 const daq::rmgr::AllocatedInfo daq::rmgr::RM_Client::getComputerResourceInfo(const std::string& computer, const std::string& partition, const std::string& resource) {
  return impl->getComputerResourceInfo(computer, partition, resource);
 }
const daq::rmgr::ConfigUpdate daq::rmgr::RM_Client::getConfigInfo() {
  return impl->getConfigInfo();
 }


