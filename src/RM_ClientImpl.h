#ifndef RM_ClientImpl_H_INCLUDED
#define RM_ClientImpl_H_INCLUDED

#include "ipc/object.h"
#include "ipc/partition.h"
#include "src/idlhelper.h"
#include <ResourceManager/exceptions.h>

#include "daq_tokens/acquire.h"
#include "daq_tokens/verify.h"

#include "config/Configuration.h"
#include "config/ConfigObject.h"

#include <rmgr/rmgr.hh>

namespace daq   {

namespace rmgr  {

enum RequestType {FreeAllInPartition, FreeAllOnComputer, FreeResource, FreePartitionResource, FreeComputerResource, FreeApplicationResources};

class ClientRequest
{
public:
RequestType type;
const char* param1;
const char* param2;
const char* param3;
//bool _log_this_call;
bool _useDaqToken;

ClientRequest(RequestType tp, const char* par1,/* bool log_this_call,*/  bool useDaqToken = false) : type(tp), param1(par1), param2(0), param3(0), /*_log_this_call(log_this_call),*/ _useDaqToken(useDaqToken)  {}
ClientRequest( RequestType tp, const char* par1, const char* par2,/* bool log_this_call,*/  bool useDaqToken = false) : type(tp), param1(par1), param2(par2), param3(0), /*_log_this_call(log_this_call),*/ _useDaqToken(useDaqToken) {}
ClientRequest(RequestType tp, const char* par1, const char* par2, const char* par3, /*bool log_this_call,*/ bool useDaqToken = false) : type(tp), param1(par1), param2(par2), param3(par3), /*_log_this_call(log_this_call),*/ _useDaqToken(useDaqToken) {}

};


class RM_ClientImpl {
protected:
  ::rmgr::ResMgr_var RMDB;
  ::IPCPartition m_defpart;
  std::string m_rmDbName;

  /**
  *  \throw daq::rmgr::CommunicationException
  */
  void lookupRMDDB();
public:
 RM_ClientImpl();
 RM_ClientImpl(const char *sid);
 void rmConfigurationUpdate ( const std::string& partition );
  long requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, //id in confdb
                        const std::string& computer,
                        const std::string& user,
                        const std::string& application,
                        unsigned long pid);
  void setProcess( long handle,  unsigned long pid); //AI 030624
/*AI 030624
 long requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, //id in confdb
                        const std::string& computerName,
                        unsigned long mypid,
                        const std::string& clientName = "NO");
*/
 long requestResourceForMyProcess(
                        const std::string& partition,
                        const std::string& resource,
                        const std::string& computer,
                       const std::string& clientName = "NO");
 long requestResource( const std::string& partition, const std::string& resource, const std::string& computer, const std::string& clientName, int process_id );
 void freeResources (long handleId); //, bool log_this_call=true);
 void freeAllInPartition(const std::string&  p);
 void freeAllOnComputer(const std::string& computer); //, bool log_this_call=true);
 void freeResource(const std::string& resource); //, bool log_this_call=true);
 void freePartitionResource(const std::string&  p, const std::string& resource); //, bool log_this_call=true);
 void freeComputerResource(const std::string& resource, const std::string& computer, const std::string&  p); //, bool log_this_call=true);
 void freeApplicationResources(const std::string& p, const std::string& swObj, const std::string& computer); //, bool log_this_call=true);
 void freeAllProcessResources( long handleId, 
		           //AI 030624 const std::string& partition,
                           const std::string& computer, unsigned long pid); //, bool log_this_call=true );
 void freeProcessResources(
                        const std::string& partition,
                        const std::string& computer,
                        unsigned long pid); //, bool log_this_call=true);
 std::vector<std::string> getPartitionsList();
 std::vector<std::string> getResourceOwners(const std::string& rname, const std::string& p);
 daq::rmgr::HandleInfo getHandleInfo(long handleId);
 daq::rmgr::AllocatedInfo getPartitionInfo( const std::string& partition );
 daq::rmgr::AllocatedInfo getResourceInfo( const std::string& partition, const std::string& rname);
 daq::rmgr::AllocatedInfo getFullResInfo(const std::string& resource);
 daq::rmgr::AllocatedInfo  getComputerInfo(const std::string& computer);

const daq::rmgr::AllocatedInfo getComputerResourceInfo(const std::string& computer, const std::string& partition, const std::string& resource);

 void rmConfigurationUpdate4MyTest ();
 daq::rmgr::ConfigUpdate getConfigInfo();

Configuration* getConfiguration( const std::string& dbname );
void setRMData(::Configuration * conf, daq::rmgr::Config_update & rm_conf);
void performRequest(ClientRequest& req);
private:
daq::rmgr::CallerInfo getCallerInfo( bool useDaqTok = false );
daq::rmgr::ResOwnerInfo getResOwnerInfo( const std::string& user, const std::string& host, unsigned long pid, const std::string& application, const std::string& partition); //AI 030624
 bool checkAccesPermission();
};

} // namespace rm
} // namespace daq


#endif
