#include <boost/spirit/include/karma.hpp>

#include <config/Configuration.h>
#include <ers2idl/ers2idl.h>
#include <pmg/pmg_initSync.h>

#include "src/RMimpl.h"
#include "src/RM_Server.h"
#include "src/idlhelper.h"

daq::rmgr::RM_Server::RM_Server( const std::string dbname) : destroyInProgress(false)
{
     const std::string backup((std::string)RMSERVERDEFAULT + (std::string)RMLOGNAME);
     RM_Backup* bck = new RM_Backup( backup.c_str(), true, true );
     bck->checkBackup();
     rm_impl = new  RM_Impl(bck, dbname);
     rm_impl->isConfigAvailable(); //throws exception if unavailable
     bck->initialize( rm_impl );
     idlRapper = new RmgrImpl(RMSERVERDEFAULT, rm_impl);
}

daq::rmgr::RM_Server::RM_Server( const std::string dbname, const char * sid, const char* backup, bool doBck, bool doresto ) : destroyInProgress(false)
{
      RM_Backup* bck = new RM_Backup( backup, doBck, doresto );
      bck->checkBackup();
      rm_impl = new  RM_Impl(bck, dbname);
      rm_impl->isConfigAvailable(); //throws exception if unavailable
      bck->initialize( rm_impl );
      idlRapper = new RmgrImpl(sid, rm_impl);
}

daq::rmgr::RM_Server::~RM_Server() {
}

void daq::rmgr::RM_Server::run( bool pmgsync) {
  IPCPartition defpart;
  if ( defpart.isObjectValid< ::rmgr::ResMgr> ( getServerName() ) == false ) {
     std::ostringstream txt1;
     txt1<<"Resource Manager with name=" << getServerName() << " is starting" ;
     if( rm_impl ) {
        if(rm_impl->isRestoreBackup()) {
           if( rm_impl->restoreDone() ) {
              txt1<<" with restore from " << rm_impl->getBckPath();
              if(rm_impl->isDoBackup())
                  txt1<<" Save backup with the same path.";
           }
           else if( rm_impl->isDoBackup() )
              txt1<<"  backup path=" << rm_impl->getBckPath();
        }
        else if(rm_impl->isDoBackup())
            txt1<<"  backup path=" << rm_impl->getBckPath();
        rm_impl->rmConfigurationUpdate(); //throws exception if config problem
     }
     ers::log(ers::Message( ERS_HERE, txt1.str())) ;
     idlRapper->logConfigInfo();

     idlRapper->publish();
     std::ostringstream txt2;
     txt2<<"Resource Manager server with name " << getServerName() << " was started in default partition."<<std::endl;
     ers::info( ers::Message( ERS_HERE, txt2.str()));
     destroyInProgress = false;
     if( pmgsync )
	     pmg_initSync();

     int sig = daq::ipc::signal::wait_for();
     ERS_LOG("Signal " << sig << " received, stopping RM_Server with name '" << getServerName() << "'.");
     removeServ();
     destroyInProgress = true;
     delete rm_impl;
     idlRapper->_destroy(true);
     ers::info( ers::Message( ERS_HERE, "Resource manager server stopped."));
  }
  else {
     ers::fatal( daq::rmgr::ServerAlreadyRunning( ERS_HERE, getServerName() ) );
  }
}


void daq::rmgr::RM_Server::removeServ() {
  try {
   idlRapper->withdraw();
  }
  catch( daq::ipc::Exception & ex ) {
     ers::error( daq::rmgr::IPCException( ERS_HERE, " RM_server withdraw problem", ex ) );
  }
}

daq::rmgr::RmgrImpl::RmgrImpl() : RMIPCNamedObject(RM_Server::RMSERVERDEFAULT) {
  rm_impl = 0;
}

daq::rmgr::RmgrImpl::~RmgrImpl() {
  rm_impl = 0;
}

daq::rmgr::RmgrImpl::RmgrImpl(std::string serverName, RM_Impl* rmimpl)  : RMIPCNamedObject(serverName), _serverName(serverName) {
   rm_impl = rmimpl;
}

void daq::rmgr::RmgrImpl::logConfigInfo() {
     const std::string cinfo = getCInfoAsString( rm_impl->getConfigInfo() );
     ers::log( ers::Message( ERS_HERE, "RM_Server loaded configuration from \"" + rm_impl->getConfDBName() +"\":\n" + cinfo ) );
}

void daq::rmgr::RmgrImpl::rmConfigUpdate ( const char* partition, const char* user ) {
  std::scoped_lock scoped_lock( m_mutex );  //for saveBackup
 //save config for any case in order users can see content if any needs
 std::ostringstream text; //for info log
 const std::string config_before = getCInfoAsString( rm_impl->getConfigInfo() );

 ers::info( ers::Message( ERS_HERE, "Configuration update initiated by " + static_cast<const std::string>(user) +" from partition " + static_cast<const std::string>(partition) ));
 try{
    Configuration conf( rm_impl->getConfDBName() );

   // std::unique_lock<std::shared_mutex> lock(so_mutex);
    {
     // std::scoped_lock scoped_lock( m_mutex ); //use single
      rm_impl->rmConfigurationUpdate( &conf );
      if(rm_impl->isDoBackup())
         rm_impl->getBck()->saveBackup();
    }
    ers::info( ers::Message( ERS_HERE, "Configuration successfully reloaded") );
 }
 catch (const daq::config::Exception & ex) {
    ers::error( ers::Message( ERS_HERE, "Configuration update initiated by " + static_cast<const std::string>(user) +" from partition " + static_cast<const std::string>(partition) + " failed.", ex ) );
    throw daq::rmgr::ConfigExceptionE(daq::ers2idl_issue(ex));
 }

 const std::string config_after = getCInfoAsString( rm_impl->getConfigInfo() );

 if (config_before != config_after)
    text << "Configuration updated from:\n" << config_before << "\nto:\n" << config_after;
 else
    text << "No configuration updated";
 ers::log( ers::Message( ERS_HERE, text.str()));
}

//request resources actions
::CORBA::Long
daq::rmgr::RmgrImpl::requestResources(const daq::rmgr::ResOwnerInfo &owner, const char *softwareId)
{
  const std::string p(owner.partition);
  const std::string sw(softwareId);
  const std::string computer(owner.host);
  const std::string client(owner.user);
  const std::string appl(owner.application);

  std::scoped_lock scoped_lock(m_mutex);

  //check if no sw object
  if (!rm_impl->hasResources(sw))
    {
      ers::log(daq::rmgr::SoftwareNotFound( ERS_HERE, sw));
      throw daq::rmgr::SoftwareNotFoundE(softwareId);
    }

  HandleAndTime hndlt = rm_impl->allocateResources(p, sw, appl, computer, owner.pid, client);

  if (rm_impl->isDoBackup())
    rm_impl->getBck()->logRequestResources(p, sw, appl, computer, owner.pid, client, hndlt.hndl, hndlt.time);

  return hndlt.hndl;
}

void daq::rmgr::RmgrImpl::setProcess(::CORBA::Long handle, ::CORBA::ULong pid) {
 {
  std::scoped_lock scoped_lock( m_mutex ); //use just single, we update
  rm_impl->setProcess( handle, pid );
 }
  if(rm_impl->isDoBackup())
     rm_impl->getBck()->logSetProcess( handle, pid );
}

 ::CORBA::Long daq::rmgr::RmgrImpl::requestResourcesForProcess(const char* partitionName,
                                const daq::rmgr::Res_ID_list&  res_lst,
                                const char* computerName,
                                CORBA::ULong pid,
                                const char* clientName) {
    const std::string p(partitionName);
    const std::string computer(computerName);
    const std::string client(clientName);
    const std::string application = " ";
    std::scoped_lock scoped_lock( m_mutex );

    const HandleAndTime hndlt = rm_impl->allocateResources4Process( p, res_lst,
		    application,
		    computer, pid, client);
    if(rm_impl->isDoBackup())
      rm_impl->getBck()->logRequestResourcesForProcess( p, res_lst, 
		      application,
		      computer, pid, client, hndlt.hndl, hndlt.time);
    return hndlt.hndl;
}

void daq::rmgr::RmgrImpl::addMesRes( std::ostringstream& str, daq::rmgr::Allocated_Info* info ) {
 try{
   const std::string types[] = { "SW", "HW" };
   str << "Free resources info= "; 
   for( unsigned int i=0; i<info->length(); i++) {
     str << "{";
     str << " handle=" << (*info)[i].handle;
     str << " resource=" << (*info)[i].resource;
     str << " type=" << types[(*info)[i].type];
     str << " maxPP=" << (*info)[i].maxPP;
     str << " maxTotal=" << (*info)[i].maxTotal;
     str << std::endl;
     str << " swObj=" << (*info)[i].swObj;
     str << " partition=" << (*info)[i].partition;
     str << " client=" << (*info)[i].client;
     str << " computer=" << (*info)[i].computer;
     str << " pid=" << (*info)[i].pid;
     str << " hwId=" << (*info)[i].hwId;
     str << " allocPP=" << (*info)[i].allocPP;
     str << " allocTotal=" << (*info)[i].allocTotal;
     str << "}" << std::endl;
   }
 }
 catch (const std::exception& ex) {
	ers::log( daq::rmgr::CannotGetAllocatedinfo( ERS_HERE, ex.what(), ex ) );
 }
}

void daq::rmgr::RmgrImpl::addMesRes( std::ostringstream& str, daq::rmgr::Handle_Info* info ) {
  try{
   str << "Free resources info= {";
   str << " handle=" << info->handle;
   str << " swObj=" << info->swObj;
   str << " computer=" << info->computer;
   str << " partition=" << info->partition;
   str << " client=" << info->client;
   str << " pid=" << info->pid << std::endl;
   str << "  Resources:" << std::endl;
   const std::string types[] = { "SW", "HW" };
   for(unsigned int i=0; i<info->resDetails.length(); i++) {
     str << "  {";
     str << "   resource=" << info->resDetails[i].resource;
     str << "   type=" << types[info->resDetails[i].type];
     str << "   maxPP=" << info->resDetails[i].maxPP;
     str << "   maxTotal=" << info->resDetails[i].maxTotal;
     str << "   allocPP=" << info->resDetails[i].allocPP;
     str << "   allocTotal=" << info->resDetails[i].allocTotal;
     str << "  }"<< std::endl;
   }
  }
  catch (const std::exception& ex) {
      ers::log( daq::rmgr::CannotGetAllocatedinfo( ERS_HERE, ex.what(), ex ) );
  }
}

const std::string daq::rmgr::RmgrImpl::getCInfoAsString( daq::rmgr::Config_update* inf ) {
  std::ostringstream txt;
  try {
     daq::rmgr::Config_updateHelper helper( inf );
     txt << helper;
     delete inf;
     helper.cinfo = nullptr;
  }
  catch (const std::exception& ex) {
      ers::log( daq::rmgr::STDExceptioon( ERS_HERE, "failed to get config info. " + static_cast<const std::string>( ex.what() ), ex ) );
  }
  return txt.str();
}
 
std::string getLogCalledBy() {
     std::string logMess = daq::tokens::enabled() ? "called by verified user " : "called by ";
     return logMess;
}

std::string daq::rmgr::RmgrImpl::getForLogUserMes( const daq::rmgr::CallerInfo& caller ) {
   const std::string usr(caller.user);
   bool useTok = daq::tokens::enabled();
   if( !useTok && usr.length() > 40)
      return "called by \"unknown user\"";
   std::string userMes = useTok ? rm_impl->getUser( usr ) : usr;
   userMes = !useTok ? "called by " + userMes : "called by verified user " + userMes;
   return userMes;
}

//free resources actions
void daq::rmgr::RmgrImpl::freeProcessResources( const char* p, const char* computer, CORBA::ULong pid, const daq::rmgr::CallerInfo& caller) {
 std::scoped_lock scoped_lock( m_mutex );
     std::ostringstream text;
     text << "\"freeProcessResources(\'" << p << "\',\'" << computer << "\'," << pid << ")\" " << caller.user 
     << "@" << caller.host << std::endl;
     daq::rmgr::Allocated_Info* inf = nullptr;
     inf = rm_impl->getPartitionCompProcInfo( p, computer, pid);

 bool isRemoved = rm_impl->freeResource( p, computer, pid);
 if( isRemoved ) {
     addMesRes( text, inf);
//     delete inf;
 }
 if(inf != nullptr)
	 delete inf;
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeProcessResources( p, computer, pid );
 if( isRemoved )
    ers::log( ers::Message( ERS_HERE, text.str()));
}

void  daq::rmgr::RmgrImpl::freeAllProcessResources( ::CORBA::Long handleId, const char* computer, CORBA::ULong pid, const daq::rmgr::CallerInfo& caller) {
    std::scoped_lock scoped_lock( m_mutex );
     std::ostringstream text;
     text << "\"freeAllProcessResources(" << handleId << ",\'" << computer << "\'," << pid << ")\" "
	<< caller.user
        << "@" << caller.host << std::endl;
     std::string partition("*"); //all partitions, use for log
     daq::rmgr::Handle_Info* inf = nullptr;
     if(handleId > 0) {
        inf = rm_impl->getHandleInfo( handleId );
     }
     daq::rmgr::Allocated_Info* inf2 = nullptr;
     if(pid != 0) { //do not free for pid==0
	  inf2 = rm_impl->getCompProcInfo( computer, pid);
     }

 bool isRemoved = false;
 if( handleId > 0 ) {
    isRemoved = rm_impl->freeResource( handleId );
    if( isRemoved ) {
	    addMesRes( text, inf);
//	    delete inf;
    }
 }
 bool isRemoved2 = false;
 if(pid != 0) {
    isRemoved2 = rm_impl->freeResource( computer, pid );
    if( isRemoved2 ) {
	    addMesRes( text, inf2);
//	    delete inf2;
    }
 }
 if(inf != nullptr)
	delete inf;
 if(inf2 != nullptr)
        delete inf2;

    if(rm_impl->isDoBackup()) {
       if( handleId > 0 && isRemoved )
          rm_impl->getBck()->logFreeResources( handleId );
       std::string partition("*"); //all partitions, use for log
       if( pid != 0 && isRemoved2) 
         rm_impl->getBck()->logFreeProcessResources( partition, computer, pid );
    }
    if( isRemoved || isRemoved2 )
      ers::log( ers::Message( ERS_HERE, text.str()));     
}

void daq::rmgr::RmgrImpl::freeResources (::CORBA::Long handle, const daq::rmgr::CallerInfo& caller) {
    std::scoped_lock scoped_lock( m_mutex );
     std::ostringstream text;
     text << "\"freeResources(" << handle <<  ")\" " 
	  << caller.user << "@" << caller.host << std::endl;
     daq::rmgr::Handle_Info* inf = rm_impl->getHandleInfo( handle );
 bool isRemoved = rm_impl->freeResource( handle );
 if( isRemoved ) {
	 addMesRes( text, inf);
	 delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeResources( handle );
 if( isRemoved )
   ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freeAllInPartition( const char* p, const daq::rmgr::CallerInfo& caller ) {
 std::scoped_lock scoped_lock( m_mutex );
 const std::string nm(caller.user);
 const  std::string myName = rm_impl->getUser( nm ); //can throw exception
 const  std::string host( caller.host );

 rm_impl->checkAccesPermission( myName, host, "freeAllInPartition" ); //throws exception if not allowed
 std::ostringstream text;

 text << "freeAllInPartition(\'" << host  << "\',\'" << myName << "\',\'" << p << "\')\" "
      << getLogCalledBy() << myName << "@" << host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getPartitionInfo(p);
 bool isRemoved = rm_impl->freeAllInPartition(  caller.host, myName.c_str(), p );
 if(isRemoved) {
    addMesRes( text, inf);
     delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
 rm_impl->getBck()->logFreePartition( caller.host, myName.c_str(), p);
 if(isRemoved)
   ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freePartition( const char* myHost,  const char* myName,const char* p ) {
 std::scoped_lock scoped_lock( m_mutex );
 std::ostringstream text;
 text << "freePartition(\'" << myHost << "\',\'" << myName << "\',\'" << p << "\')\" called by " << myName << "@" << myHost << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getPartitionInfo(p);
 bool isRemoved = rm_impl->freeAllInPartition( myHost, myName, p );  
 if(isRemoved) {
    addMesRes( text, inf);
     delete inf;
 }

 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreePartition( myHost, myName, p);
 if( isRemoved )
    ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freePartitionResource( const char* p, const char* resource, const daq::rmgr::CallerInfo& caller ) {
 std::scoped_lock scoped_lock( m_mutex );
 std::ostringstream text;
 text << "\"freePartitionResource(\'" << p << "\',\'" << resource << "\')\" "
      << caller.user << "@" << caller.host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getResourceInfo( p, resource );
 bool isRemoved = rm_impl->freePartitionResource( p, resource );
 if( isRemoved ) {
    addMesRes( text, inf);
    delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved )
   rm_impl->getBck()->logFreePartitionResource( p, resource );
 if( isRemoved )
    ers::log( ers::Message( ERS_HERE, text.str()));     
}

void daq::rmgr::RmgrImpl::freeAllOnComputer( const char* computer, const daq::rmgr::CallerInfo& caller ) {
 std::scoped_lock scoped_lock( m_mutex );
 const std::string nm(caller.user);
 const  std::string myName = rm_impl->getUser( nm );  //can throw exception
 const  std::string host( caller.host );
  
 rm_impl->checkAccesPermission( myName, host, "freeAllOnComputer" ); //throws exception if not allowed
 std::ostringstream text;
 text << "\"freeAllOnComputer(\'" << computer << "\')\" "
      << myName << "@" << host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getComputerInfo( computer );
 bool isRemoved = rm_impl->freeAllOnComputer( computer );
 if( isRemoved ) {
    addMesRes( text, inf);
    delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeComputerAll( computer );
 if( isRemoved )
    ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freeResource( const char* resource, const daq::rmgr::CallerInfo& caller ) {
  std::scoped_lock scoped_lock( m_mutex );
  std::ostringstream text;
  text << "\"freeResource(\'" << resource << "\')\" "
       << caller.user << "@" << caller.host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getResourceInfo( resource );
 bool isRemoved = rm_impl->freeResource( resource );
 if( isRemoved ) {
    addMesRes( text, inf);
    delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeResource( resource );
 if( isRemoved )
   ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freeComputerResource( const char* resource, const char* computer, const char* p, const daq::rmgr::CallerInfo& caller ) {
 std::scoped_lock scoped_lock( m_mutex );
 std::ostringstream text;
 text << "\"freeComputerResource(\'" << resource << "\',\'" << computer << "\',\'" << p << "\')\" "
      << caller.user << "@" << caller.host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getComputerResourceInfo( computer, p, resource );

 bool isRemoved = rm_impl->freeComputerResource( resource, computer, p );
 if( isRemoved ) {
    addMesRes( text, inf);
    delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeComputerResource( resource, computer, p );
 if( isRemoved )
   ers::log( ers::Message( ERS_HERE, text.str()));
}

void daq::rmgr::RmgrImpl::freeApplicationResources( const char* p, const char* swObj, const char* computer, const daq::rmgr::CallerInfo& caller ) {
 std::scoped_lock scoped_lock( m_mutex );
 const std::string nm(caller.user);
 const  std::string myName = rm_impl->getUser( nm );  //can throw exception
 const  std::string host( caller.host );

 const std::string partit( p );
 const std::string swo( swObj );
 if( !rm_impl->isOwner( partit, swo, host, myName ) )
    rm_impl->checkAccesPermission( myName, host, "freeApplicationResources" ); //throws exception if not allowed

 std::ostringstream text;
 text << "\"freeApplicationResources(\'" <<  p << "\',\'" << swObj << "\',\'" << computer << "\')\" "
      << myName << "@" << host << std::endl;
 daq::rmgr::Allocated_Info* inf = rm_impl->getApplicationInfo( p, swObj, computer );

 bool isRemoved = rm_impl->freeApplicationResources( p, swObj, computer );
 if( isRemoved ) {
    addMesRes( text, inf);
    delete inf;
 }
 if(rm_impl->isDoBackup() && isRemoved)
   rm_impl->getBck()->logFreeApplicationResources( p, swObj, computer );
 if( isRemoved )
   ers::log( ers::Message( ERS_HERE, text.str()));
}

//getting different info

daq::rmgr::Rmstr_lst* daq::rmgr::RmgrImpl::getPartitionsList() {
 std::scoped_lock scoped_lock( m_mutex );
    return rm_impl->getPartitionsList();
}

daq::rmgr::Rmstr_lst* daq::rmgr::RmgrImpl::getResourceOwners(const char* rname, const char* pname) {
 std::scoped_lock scoped_lock( m_mutex );
    return rm_impl->getResourceOwners(rname, pname);
}

daq::rmgr::Handle_Info* daq::rmgr::RmgrImpl::getHandleInfo(::CORBA::Long handle) {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getHandleInfo(handle);
}

daq::rmgr::Allocated_Info*  daq::rmgr::RmgrImpl::getResourceInfo(const char* p, const char* rn) {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getResourceInfo( p, rn);
}

daq::rmgr::Allocated_Info* daq::rmgr::RmgrImpl::getFullResInfo(const char*  rname) {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getResourceInfo( rname );
}
daq::rmgr::Allocated_Info* daq::rmgr::RmgrImpl::getPartitionInfo(const char* partition,
                                             const daq::rmgr::CallerInfo& caller) {
 std::scoped_lock scoped_lock( m_mutex );
 const std::string nm(caller.user);
 const  std::string myName = rm_impl->getUser( nm );  //can throw exception
 const  std::string host( caller.host );
 if( !rm_impl->isSoleOwner( partition, myName ) )
    rm_impl->checkAccesPermission( myName, host, "getPartitionInfo" ); //throws exception if not allowed

 //std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getPartitionInfo( partition );
}
daq::rmgr::Allocated_Info* daq::rmgr::RmgrImpl::getComputerInfo(const char* computer) {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getComputerInfo( computer );
}
daq::rmgr::Allocated_Info* daq::rmgr::RmgrImpl::getComputerResourceInfo(const char* computer, const char* partition, const char* resource) {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getComputerResourceInfo( computer, partition, resource );
}

daq::rmgr::Config_update* daq::rmgr::RmgrImpl::getConfigInfo() {
 std::scoped_lock scoped_lock( m_mutex );
 return rm_impl->getConfigInfo();
}

bool daq::rmgr::RM_Backup::createLogoutStream(const char* log_file) {
 if ( !_doBackup )
    return true;
 logostrm.exceptions ( std::ofstream::eofbit | std::ofstream::failbit
             | std::ofstream::badbit );
 try{
    if( logostrm.is_open() )
       logostrm.close();
 }
 catch (const std::ofstream::failure& ex) {
     std::string reason ( ex.what() );
     std::ostringstream txt;
     txt << ex.code();
     ers::error( daq::rmgr::closeStreamFailed( ERS_HERE, "old journal", reason, txt.str(), ex ) );
 }
 try {
    logostrm.open ( log_file, std::ios::app );
 }
 catch (const std::ofstream::failure& ex) {
    std::string fl( log_file );
    std::string reason ( ex.what() );
    std::ostringstream txt;
    txt << ex.code();
    throw daq::rmgr::CannotOpenJournal( ERS_HERE, fl, reason, txt.str() );
 }
 if(logostrm.tellp() == 0) { //was not created before
   try{
     mytime();
     logostrm << rmStamp << " " << logTime << std::endl;
     _numOkOperations = 0;
   }
   catch (const std::ofstream::failure& ex2) {
     std::string fl( log_file );
     std::string reason ( ex2.what() );
     std::ostringstream txt;
     txt << ex2.code();
     throw daq::rmgr::CannotWriteToJournal( ERS_HERE, fl, reason, txt.str() );
   }
 }
 return true;
}

void daq::rmgr::RM_Backup::initialize( daq::rmgr::RM_Impl*& rm_impl) {
   _rm_impl = rm_impl;
   _restoreDone = false;
   if( _restoreFrBck ) {
      bool keep_saveBackup = _doBackup;
      _doBackup = false;
      _restoreDone = restoreArchive();
      _rm_impl->resetBackup( rm_impl->getBck() );
      _rm_impl->setConfDBName( rm_impl->getConfDBName() );
      rm_impl = _rm_impl; // because of serialization
      bool fromJournal = isDoResto( true); //check can load from journal
      if( fromJournal )
         loadFromLog(  getJournalPath().c_str(), rm_impl );
      else
          ers::error( daq::rmgr::NoJournal( ERS_HERE,  getJournalPath() ) );
      _doBackup = keep_saveBackup;
      _restoreDone = _restoreDone || fromJournal; //restore at least from archive or journal
      
   }
   if( _doBackup )
     createLogoutStream( getJournalPath().c_str() ); //here now append if exists
}

bool daq::rmgr::RM_Backup::checkState() {
   mytime();
   if ( !_doBackup )
      return false;
   if(!logostrm.is_open()) {
      if( _doBackup )
         ers::error( daq::rmgr::JournalDontOpen( ERS_HERE, getJournalPath() ) );
      return false;
   }
   if( logostrm )
      return true;
   else {
      ers::error( daq::rmgr::JournalFileBadState( ERS_HERE, getJournalPath() ) );
      return false;
   }
}

void daq::rmgr::RM_Backup::doErrMess(const std::ofstream::failure& ex ) {
   std::string reason ( ex.what() );
   std::ostringstream txt;
   txt << ex.code();
   ers::error( daq::rmgr::CannotWriteToJournal( ERS_HERE, getJournalPath() , reason, txt.str() ) );
}

void daq::rmgr::RM_Backup::readStrFromLog(std::ifstream& logstream, std::string& dest) {
  unsigned int w(0);
  logstream >> w;
  if(w > 0)
    logstream.get(logBuf[0]); //skip " "
  for(unsigned int i=0;i<w+1;i++)
    logstream.get(logBuf[i]);
  const std::string tmp(logBuf,w);
  if(!w)
    dest.clear();
  else
    dest = tmp;
}

std::string& daq::rmgr::RM_Backup::uint2str( unsigned int inp, std::string& outstr ) {
    static char* ptr;
    ptr = bufUint;
    boost::spirit::karma::generate(ptr, boost::spirit::uint_, inp);
    outstr.assign(bufUint, ptr - bufUint);
    return outstr;
}

std::string& daq::rmgr::RM_Backup::ulong2str( unsigned long inp, std::string& outstr ) {
    static char* ptr;
    ptr = bufUint;
    boost::spirit::karma::generate(ptr, boost::spirit::ulong_, inp);
    outstr.assign(bufUint, ptr - bufUint);
    return outstr;
}

std::string& daq::rmgr::RM_Backup::long2str( long inp, std::string& outstr ) {
    static char* ptr;
    ptr = bufUint;
    boost::spirit::karma::generate(ptr, boost::spirit::long_, inp);
    outstr.assign(bufUint, ptr - bufUint);
    return outstr;
}

void daq::rmgr::RM_Backup::logStr( const std::string& valStr) {
   logostrm << prb << uint2str( valStr.length(), str4conv ) << prb <<  valStr;
}

void daq::rmgr::RM_Backup::logChr( const char* valChr) {
   logostrm << prb << uint2str( strlen(valChr), str4conv ) << prb <<  valChr;
}

void daq::rmgr::RM_Backup::logULong( CORBA::ULong val) {
  logostrm << prb <<  ulong2str( val, str4conv ); 
}

void daq::rmgr::RM_Backup::logRequestResourcesForProcess( const std::string& p, const daq::rmgr::Res_ID_list& res_lst, 
		const std::string& application,
		const std::string& computer, CORBA::ULong pid, const std::string& client, 
		long handle, long long allocTime) {
   if( checkState() )
     try {
         std::string res0(res_lst.length()>0 ? res_lst[0] : "");
         if(handle > 0)
           logostrm << okey;
         else
           logostrm << nok;
         logostrm << long2str( logTime, str4conv ) << prb
                 << daq::rmgr::opRequestResourcesForProcess;
         logStr( p );
         logStr( res0 );
	 logStr( application );
         logStr( computer );
         logULong( pid );
         logStr( client );
         logostrm << prb << allocTime;
         logostrm << idStr << long2str( handle, str4conv )
                  << std::endl << std::flush;
         ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logRequestResources( const std::string& p, const std::string& sw, 
		const std::string& applic,
		const std::string& computer, CORBA::ULong pid, const std::string& client,
	       long handle, long long timeCreation) {
   if( checkState() )
     try {
         if(handle > 0)
           logostrm << okey;
         else
           logostrm << nok;
         logostrm << long2str( logTime, str4conv ) << prb
                 << daq::rmgr::opRequestResources;
         logStr( p );
         logStr( sw );
	 logStr( applic ); 
         logStr( computer );
         logULong( pid );
         logStr( client );
         logostrm << prb << timeCreation;
         logostrm << idStr << long2str( handle, str4conv )
                  << std::endl << std::flush;
         ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeResources( long handle ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << daq::rmgr::opFreeResources;
        logostrm << prb << long2str( handle, str4conv )
                 << std::endl << std::flush;
           ++_numOkOperations;
        }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeApplicationResources( const std::string& p, const std::string& swObj, const std::string& computer ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreeApplicationResources;
        logStr( p );
        logStr( swObj );
        logStr( computer );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeProcessResources( const std::string& p, const std::string& computer, CORBA::ULong pid ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreeProcessResources;
        logStr( p );
        logStr( computer );
        logULong( pid );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreePartition( const char* myHost,  const char* myName, const char* p ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreePartition;
        logChr( myHost );
        logChr( myName );
        logChr( p );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreePartitionResource( const char* p, const char* resource ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreePartitionResource;
        logChr( p );
        logChr( resource );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeComputerAll( const char* computer ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreeComputerAll;
        logChr( computer );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeResource( const char* resource ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreeResource;
        logChr( resource );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logFreeComputerResource( const char* resource, const char* computer, const char* p ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << opFreeComputerResource;
        logChr( resource );
        logChr( computer );
        logChr( p );
        logostrm << std::endl << std::flush;
           ++_numOkOperations;
     }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }
}

void daq::rmgr::RM_Backup::logSetProcess( long hndle, unsigned long pid ) {
   if( checkState() )
     try {
        logostrm << okey << long2str( logTime, str4conv ) << prb
                 << daq::rmgr::opSetProcess;
        logostrm << prb << long2str( hndle, str4conv )
		 << prb << long2str( pid, str4conv ) 
                 << std::endl << std::flush;
           ++_numOkOperations;
        }
     catch (const std::ofstream::failure& ex1) {
            doErrMess( ex1 );
     }

}

void daq::rmgr::RM_Backup::exitLogLoadWithError(std::string operation, int inum,  std::ostringstream& txt) {
   txt << std::endl<<"loadFromLog read error in operation " << operation
    << " in line number " << inum;
   ers::fatal( daq::rmgr::WrongJournal( ERS_HERE, txt.str()));
   throw daq::rmgr::WrongJournal( ERS_HERE, txt.str() );
}

void daq::rmgr::RM_Backup::unknownExcInLoadJournal( std::string operation, int inum,  std::ostringstream& txt, const std::string commonMess) {
  txt<<"Uknown exception"<<std::endl;
  txt<< commonMess;
  exitLogLoadWithError( operation, inum, txt ); 
}
void daq::rmgr::RM_Backup::readStr(std::ifstream& logstream, std::string& dest) {
  unsigned int w(0);
  logstream >> w;
  if(w > 0)
    logstream.get(logBuf[0]); //skip " "
  for(unsigned int i=0;i<w+1;i++)
    logstream.get(logBuf[i]);
  const std::string tmp(logBuf,w);
  if(!w)
    dest.clear();
  else
    dest = tmp;
}

void daq::rmgr::RM_Backup::messFromIOExcep( std::ostringstream & txt, const std::ios_base::failure& e ) {
   txt << "Caught an ios_base::failure.\n"
       << "Reason: " << e.what()
       << "\nError code: " << e.code();
}

void daq::rmgr::RM_Backup::loadFromLog( const char* logPath, daq::rmgr::RM_Impl* rm_impl) {
   std::string p;
   std::string computer;
   std::string client;
   std::string sw;
   std::string application;
   std::string host;
   std::string user;
   std::string res0;
   std::ifstream pis(logPath);
   daq::rmgr::Res_ID_list res_lst;
   int iline = 0;
   std::string _okey;
   long long timeCreation = 0;

   std::string test;
   std::ostringstream txt;
   std::ostringstream commonMess;
   commonMess << "Can not load RM_Server from log with path=\"" << logPath << "\".";

 try{
   if (!pis.is_open()){
     txt<< commonMess.str();
     exitLogLoadWithError("Log unavailable for RM_Server load", -1, txt); 
   }
//adjust to 1st action read
   int ij=0;
   std::string strtmp ="";
   pis >> test;
   while(strtmp != "time") {
     ij++;
     pis >> strtmp;
     test +=" " + strtmp;
     if (ij >10) break;
   }
   
   if (!(test  == rmStamp)){ //log crashed
     txt<< commonMess.str();
     exitLogLoadWithError("Log did not detected as Resource Manager log.", iline, txt);
   }

   pis >> test; //time 
   pis >> _okey;

   while(!pis.eof()){
     unsigned long pid;
     int  action;
     pis  >> ij >> action;
     iline++;
     switch ( action) {
       case opRequestResources:
         try{
            readStr(pis, p);
            readStr(pis, sw);
            readStr(pis, application);
            readStr(pis, computer);
            pis >> pid;
            readStr(pis, client);
	    pis >> timeCreation;
            pis >> test;
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->allocateResources( p, sw,
				 application,
				 computer, pid, client, timeCreation);
                 _numOkOperations++;
              } 
              catch ( daq::rmgr::Exception & ex) {
                ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "RequestResources", iline, txt, commonMess.str() );
              }
            }   
         }
         catch( std::ifstream::failure& exrr ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exrr );
            exitLogLoadWithError("RequestResources", iline, txt);
         }

         break;

       case opRequestResourcesForProcess:
        try{
            readStr(pis, p);
            readStr(pis, res0);
	    readStr(pis, application);
            readStr(pis, computer);
            pis >> pid;
            readStr(pis, client);
            pis >> timeCreation;
            pis >> test;
            res_lst.length(1);
            res_lst[0] = CORBA::string_dup(res0.c_str());
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->allocateResources4Process( p, res_lst, 
				 application,
				 computer, pid, client, timeCreation);
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "RequestResourcesForProcess", iline, txt, commonMess.str() );
              }
            }
         }
         catch(  std::ifstream::failure& exrp ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exrp );
            exitLogLoadWithError("RequestResourcesForProcess", iline,txt);
         }
         break;
       case opFreeResources:
         try{
           long handle;
           pis >> handle;
           if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeResource( handle );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeResources", iline, txt, commonMess.str() );
              }
           }
         }
         catch(  std::ifstream::failure& exfp) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exfp );
            exitLogLoadWithError("FreeResources", iline, txt);
         }
         break;
       case opFreeApplicationResources:
         try{
            readStr(pis, p);
            readStr(pis, sw);
            readStr(pis, computer);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeApplicationResources( p, sw, computer );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeApplicationResources", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& exfar) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exfar );
            exitLogLoadWithError("FreeApplicationResources", iline, txt);
         }
         break;
       case opFreeProcessResources:
         try{
            readStr(pis, p);
            readStr(pis, computer);
            pis >> pid;
            if((const std::string) _okey == okey1) {
              try {
		 if( p == "*" )
			 rm_impl->freeResource(computer, pid);
		 else
                         rm_impl->freeResource( p, computer, pid);
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeProcessResources", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& exfpr ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exfpr );
            exitLogLoadWithError("FreeProcessResources", iline, txt);
         }
         break;

       case opFreePartition:
         try{
            readStr(pis, host);
            readStr(pis, user);
            readStr(pis, p);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeAllInPartition( host.c_str(), user.c_str(), p );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreePartition", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& ex2 ) {
             txt<< commonMess.str();
            messFromIOExcep( txt, ex2 );
            exitLogLoadWithError("FreePartition", iline, txt);
         }
         break;
       case opFreePartitionResource:
         try{
            readStr(pis, p);
            readStr(pis, res0);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freePartitionResource( p, res0 );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreePartitionResource", iline, txt, commonMess.str() );
              }
            }
         }
         catch(  std::ifstream::failure& ex3) {
            txt<< commonMess.str();
            messFromIOExcep( txt, ex3 );
            exitLogLoadWithError("FreePartitionResource", iline, txt);
         }
         break;

       case opFreeComputerAll:
         try{
            readStr(pis, computer);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeAllOnComputer( computer );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeComputerAll", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& ex4 ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, ex4 );
            exitLogLoadWithError("FreeComputerAll", iline, txt);
         }
         break;

       case opFreeResource:
         try{
            readStr(pis, res0);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeResource( res0 );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeResource", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& ex5 ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, ex5 );
            exitLogLoadWithError("FreeResource", iline, txt);
         }
         break;

       case opFreeComputerResource:
         try{
            readStr(pis, res0);
            readStr(pis, computer);
            readStr(pis, p);
            if((const std::string) _okey == okey1) {
              try {
                 rm_impl->freeComputerResource( res0, computer, p );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                 ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "FreeComputerResource", iline, txt, commonMess.str() );
              }
            }
         }
         catch( std::ifstream::failure& ex6 ) {
            txt<< commonMess.str();
            messFromIOExcep( txt, ex6 );
            exitLogLoadWithError("FreeComputerResource", iline, txt);
         }
         break;

       case opSetProcess:
	 try{ 
           long handle;
	   unsigned long pid;
           pis >> handle;
	   pis >> pid;
           if((const std::string) _okey == okey1) {
              try {
                 rm_impl->setProcess( handle, pid );
                 _numOkOperations++;
              }
              catch ( daq::rmgr::Exception & ex) {
                ers::debug(ex, 1);
              }
              catch( ... ) {
                 unknownExcInLoadJournal( "SetProcess", iline, txt, commonMess.str() );
              }
           }
         }
         catch(  std::ifstream::failure& exfp) {
            txt<< commonMess.str();
            messFromIOExcep( txt, exfp );
            exitLogLoadWithError("SetProcess", iline, txt);            
	 }

       default:
          break; 
     }
     pis >> _okey;
   }
   pis.close();
 }
 catch( std::ifstream::failure& exfin ) {
   txt<< commonMess.str();
   messFromIOExcep( txt, exfin );
   exitLogLoadWithError("Uknown execption in load journal", iline, txt);
 }
}

bool daq::rmgr::RM_Backup::saveArchive() {
  try {
     std::ofstream ofs( getBckArchivePath(true) ); //save always into new file
     boost::archive::text_oarchive oa(ofs);
     oa & _rm_impl;
     return true;
  }
  catch( const boost::archive::archive_exception& ex ) {
     ers::error( daq::rmgr::CannotSaveArchive( ERS_HERE, getBckArchivePath(true), ex ) );
  }
  catch( const std::ofstream::failure &ex ) {
      ers::error( daq::rmgr::CannotSaveArchive( ERS_HERE, getBckArchivePath(true), ex ) );
  }
  return false;
}

bool daq::rmgr::RM_Backup::restoreArchive() {
  if( !isDoResto() ) {
     ers::error( daq::rmgr::NoBackupFile( ERS_HERE, getBckArchivePath() ) );
     return false;
  }
  std::ifstream ifs( getBckArchivePath() );
  try {
     boost::archive::text_iarchive ia(ifs);
     ia & _rm_impl;
  }
  catch( const boost::archive::archive_exception& ex ) {
     ers::fatal( daq::rmgr::FailedRestoreFromArchive( ERS_HERE, getBckArchivePath(), ex.what() ) );
     throw daq::rmgr::FailedRestoreFromArchive( ERS_HERE, getBckArchivePath(), ex.what() );
  }
  return true;
}

daq::rmgr::RM_Backup::~RM_Backup() {
  if( _doBackup )
       saveBackup();
  _doBackup = false;
  try {
     if( logostrm.is_open() )
        logostrm.close();
  }
  catch( std::ofstream::failure& ex1 ) {
     std::string reason ( ex1.what() );
     std::ostringstream txt;
     txt << ex1.code();
     ers::error( daq::rmgr::closeStreamFailed( ERS_HERE, "journal", reason, txt.str(), ex1 ) );
  }
}

bool daq::rmgr::RM_Backup::saveBackup() {
 bool retv = saveArchive();
 if( !retv ) {
     return false; //all messages already in saveArchive
 }
 // keep current journal info in OLD and reopen it.
 try {
    if( logostrm.is_open() )
       logostrm.close();  
    //now replace archive and journal names (keep old stuff as OLD)
    rename( getBckArchivePath().c_str(), ( getBckArchivePath() + OLD ).c_str() );
    rename( getJournalPath().c_str(), ( getJournalPath() + OLD ).c_str() );
    //archive saved always into NEW. Now remove NEW from name
    rename( getBckArchivePath( true ).c_str(), getBckArchivePath().c_str());
   //now reopen journal stream
   createLogoutStream( getJournalPath().c_str() );
 }
 catch (std::ofstream::failure& ex) {
    ers::error( daq::rmgr::CopyJournalFailed( ERS_HERE, getBckArchivePath(), ex ) );
    return false;
 }
 return true;
}

bool daq::rmgr::RM_Backup::isDoResto( bool journal ) {
  if( !_restoreFrBck )
     return false;
  std::string filename = journal ? getJournalPath() : getBckArchivePath(); 
  std::filesystem::path bckf = filename;
  if( !std::filesystem::exists( bckf ) )
     return false;
  if( std::filesystem::file_size( bckf ) < 1 ) {
     return false;
  }
  return true;
}

void daq::rmgr::RM_Backup::checkBackup() {
  if( !_doBackup && !_restoreFrBck)
      return; 
  std::filesystem::path bckf = getBckArchivePath();

  if( std::filesystem::is_directory( _backupPath ) ) {
    ers::fatal( daq::rmgr::BackupFileIsDirectory( ERS_HERE, bckf.string() ) );
    throw daq::rmgr::BackupFileIsDirectory( ERS_HERE, bckf.string() );
  }
  if( std::filesystem::exists( bckf ) )
      return; // need check dir as writable?

  std::filesystem::path parDir = bckf.parent_path();
  if( parDir.string() == "/")
     parDir = std::filesystem::path( _backupPath );
  try {
      if( !std::filesystem::exists( parDir ) )
          std::filesystem::create_directories( parDir );
  }
  catch (const std::filesystem::filesystem_error& ex) {
      std::string reason ( ex.what() );
      std::ostringstream txt;
      txt << ex.code();
      ers::fatal( daq::rmgr::FailedCreateBckParentDir( ERS_HERE, parDir.string(), reason,  txt.str() ) );
      throw daq::rmgr::FailedCreateBckParentDir( ERS_HERE, parDir.string(), reason,  txt.str() );
  }

  try {
     std::ofstream ofs( bckf.string() );
     ofs.close();
     std::remove( bckf.string().c_str() );
  }
  catch( const std::ofstream::failure &ex ) {
      std::string reason ( ex.what() );
      std::ostringstream txt;
      txt << ex.code();
      ers::fatal( daq::rmgr::FailedWriteToBackupDir( ERS_HERE, parDir.string(), reason,  txt.str() ) );
      throw daq::rmgr::FailedWriteToBackupDir( ERS_HERE, parDir.string(), reason,  txt.str() );
  }
}



