#include <ResourceManager/rminfo.h>

std::ostream& daq::rmgr::operator<<(std::ostream& str, const daq::rmgr::HandleInfo& info) {
   str << "HandleInfo={" << std::endl;
   str << " handle=" << info._handle << std::endl;
   str << " swObj=" << info._swObj;
   str << " application=" << info._application; //AI 030624
   str << " computer=" << info._computer;
   str << " partition=" << info._partition;
   str << " client=" << info._client;
   str << " pid=" << info._pid << std::endl;
   str << "  Resources:" << std::endl;
   const std::string types[] = { "SW", "HW" };
   //int j;
   for( auto& ri : info._resDetails ) {
     str << "  {";
     str << "   resource=" << ri._resource;
     int  j = ri._isHWR ? 1 : 0;
     str << "   type=" << types[j];
     str << "   maxPP=" << ri._maxPP;
     str << "   maxTotal=" << ri._maxTotal << std::endl;
     str << "   allocPP=" << ri._allocPP;
     str << "   allocTotal=" << ri._allocTotal;
     str << "  }"<< std::endl;
   }
   return str;
}

std::ostream& daq::rmgr::operator<<(std::ostream& str,  const daq::rmgr::AllocatedResource& info) {
   const std::string types[] = { "SW", "HW" };
   int j = info._isHWR ? 1 : 0;
   str << "AllocatedResource: {" << std::endl;
     str << " handle=" << info._handle;
     str << " resource=" << info._resource;
     str << " type=" << types[j];
     str << " maxPP=" << info._maxPP;
     str << " maxTotal=" << info._maxTotal;
     str << " hwClass=" << info._hwClass << std::endl;
     str << " swObj=" << info._swObj;
     str << " application=" << info._application;
     str << " partition=" << info._partition;
     str << " client=" << info._client << std::endl;
     str << " computer=" << info._computer;
     str << " pid=" << info._pid;
     str << " hwId=" << info._hwId;
     str << " allocPP=" << info._allocPP;
     str << " allocTotal=" << info._allocTotal;
     str << "}" << std::endl;
   return str;
}

std::ostream& daq::rmgr::operator<<(std::ostream& str, const daq::rmgr::AllocatedInfo& info) {
  str << "AllocatedInfo= " << std::endl;
  for( const auto& ar : info.list )
     str<<ar;
  str<< "  }"<<std::endl;
  return str;
}

std::ostream& daq::rmgr::operator<<(std::ostream& str, const daq::rmgr::ConfigUpdate& cinfo) {
  str << "Config_update = {"<<std::endl << "  SW_Resources= {";
  for(const auto& r : cinfo.swr)
     str << "    {  id="<<r._rid << " maxTotal=" << r._maxTotal 
         << " maxPP=" << r._maxPP << " }" <<std::endl;
  str << "  }"<<std::endl;
  str << "   HW_Resources= {";
  for( const auto& r : cinfo.hwr)
     str << "    {  id="<< r._rid << " maxTotal=" << r._maxTotal
         << " maxPP=" << r._maxPP << " hwClass=" << r._hwClass << "  }" <<std::endl;
  str << "  }"<<std::endl;
  str << "    SWObjects= {";
  for( const auto& sw :  cinfo.swo ) {
     str << "swObj="<<sw._swid<< "  Resources={ ";
     for( const auto& rid : sw._rlist )
         str << " {"<<rid << "}";
     str << "  }"<<std::endl;
  }
  str << "   }"<<std::endl<<"} end of Config_update"<<std::endl;
  return str;
}
