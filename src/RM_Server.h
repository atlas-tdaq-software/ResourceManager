#ifndef RM_Server_H_INCLUDED
#define RM_Server_H_INCLUDED

#include <mutex>
#include <shared_mutex>
#include <boost/spirit/include/karma.hpp>
#include <fstream>
#include <filesystem>

#include <locale.h>
#include <time.h>
#include <map>
#include <vector>



#include "src/RMimpl.h"

#include "ipc/object.h"
#include "ipc/core.h"
#include <ipc/signal.h>

using RMIPCNamedObject = ::IPCNamedObject<POA_rmgr::ResMgr>;

namespace daq
{
namespace rmgr
{

enum logAction { opRequestResources, opRequestResourcesForProcess,
opFreeResources, opFreeApplicationResources, opFreeProcessResources, opFreePartition, opFreePartitionResource, opFreeComputerAll, opFreeResource, opFreeComputerResource, opSetProcess };
/**
*  This class implement the IDL interface and delegate
*  any action to the RM_Impl class
* **/
class RmgrImpl :  public RMIPCNamedObject
{
public:
  RmgrImpl();
  RmgrImpl(std::string serverName, RM_Impl* rmimpl);
  ~RmgrImpl();
  void rmConfigUpdate ( const char* partition, const char* user );

//request resources actions
  ::CORBA::Long requestResources( const daq::rmgr::ResOwnerInfo& owner, const char* softwareId ); 
  void setProcess(::CORBA::Long handle, ::CORBA::ULong pid);
 ::CORBA::Long requestResourcesForProcess(const char* partitionName,
                                const daq::rmgr::Res_ID_list&,
                                const char* computerName,
                                CORBA::ULong pid,
                                const char* clientName);

//free resources actions
void freeProcessResources( const char* p, const char* computer, CORBA::ULong pid, const daq::rmgr::CallerInfo& caller);
void  freeAllProcessResources( ::CORBA::Long handleId, const char* computer, CORBA::ULong pid, const daq::rmgr::CallerInfo& caller);
void freeResources (::CORBA::Long handle, const daq::rmgr::CallerInfo& caller);
void freePartition( const char* myhost, const char* myname, const char* p );
void freeAllInPartition( const char* p, const daq::rmgr::CallerInfo& caller );

void freePartitionResource( const char* p, const char* resource, const daq::rmgr::CallerInfo& caller );
void freeAllOnComputer( const char* computer, const daq::rmgr::CallerInfo& caller );
void freeResource( const char* resource, const daq::rmgr::CallerInfo& caller );
void freeComputerResource( const char* resource, const char* computer, const char* p, const daq::rmgr::CallerInfo& caller );
void freeApplicationResources( const char* p, const char* swObj, const char* computer, const daq::rmgr::CallerInfo& caller );

//getting different info
daq::rmgr::Rmstr_lst* getPartitionsList();
daq::rmgr::Rmstr_lst* getResourceOwners(const char* rname, const char* pname);
daq::rmgr::Handle_Info* getHandleInfo(::CORBA::Long handle);
daq::rmgr::Allocated_Info*  getResourceInfo(const char* p, const char* rn);
daq::rmgr::Allocated_Info* getFullResInfo(const char*  rname);
daq::rmgr::Allocated_Info* getPartitionInfo(const char* partition, const daq::rmgr::CallerInfo& caller );
daq::rmgr::Allocated_Info* getComputerInfo(const char* computer);
daq::rmgr::Allocated_Info* getComputerResourceInfo(const char* computer, const char* partition, const char* resource);
daq::rmgr::Config_update* getConfigInfo();
const std::string getServerName() const {
    return _serverName;
}
void shutdown() {
   daq::ipc::signal::raise(SIGTERM);
}

void logConfigInfo();

private:
    std::string _serverName;
    RM_Impl* rm_impl;
    std::mutex m_mutex;

    void addMesRes( std::ostringstream& str, daq::rmgr::Allocated_Info* info );
    void addMesRes( std::ostringstream& str, daq::rmgr::Handle_Info* info );
    const std::string getCInfoAsString( daq::rmgr::Config_update* inf );
    std::string getForLogUserMes( const daq::rmgr::CallerInfo& caller );
};

/**
*  Support all back-up functionality including journaling, store/restore data into/from backup file
*
*  **/
class RM_Backup {
    public:
      RM_Backup( const char * backupPath, bool doBackup, bool restoreFrBck = false)
         :  _backupPath(backupPath), _doBackup(doBackup), _restoreFrBck(restoreFrBck),
            _numOkOperations(0) {
      }
      ~RM_Backup();
      const std::string& getBckPath() {
         return _backupPath;
      }
      bool isDoBackup() {
          return _doBackup;
      }
      bool isRestoreBackup() {
         return _restoreFrBck;
      }
      bool saveBackup();

//all print to journal methods
void logRequestResourcesForProcess( const std::string& p, const daq::rmgr::Res_ID_list& res_lst, 
		const std::string& application, //AI 030624
		const std::string& computer, CORBA::ULong pid, const std::string& client, 
		long handle, long long timeCreation);
void logRequestResources( const std::string& p, const std::string& sw, 
		const std::string& applic, //AI 030624
		const std::string& computer, CORBA::ULong pid, const std::string& client, 
		long handle, long long timeCreation);
void logFreeResources( long handle );
void logFreeApplicationResources( const std::string& p, const std::string& swObj, const std::string& computer );
void logFreeProcessResources( const std::string& p, const std::string& computer, CORBA::ULong pid );
void logFreePartition( const char* myHost,  const char* myName,const char* p );
void logFreePartitionResource( const char* p, const char* resource );
void logFreeComputerAll( const char* computer );
void logFreeResource( const char* resource );
void logFreeComputerResource( const char* resource, const char* computer, const char* p );
void logSetProcess( long hndle, unsigned long pid ); //AI 030624

void loadFromLog( const char* logPath, daq::rmgr::RM_Impl* rm_impl);
void initialize( daq::rmgr::RM_Impl*& rm_impl );
void checkBackup(); //throw exception if problem
bool restoreDone() { return _restoreDone; }

  protected:
      bool createLogoutStream(const char* log_file);
      bool checkState();
      void doErrMess( const std::ofstream::failure& ex );
      void readStrFromLog(std::ifstream& logstream, std::string& dest);
  private:
      std::string _backupPath;
      bool _doBackup;
      bool _restoreFrBck;
      bool _restoreDone = false; //to use in server strating message
      long _numOkOperations; //number of succeed requests
      std::ofstream logostrm; //stream for logout sysprints
      long logTime;
      std::string str4conv;
      char bufUint[32];
      char logBuf[512];
      RM_Impl* _rm_impl; //only pointer, do not delete here
      //backup constants
      const std::string JOURNAL =  ".rmgrjournal";
      const std::string ARCH = ".rmgrarch";
      const std::string OLD = ".old";
      const std::string NEW = ".new";
      //working constants
      const std::string rmStamp = "Resource Manager time"; //first line
      const std::string bckMessErr = "Exception when writing to backup";
      const std::string prb = " ";
      const std::string okey = ">>Ok ";
      const std::string okey1 = ">>Ok";
      const std::string nok = ">>No ";
      const std::string idStr = " id=";

      void mytime() { logTime = (long) time(NULL); }
      std::string& uint2str( unsigned int inp, std::string& outstr );
      std::string& ulong2str( unsigned long inp, std::string& outstr );
      std::string& long2str( long inp, std::string& outstr );
      void logStr( const std::string& valStr);
      void logChr( const char* valChr);
      void logULong( CORBA::ULong val);
      void exitLogLoadWithError(std::string operation, int inum, std::ostringstream& txt);
      void unknownExcInLoadJournal( std::string operation, int inum,  std::ostringstream& txt, const std::string commonMess );
      void readStr(std::ifstream& logstream, std::string& dest);
      std::string getJournalPath( bool isNew = false ) {
         return !isNew ? (_backupPath + JOURNAL) : _backupPath + JOURNAL + NEW;
      }
      std::string getBckArchivePath( bool isNew = false ) {
         return !isNew ? (_backupPath + ARCH) : _backupPath + ARCH + NEW;
      }
      std::string getBackupPath( bool isNew = false ) {
         return !isNew ? _backupPath : _backupPath + NEW; 
      }

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version) {
         ar & _rm_impl;
      }
      bool saveArchive();
      bool  restoreArchive();
      void messFromIOExcep( std::ostringstream & txt, const std::ios_base::failure& e );
      bool isDoResto( bool journal=false );
};

/*! \brief This class provides initialisation, start and stop of the RM server.
    \ingroup public
    \sa
 */
class RM_Server
{
public:

//constructors
/*! \brief Create server object with server name RMSERVERDEFAULT name (hardcoded "RM_Server") and backup path RMSERVERDEFAULT + "_" + RMLOGNAME (hardcoded "RM_Server.rmgrbackup.data.xml"). Load configuration from configuration data base dbname
 *  */
  RM_Server(const std::string dbname);

/*! \brief Create server object with according parameters. If logfilename does not keep server name in its name then server name added at the end of logfilename.
 * \param  Configuration data base name which is used for configuration data loading and updating
 * \param sid         RM server name
 * \param backup  path where backup should be stored. Journal has the same as backup path but with additional extension ".journal". It keeps information about successful request/free resources Resource Manager operations. Both these files are needed for restore from backup but only backup path is needed as input parameter.
 * \param doBck         create backup file if parameter value is true
 * \param doresto       restore from backup file if parameter value is true and backup exists.
 *          */
  RM_Server(const std::string dbname, const char * sid, const char* backup = "RM_Server_rm.rmgrbackup.data.xml", bool doBck=true, bool doresto=true);

  ~RM_Server();
  const std::string getServerName() const {
    return idlRapper->getServerName();
  }

//server func
  void run( bool pmgsync=false );
  //void run();
  void removeServ();
inline static const std::string RMSERVERDEFAULT = "RM_Server";
private:
const std::string RMLOGNAME = "_rmgrbackup.data.xml";

  bool destroyInProgress;
  RmgrImpl* idlRapper;
  RM_Impl* rm_impl;
};


  } //end rm
} //end daq

#endif /* RM_Server_H_INCLUDED  */
