#include <cstring>

#include <ers2idl/ers2idl.h>
#include "src/RMimpl.h"
#include "src/RM_Server.h"
//#include "src/idlhelper.h"
#include <AccessManager/xacml/impl/RMResource.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/ServerInterrogator.h>
#include <AccessManager/util/ErsIssues.h>
#include <dal/RM_HW_Resource.h>
#include <dal/RM_SW_Resource.h>

namespace daq
{
namespace rmgr
{
const std::string eMess[] = {"\nResource ",  " unavailable in ", "partition", "global",
       	" scope", // for requestor:\n",
       	"* process ", " of software object ", " on host ", " run by ", " in partition ", "* handle ", " for process ",  " on computer ", "Resource holders:",
  " for hardware object ", " of class ", " of application " };
}
const std::string mesMaxBusy[] = { " Max: ", " Max (per partition): ",  " granted:  " };
}

// RMimpl class methods
daq::rmgr::RM_Impl::RM_Impl() : _use_am_auth(0),_lastHandle(0) { //only for restore from archive
  _bck = 0;
  setUseAM();
}
daq::rmgr::RM_Impl::RM_Impl( const std::string dbname ) : _config_db_name( dbname), _use_am_auth(0),_lastHandle(0) {
  _bck = 0;
  setUseAM();
}
daq::rmgr::RM_Impl::RM_Impl( RM_Backup* bck, const std::string dbname ) : _config_db_name( dbname), _use_am_auth(0),_lastHandle(0) {
  _bck = bck;
  setUseAM();
}
daq::rmgr::RM_Impl::~RM_Impl() {
   delete _bck;
   _bck = nullptr;
}


bool daq::rmgr::RM_Impl::isConfigAvailable() {
//call here only without idl call, can throw ERS exception
   try{
     Configuration cf(_config_db_name);
     return true;
   }
   catch( daq::config::Exception & ex) {
     throw daq::rmgr::ConfigurationException( ERS_HERE, std::string("failed load  configuration ") + getConfDBName(), ex);
   }
}

void daq::rmgr::RM_Impl::rmConfigurationUpdate() {
//call here only without idl call
   try{ 
     Configuration conf(_config_db_name); //exception if any problem
     rmConfigurationUpdate( &conf );
   }
   catch( daq::config::Exception & ex) {
     throw daq::rmgr::ConfigurationException( ERS_HERE, std::string("failed load  configuration ") + getConfDBName(), ex);
   }
}

void daq::rmgr::RM_Impl::rmConfigurationUpdate(::Configuration * conf) {
    //get all config resources
    std::vector<const daq::core::RM_Resource *> resources;
    conf->get(resources);
    ResourceType type;
    //update resources
    bool firstUpdate = _conf_res_lst.empty();
    for( auto& ir : resources ) {
      if(ir->castable(&daq::core::RM_SW_Resource::s_class_name))
         type = daq::rmgr::ResourceType::SWR;
      else if(ir->castable(&daq::core::RM_HW_Resource::s_class_name))
         type = daq::rmgr::ResourceType::HWR;
      else
         continue;
      RM_Resource r( ir->UID().c_str(), ir->get_MaxCopyTotal(), ir->get_MaxCopyPerPartition(), type);

//AI oct 2023
      if(const daq::core::RM_HW_Resource* hw_r = ir->cast<daq::core::RM_HW_Resource>())
         r.setHwCls( hw_r->get_HardwareClass() );

      //erase old value with the same key
      const auto& it =  _conf_res_lst.find(r.uid());
      if(it != _conf_res_lst.end()) {
          configWarningMess( &(it->second), &r );
         _conf_res_lst.erase(it);
      }
      else if(!firstUpdate)
          configWarningMess( 0, &r );
      //insert new value
      _conf_res_lst.insert( std::make_pair(r.uid(), r ) );
    }
//sw objects
 std::string strNull("");
    firstUpdate = _swobj_map.empty();
    std::vector<const daq::core::SW_Object *> sw_objects;
    conf->get(sw_objects);
    for( auto& j : sw_objects ) {
       std::string keysw( j->UID().c_str() );
       const auto& itsw = _swobj_map.find(keysw);
       unsigned int nr = j->get_Needs().size();
       if( itsw != _swobj_map.end()) {
          configWarningMess( *itsw, keysw, j->get_Needs() );
          _swobj_map.erase(keysw);
       }
       else {
          if(nr > 0 && !firstUpdate)
             configWarningMess( keysw );
       }
       if( nr == 0 )
          continue;
       std::vector<RM_Resource> rList;
       //add res references
       for ( size_t ii=0; ii < j->get_Needs().size(); ii++ ) {
          std::string res( j->get_Needs()[ii]->UID().c_str() );
          const auto& lstptr =  _conf_res_lst.find(res);
          if(lstptr != _conf_res_lst.end())
              rList.push_back(lstptr->second);
       }
       if(rList.size() > 0)
         _swobj_map.insert( std::make_pair(keysw, rList) );
    } 
    
}
void daq::rmgr::RM_Impl::configWarningMess(  std::string& str ) {
  std::ostringstream txt;
  txt << "New software object \'" << str << "\' added"<<std::endl;
  ers::log( ers::Message( ERS_HERE, txt.str()));
}
bool daq::rmgr::RM_Impl::listResourcesDiffer( std::vector<RM_Resource> oldV, const std::vector<const daq::core::RM_Resource*>& newVal ) {
   if(oldV.size() != newVal.size()) {
     return true;
  }
   bool found;
   for( unsigned int i=0; i<oldV.size(); i++) {
     found = false;
     for( unsigned int j=0; j<newVal.size(); j++ ) {
       if( oldV[i].uid() == newVal[j]->UID() ) {
          found = true;
          continue;
       }
     }
     if( !found )
       return true;
   }
   return false;
}
void daq::rmgr::RM_Impl::configWarningMess( const std::pair< std::string, std::vector<RM_Resource> > oldV, std::string& str,  const std::vector<const daq::core::RM_Resource*>& newVal ) {
    std::ostringstream txt;
    if( newVal.size() == 0 ) {
       txt << " \n Resources removed from software object \'" << str << "\'"<<std::endl;
    }
    else if( oldV.second.size() == 0 ) {
      txt << " \n Resources added to software object \'" << str << "\'"<<std::endl;
    }
    else {
        if( listResourcesDiffer(oldV.second, newVal) )
      txt << " \n Resource list was changed in software object \'" << str << "\'"<<std::endl;
    }
    if( txt.tellp() > 10 )
       ers::log( ers::Message( ERS_HERE, txt.str()));
}

void daq::rmgr::RM_Impl::configWarningMess( RM_Resource* oldVal, RM_Resource* newVal) {
    std::ostringstream txt;
    if( oldVal == 0 ) {
      txt << "\n New resource \'"<<newVal->uid()<<"\' added"<<std::endl;
      ers::log( ers::Message( ERS_HERE, txt.str()));
      return;
    }
    if( oldVal->maxTot() == newVal->maxTot() 
         && oldVal->maxPP() == newVal->maxPP()
         && oldVal->type() == newVal->type() )
       return; //same resources
    txt<<"\n Resource \'"<<oldVal->uid()<<"\' changed:";
    if( oldVal->maxTot() != newVal->maxTot() )
       txt<<" [maxTotal: old=" << oldVal->maxTot() << " new=" << newVal->maxTot()<<"]";
    if( oldVal->maxPP() != newVal->maxPP() )
       txt<<" [maxPP: old=" << oldVal->maxPP() << " new=" << newVal->maxPP()<<"]";
    if( oldVal->type() != newVal->type() ) {
       txt<<" [Resource type:  old =\'";
       if(oldVal->type() == daq::rmgr::SWR)
         txt<<"SWR\'";
       else
          txt<<"HWR\'";
       txt<<" new=\'";
       if(newVal->type() == daq::rmgr::SWR)
         txt<<"SWR\'";
       else
          txt<<"HWR\'";
     }
     ers::log( ers::Message( ERS_HERE, txt.str()));
}

bool daq::rmgr::RM_Impl::restoreDone() { return _bck->restoreDone(); }

daq::rmgr::Config_update* daq::rmgr::RM_Impl::getConfigInfo() {
  daq::rmgr::Config_update* rm_conf = new daq::rmgr::Config_update();
  int iswr = 0, ihwr = 0;
  for(  auto& it0 : _conf_res_lst )
       if( (it0.second).type() == daq::rmgr::SWR )
          iswr++;
       else
          ihwr++;
  rm_conf->swr_lst.length( iswr);
  rm_conf->hwcr_lst.length( ihwr );
  //set resources
  iswr = 0;
  ihwr = 0;
  for( auto& it : _conf_res_lst ) {
      RM_Resource r =  it.second;
      if(r.type() == daq::rmgr::SWR) {
         rm_conf->swr_lst[iswr].rid = CORBA::string_dup( r.uid().c_str() );
         rm_conf->swr_lst[iswr].max_total = r.maxTot();
         rm_conf->swr_lst[iswr++].max_pp = r.maxPP();
      }
      else { //HW
         rm_conf->hwcr_lst[ihwr].rid = CORBA::string_dup(r.uid().c_str());
         rm_conf->hwcr_lst[ihwr].max_total = r.maxTot();
         rm_conf->hwcr_lst[ihwr].max_pp = r.maxPP();
         rm_conf->hwcr_lst[ihwr].hwClass = CORBA::string_dup( r.hwCls().c_str());
         ihwr++;
      }
  }
  //set SWObj
  rm_conf->swobj_lst.length( _swobj_map.size() );
  int inum = 0, jnum = 0;
  for( auto& it2 : _swobj_map ) {
      std::vector<RM_Resource> list = it2.second;
      const std::string key = it2.first;
      rm_conf->swobj_lst[inum].swid = CORBA::string_dup( key.c_str() );
      rm_conf->swobj_lst[inum].res_list.length( list.size() );
      jnum = 0;
      for( auto& j : list )
              rm_conf->swobj_lst[inum].res_list[jnum++] = CORBA::string_dup( j.uid().c_str() );
      inum++;
  }
  return rm_conf;
}

//for unavailable info in exception
void daq::rmgr::RM_Control::putNoResourceMess( std::ostringstream& mess, const std::string& resource, const std::string& computer, bool isTot ) {
 mess << eMess[0] << resource;
 if(!computer.empty())
    mess << eMess[12] << computer;
 mess << eMess[1];
 if( isTot )
   mess << eMess[3];
 else
    mess << eMess[2];
 mess << eMess[4];
}

void daq::rmgr::RM_Control::putResStatMess( std::ostringstream& mess, const daq::rmgr::RM_Resource& res, const long allocated, bool isTot) {
  if( isTot )
    mess << mesMaxBusy[0] << res.maxTot();
  else
     mess << mesMaxBusy[1] << res.maxPP();
  mess << mesMaxBusy[2] << allocated;
  mess << std::endl;
}
//060325
bool daq::rmgr::RM_Control::isCorrectAppName( const std::string application, const std::string computer) {
  if( application.length() < 2 )
     return false;
  const std::string txt = "@" + computer;
  return application.rfind( txt ) == application.npos;
}
void daq::rmgr::RM_Control::putHandleMess( std::ostringstream& mess,  const std::string& computer, const std::string& swObj, const std::string& application, const std::string& client, const std::string& p, unsigned long pid, long hndl, const std::string& hwObj,  const std::string& hwCls ) {
  if(hndl > 0) // otherwise - requestor, no handle
    mess << eMess[10] << hndl; //handle
  mess << eMess[11] << pid; //process
  //check if applciation is real
  if( isCorrectAppName( application, computer) )
     mess << eMess[16] << application; //of application
  else {
    if( application.length() > 1) //not for process
       mess << eMess[6] << swObj; //of software object
  }
  if(!computer.empty())
    mess << eMess[7] << computer; //on host
  mess << eMess[8] << client; //run by   client
  mess << eMess[9] << p; //in partition 
  if( !hwObj.empty() ) {
     mess << eMess[14] << hwObj; //for hw obj
     mess << eMess[15] << hwCls; //in class
  }
  mess << std::endl;
}

void daq::rmgr::RM_Control::putHandleMess(  std::ostringstream& mess, std::vector<RM_AllocatedResource>& resLst, bool isTot, const std::string partition ) {
   for( auto& i : resLst ) {
	   if( !isTot && i.getPartition() != partition)
		   continue;
      putHandleMess( mess, i.getComputer(), i.getSwObj(), i.getApplication(), i.getClient(),
                    i.getPartition(), i.getPid(), i.getHandle(),
                    i.getHwObj(), i.getHwClass()  );
   }
}

bool daq::rmgr::RM_Control::isResourceAvailable(const daq::rmgr::RM_Resource& res, const std::string& partition, const std::string& computer,
       const std::string& swObj, const std::string& application, const std::string& client, unsigned long pid // for exception
) {

    bool available = false;
    bool unavailableTot = false;

    std::ostringstream mess; //for exception
            const daq::rmgr::ResourceType resType = res.type();
            const std::string& resID = res.uid();
            
            if(resType == daq::rmgr::ResourceType::HWR) { 
                // Check number of copies per partition
                const long allocPerPart = getComputerAllocatedPP(resID, partition, computer);
                if( allocPerPart < res.maxPP() ) {
                    // Check total number
                    const long allocTOT = getTotalAllocatedPerComputer(resID, computer);
                    if( allocTOT < res.maxTot() ) {
                        available = true;
                    }
                    else { // tot not avail.
                        putNoResourceMess( mess, resID, computer, true);
                        putHandleMess( mess, computer, swObj, application,  client, partition, pid, 0,"","");// requestor info
                        putResStatMess( mess, res, allocTOT, true);
			unavailableTot = true;
                    }
                }
                else {  //pp no avail.
                  putNoResourceMess( mess, resID, computer, false);
                  putHandleMess( mess, computer, swObj, application, client, partition, pid, 0,"","");
                  putResStatMess( mess, res, allocPerPart, false);
                }
            } else if(resType == ResourceType::SWR) {
                // Check number of copies per partition
                const long allocPerPart = getAllocatedPP(resID, partition);
                if( allocPerPart < res.maxPP() ) {
                    // Check total number
                    const long allocTOT = getTotalAllocated(resID);
                    if( allocTOT < res.maxTot() ) {
                        available = true;
                    }
                    else { // tot not avail.
                        putNoResourceMess( mess, resID, "", true);
                        putHandleMess( mess, computer, swObj, application, client, partition, pid, 0,"","");
                        putResStatMess( mess, res, allocTOT, true);
			unavailableTot = true;
                   }
                }
                else {  //pp no avail.
                  putNoResourceMess( mess, resID, "",  false);
                  putHandleMess( mess, computer, swObj, application, client, partition, pid, 0,"","");
                  putResStatMess( mess, res, allocPerPart, false);
                }
            }
            if( !available) {
              mess << eMess[13] << std::endl;
              //get info about holders of allocated resources and put to message for exception
              if(resType == ResourceType::HWR) {
                  std::vector<RM_AllocatedResource> hInf = computerResourceInfo( computer,resID );
                  putHandleMess( mess, hInf, unavailableTot, partition );
              }
              else {
                std::vector<RM_AllocatedResource> hInf1 = resourceInfo( resID );
                putHandleMess( mess, hInf1, unavailableTot, partition );
              }
              throw daq::rmgr::UnavailableResourcesE( mess.str().c_str() );
            }
            return available;
}

bool daq::rmgr::RM_Impl::freeResource(long handle) {
   return _rm_ctrl.removeResource(handle);
}

bool daq::rmgr::RM_Impl::freeResource(const std::string& partition, const std::string& computer, long pid) {
   return _rm_ctrl.removeResource(partition, computer, pid);
}
//AI 030624
bool daq::rmgr::RM_Impl::freeResource(const std::string& computer, long pid) {
   return _rm_ctrl.removeResource(computer, pid);
}

bool daq::rmgr::RM_Impl::hasResources(const std::string swobjId) const {
  return ( _swobj_map.find(swobjId) != _swobj_map.end() );
}


daq::rmgr::HandleAndTime daq::rmgr::RM_Impl::allocateResources( const std::string& partitionName,
                        const std::string& softwareUID,
			const std::string& application, //AI 030624
                        const std::string& computer,
                        unsigned long pid,
                        const std::string& client,
			long long timeCreation) {
    return allocateResources(partitionName, softwareUID, application, computer, pid, client, timeCreation,
                      "Computer", "");
} 

//now this method is private
daq::rmgr::HandleAndTime daq::rmgr::RM_Impl::allocateResources(const std::string& partition,
                              const std::string& swObject,
			      const std::string& application, //AI 030624
                              const std::string& computer,
                              unsigned long pid,
                              const std::string& client,
                              long long timeCreation,
                              const std::string& hwClass,
                              const std::string& hwId ) {
            long toReturn = 0;
            const auto& it = _swobj_map.find(swObject);
            if(it == _swobj_map.end()) {
                //check already in upper call stack, but for any case
                //return 0; //no resources
                ers::log( daq::rmgr::SoftwareNotFound( ERS_HERE, swObject ) );
                throw daq::rmgr::SoftwareNotFoundE( swObject.c_str() ); 
            }
            std::vector<RM_Resource>& rlst = it->second;
            //check if all resources are available
            for( unsigned int i=0; i<rlst.size(); i++) {
                RM_Resource& r = rlst[i];
                _rm_ctrl.isResourceAvailable( r, partition, computer, swObject, application, client, pid);
            }
            toReturn = nextHandle();
	    //AI oct24
	    long long timeCreation1 = timeCreation;
            daq::rmgr::HandleAndTime retv;
	    retv.hndl = toReturn;

            for(  unsigned int i1=0; i1<rlst.size(); i1++) {
                RM_Resource& r1 = rlst[i1];
                const RM_AllocatedResource allocRes( toReturn, r1.uid(), swObject,
				                     application, //AI 030624
				                     computer,
                                                     partition, pid, client, timeCreation
                                                     , hwClass, hwId);
                _rm_ctrl.addResource(allocRes);
		if( timeCreation == 0 )
			timeCreation1 = allocRes.getAllocationTime();
            }
	    retv.time = timeCreation1;
            return retv;
}

//AI 030624
void daq::rmgr::RM_Impl::setProcess( long handle, unsigned long pid ) {
   _rm_ctrl.updateProcess( handle, pid );
/*
   auto range = _rm_ctrl.equal_range( handle );
   for (auto it = range.first; it != range.second; ++it) {
        it->second.pid = pid;
   }

   daq::rmgr::Handle_Info* inf = daq::rmgr::RM_Impl::getHandleInfo(long handle);
   std::vector<RM_AllocatedResource> resLst = _rm_ctrl.handleResources( handle );
   for( auto& i : resLst ) {
                   const RM_AllocatedResource allocRes( inf.getHandle(), i.uid(), inf.getSwObject(),
                                                     inf.getApplication(),
                                                     inf.getComputer(),
                                                     inf.getPartition(), pid, inf.getClient(),
                                                     , hwClass, hwId);
                _rm_ctrl.addResource(allocRes);
*/
}

std::string generateVirtSWobjId(const std::string& computer, unsigned long pid) {
    std::string retv = "@" + computer;
    std::ostringstream txt;
    txt << pid << retv;
    retv = txt.str();
    return retv;
}

daq::rmgr::HandleAndTime daq::rmgr::RM_Impl::allocateResources4Process( const std::string& partition, 
                                                  const daq::rmgr::Res_ID_list& res_lst,
						  const std::string& application, //AI 030624
                                                  const std::string& computer, unsigned long pid, 
                                                  const std::string& client
						  , long long timeCreation) {
      long long timeCreation1 = timeCreation;
      if( res_lst.length() < 1 ) {
           ers::log( daq::rmgr::ResourceNotFound( ERS_HERE, "" ) );
           throw daq::rmgr::ResourceNotFoundE("");
      }
      if( res_lst.length() > 1) {
          //several resources: check if all available first
          for(unsigned int j = 0; j<res_lst.length(); j++) {
              std::string res_j( res_lst[j]) ;
              const auto& it = _conf_res_lst.find(res_j);
              if(it == _conf_res_lst.end()) {
                    ers::log( daq::rmgr::ResourceNotFound( ERS_HERE, res_j ));
                    throw daq::rmgr::ResourceNotFoundE( res_j.c_str() );
              }
              RM_Resource& r = it->second;
              _rm_ctrl.isResourceAvailable( r, partition, computer, "", application, client, pid);
          }
      }
      long retv0 = 0;
      std::string swVirtId = generateVirtSWobjId(computer, pid);
      if(res_lst.length() > 1)
         retv0 =  nextHandle();
      //AI oct24
      daq::rmgr::HandleAndTime retv;
      retv.hndl = retv0;

      for(unsigned int i = 0; i<res_lst.length(); i++) {
	  long long tt = timeCreation1;
          std::string res_i( res_lst[i] );
          const auto& rit = _conf_res_lst.find(res_i);
          if(rit == _conf_res_lst.end()) {
                 ers::log( daq::rmgr::ResourceNotFound( ERS_HERE, res_i.c_str() ) );
                 throw daq::rmgr::ResourceNotFoundE( res_i.c_str() );
          }
          RM_Resource& rr = rit->second;

          if(res_lst.length() == 1) {
              _rm_ctrl.isResourceAvailable( rr, partition, computer, swVirtId, application, client, pid); 
              retv0 =  nextHandle();
	      retv.hndl = retv0; //AI oct24
          }
          //now allocate
	  std::string appl = application.length() == 0 ? swVirtId : application;  //AI 030624
          const RM_AllocatedResource allocRes( retv0, rr.uid(),
                                          swVirtId,
					  appl, //AI 030624
                                          computer, partition, pid, client, tt);
	  if(timeCreation1 == 0)
		 timeCreation1 =  allocRes.getAllocationTime();
         _rm_ctrl.addResource(allocRes);

      }
      retv.time = timeCreation1;
      return retv;
}

daq::rmgr::Handle_Info* daq::rmgr::RM_Impl::getHandleInfo(long handle) {
    daq::rmgr::Handle_Info* info = new daq::rmgr::Handle_Info();
    
    std::vector<RM_AllocatedResource> resLst = _rm_ctrl.handleResources( handle );
    const std::string empty("");
    //check if empty
    if(resLst.size() == 0) {
       info->handle = 0;
       setMemStr(info->swObj, empty);
       setMemStr(info->application, empty); //AI 030624
       setMemStr(info->computer, empty);
       setMemStr(info->partition, empty);
       setMemStr(info->client, empty);
       info->pid = 0;
       info->allocTime = 0;
       info->resDetails.length(0);

       return info;
    }
    int ii = 0;
    for( auto& i : resLst ) {
       if( ii == 0 ) {
          info->handle = i.getHandle();
          setMemStr(info->swObj,i.getSwObj());
	  setMemStr(info->application, i.getApplication()); //AI 030624
          setMemStr(info->computer,i.getComputer());
          setMemStr(info->partition, i.getPartition());
          setMemStr(info->client, i.getClient());
          info->pid =  i.getPid();
	  info->allocTime = i.getAllocationTime();
          info->resDetails.length( resLst.size() );
       }
       const RM_Resource r = getResource( i.getID() );
       setMemStr(info->resDetails[ii].resource, r.uid());
       info->resDetails[ii].type = r.type();
       info->resDetails[ii].maxPP = r.maxPP();
       info->resDetails[ii].maxTotal = r.maxTot();
       info->resDetails[ii].allocPP = _rm_ctrl.allocPP( r, i );
       info->resDetails[ii].allocTotal = _rm_ctrl.allocTot( r, i );
       ii++;
    }
    return info;
}

daq::rmgr::Rmstr_lst*  daq::rmgr::RM_Impl::getPartitionsList() {
  daq::rmgr::Rmstr_lst* list = new daq::rmgr::Rmstr_lst();
  std::vector<std::string> lst =  _rm_ctrl.getPartitions();
  list->length( lst.size() );
  int j = 0;
  for( auto& i : lst )
     setElemStr( (*list)[j++], i);
  return list;
}

daq::rmgr::Rmstr_lst*  daq::rmgr::RM_Impl::getResourceOwners(const char* rname, const char* pname) {
	daq::rmgr::Rmstr_lst_var list0 = new daq::rmgr::Rmstr_lst();
   daq::rmgr::Rmstr_lst* list = new daq::rmgr::Rmstr_lst();
   std::string p(pname);
   std::string r(rname);
   std::vector<std::string> lst =  _rm_ctrl.getResourceOwners( r, p );
   list->length( lst.size() );
   int j = 0;
   for( auto& i : lst )
     setElemStr( (*list)[j++], i);
  return list;
}

daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getResourceInfo(const char* p, const char* rn) {
   daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
   std::string pn(p);
   std::string r(rn);
   std::vector<RM_AllocatedResource> lst = _rm_ctrl.resourceInfo( pn, r );
   info->length( lst.size() );
   int j(0);
   for( auto& i : lst ) {
          const RM_Resource r = getResource( i.getID() );
          (*info)[j].handle = i.getHandle();
          setMemStr((*info)[j].resource, i.getID());
          (*info)[j].type = r.type();
          (*info)[j].maxPP = r.maxPP();
          (*info)[j].maxTotal = r.maxTot();
          setMemStr((*info)[j].hwClass, r.hwCls());
          setMemStr((*info)[j].swObj, i.getSwObj());
          setMemStr((*info)[j].partition, i.getPartition());
          setMemStr((*info)[j].client, i.getClient());
          setMemStr((*info)[j].computer, i.getComputer());
          (*info)[j].pid = i.getPid();
          setMemStr((*info)[j].hwId, i.getHwObj());
          (*info)[j].allocPP = _rm_ctrl.allocPP( r, i );
          (*info)[j].allocTotal = _rm_ctrl.allocTot( r, i );
       j++;
    }
    return info;
}

//AI 030624
bool daq::rmgr::RM_Impl::isSoleOwner( const char* p, const std::string& user) {
    std::string partit( p );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.partitionInfo( partit );
    for( auto& i : lst )
        if( i.getClient() != user ) {
	   return  false;
	}
    return true;
}


daq::rmgr::Allocated_Info* daq::rmgr::RM_Impl::createAllocatedInfo(std::vector<RM_AllocatedResource>& lst, daq::rmgr::Allocated_Info* info) {
   info->length( lst.size() );
   int j(0);
    for( auto& i : lst ) {
          const RM_Resource r = getResource( i.getID() );
          (*info)[j].handle = i.getHandle();
          setMemStr((*info)[j].resource, i.getID());
          (*info)[j].type = r.type();
          (*info)[j].maxPP = r.maxPP();
          (*info)[j].maxTotal = r.maxTot();
          setMemStr((*info)[j].hwClass, r.hwCls());
          setMemStr((*info)[j].swObj, i.getSwObj());
	  setMemStr((*info)[j].application, i.getApplication()); //AI 030624
          setMemStr((*info)[j].partition, i.getPartition());
          setMemStr((*info)[j].client, i.getClient());
          setMemStr((*info)[j].computer, i.getComputer());
          (*info)[j].pid = i.getPid();
          setMemStr((*info)[j].hwId, i.getHwObj());
          (*info)[j].allocPP = _rm_ctrl.allocPP( r, i );
          (*info)[j].allocTotal = _rm_ctrl.allocTot( r, i );
	  (*info)[j].allocTime = i.getAllocationTime();
       j++;
    }
    return info;
}

daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getResourceInfo( const std::string& res) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string r(res);
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.resourceInfo( r );
    return createAllocatedInfo( lst, info );
}
daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getPartitionInfo( const std::string& p ) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string partit( p );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.partitionInfo( partit );
    return createAllocatedInfo( lst, info );
}
daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getComputerInfo( const std::string& comp ) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string c( comp );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.computerInfo( c );
    return createAllocatedInfo( lst, info );
}
daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getComputerResourceInfo( const std::string& comp,  const std::string& partit, const std::string& res) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string p( partit );
    std::string c( comp );
    std::string r( res );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.computerResourceInfo( c, p, r);
    return createAllocatedInfo( lst, info );
}
daq::rmgr::Allocated_Info*  daq::rmgr::RM_Impl::getApplicationInfo( const std::string& partit, const std::string& swObj, const std::string& comp) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string p( partit );
    std::string c( comp );
    std::string bin( swObj);
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.applicationInfo( p, bin, c );
    return createAllocatedInfo( lst, info );
}

daq::rmgr::Allocated_Info* daq::rmgr::RM_Impl::getPartitionCompProcInfo(const std::string& partition, const std::string& computer, long pid) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string p( partition );
    std::string c( computer );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.partitionCompProcInfo(p,c,pid);
    return createAllocatedInfo( lst, info );
}
//AI 030624
daq::rmgr::Allocated_Info* daq::rmgr::RM_Impl::getCompProcInfo(const std::string& computer, long pid) {
    daq::rmgr::Allocated_Info* info = new daq::rmgr::Allocated_Info();
    std::string c( computer );
    std::vector<RM_AllocatedResource> lst = _rm_ctrl.compProcInfo(c,pid);
    return createAllocatedInfo( lst, info );
}



long daq::rmgr::RM_Control::allocTot( const RM_Resource& r, const RM_AllocatedResource& allocR ) {
    switch (r.type()) {
       case daq::rmgr::SWR :
           return getTotalAllocated( r.uid() );
       case daq::rmgr::HWR :
          if( allocR.getHwClass().length() == 0 )
           return getTotalAllocatedPerComputer( r.uid(), allocR.getComputer() );
          return getHWObjAllocated( r.uid(), allocR.getHwClass(), allocR.getHwObj() );
    }
    return -1;
} 

long daq::rmgr::RM_Control::allocPP( const RM_Resource& r, const RM_AllocatedResource& allocR ) {
    switch (r.type()) {
       case daq::rmgr::SWR :
           return getAllocatedPP( r.uid(), allocR.getPartition() );
       case daq::rmgr::HWR :
////          if( allocR.getHwClass().length() == 0 )
           return getComputerAllocatedPP( r.uid(), allocR.getPartition(), allocR.getComputer() );
////          return getHWObjAllocatedPP( r.uid(), allocR.getPartition(), allocR.getHwClass(), allocR.getHwObj() );
    }
    return -1;
}

bool daq::rmgr::RM_Control::isOwner( const std::string& partit, const std::string& swObj, const std::string& comp, const std::string& client ) {
//test
    std::string p( partit );
    std::string c( comp );
    std::string bin( swObj);
    std::vector<RM_AllocatedResource> lst = applicationInfo( p, bin, c );
    for( auto& it : lst ) {
       if(it._client != client)
          return false;
    }
    return true;
}

//Access Manager related
void daq::rmgr::RM_Impl::setUseAM() {
   if (char * envAM = getenv( "TDAQ_AM_AUTHORIZATION" ))
     _use_am_auth = (strcmp(envAM, "on") == 0);
   else
     _use_am_auth = 1;

   ERS_LOG("Access Manager is " << (_use_am_auth ? "enabled" : "disabled"));
}

bool  daq::rmgr::RM_Impl::checkAccesPermission( const std::string mname, const std::string mhost, const char * function_name) {
  bool allowed(false);
  if( isUseAM() ) {
    try {
       //check permission by AM
       daq::am::ServerInterrogator si;
       const daq::am::RMResource *rmRes  = daq::am::RMResource::getInstanceForActionANY("any");
       daq::am::RequestorInfo pAccessSubject( mname, mhost );
       allowed = si.isAuthorizationGranted(*rmRes, pAccessSubject);
       delete rmRes;
    }
    catch(const am::Exception &ex) {
       ers::warning(ex);
       allowed = false;
    }
  }
  else
    allowed = true;

  if(!allowed) {
    const std::string user =  !daq::tokens::enabled() && mname.length() > 40
               ? "\"unknown user\"" : mname;
    ers::log( daq::rmgr::AccessManagerDontAllow( ERS_HERE, function_name, mhost, user ) );
    throw  daq::rmgr::AcceessAnyDeniedE( function_name, mhost.c_str(), user.c_str() );
  }
  return allowed;
}

std::string daq::rmgr::RM_Impl::getUser( const std::string clientDaqTok) {
   try{
      return daq::tokens::enabled() ? daq::tokens::verify(clientDaqTok).get_subject() : std::string(clientDaqTok);
   }
   catch( const daq::tokens::CannotVerifyToken& ex ) {
        throw daq::rmgr::CannotVerifyTokenE(daq::ers2idl_issue(ex));
   }
   catch( const std::runtime_error& ex) {
        throw daq::rmgr::CannotVerifyTokenE(daq::ers2idl_issue( ers::Message( ERS_HERE, "Runtime error", ex )) );
   }
}

//AI 030624 RM_AllocatedResource constructors moved here
    daq::rmgr::RM_AllocatedResource::RM_AllocatedResource()
         : _handle(0), _id(""), _swObj(""), _application(""),_computer(""),
        _partition(""), _pid(0), _client(""),
        _allocTime(0),
        _hwClass("Computer"),
        _hwObj("") {
		//_allocTime = (long) time(NULL); //AI 030624  time(&_tcreation);
		_allocTime = createAllocTime();
    }
    daq::rmgr::RM_AllocatedResource::RM_AllocatedResource( long handle, std::string id,  std::string swObj,
         std::string applic,
         std::string computer, std::string partition,  unsigned  long pid,
         std::string client,
	 long long timeCreation,
         std::string hwClass,  std::string hwObj)
      : _handle(handle), _id(id), _swObj(swObj), 
	_application(applic), //AI 030624
	_computer(computer),
        _partition(partition), _pid(pid), _client(client),
        //AI 030624 _callstamp(callstamp),
        _hwClass(hwClass),
        _hwObj(hwObj) {
		_allocTime = timeCreation > 0 ? timeCreation : createAllocTime();
    }


//backup manager related
bool daq::rmgr::RM_Impl::isDoBackup() {
   return _bck && _bck->isDoBackup();
}
const std::string daq::rmgr::RM_Impl::getBckPath() const {
   if(_bck)
     return _bck->getBckPath();
   return (const std::string)"";
}
bool daq::rmgr::RM_Impl::isRestoreBackup() {
   return _bck && _bck->isRestoreBackup();
}

std::ostream& daq::rmgr::operator<<(std::ostream& str, daq::rmgr::RM_AllocatedResource& alloc) {
  str << "RM_AllocatedResourc= { handle="<<alloc._handle<<"  res="<<alloc._id<<"  swObj="<<alloc._swObj<<"  computer="<<alloc._computer<<"  partition="<<alloc._partition<<"  pid="<<alloc._pid<<"  client="<<alloc._client<<"  hwClass="<<alloc._hwClass<<"  hwObj="<<alloc._hwObj <<" }";
 return str;  
}

