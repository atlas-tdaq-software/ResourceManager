#include <time.h>
#include <regex>

#include "src/RM_ClientImpl.h"
#include <system/Host.h>
#include <system/User.h>

#include "ers/ers.h"
#include <dal/SW_Object.h>
#include <dal/RM_Resource.h>
#include <dal/RM_SW_Resource.h>
#include <dal/RM_HW_Resource.h>
#include <ers2idl/ers2idl.h>
#include <ipc/exceptions.h>

static std::mutex lookup_mutex;

daq::rmgr::RM_ClientImpl::RM_ClientImpl():m_defpart(),m_rmDbName("RM_Server")
{
}

daq::rmgr::RM_ClientImpl::RM_ClientImpl(const char *sid):m_defpart(),m_rmDbName(sid)
{
}

std::vector<std::string> daq::rmgr::RM_ClientImpl::getPartitionsList(){
  lookupRMDDB();
  std::vector<std::string> retv;

  try {
    Rmstr_lst_var list( RMDB->getPartitionsList() );
    for (size_t i = 0; i< list->length(); i++ )
      retv.push_back( static_cast<const char*>((list)[i]) );
    return retv;
  }
  catch(CORBA::SystemException & excorb) {
              throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

long daq::rmgr::RM_ClientImpl::requestResources (const std::string& partition, const std::string& swobjectid, const std::string& computer, const std::string& userOwner, const std::string& application,  unsigned long pid) {
//AI 030624
     //unsigned long mypid =  getpid();
     //std::string no_client("NO");
     //std::string client(clientName);
     //if( client == no_client) {
     std::string user(userOwner);
     if( user.length() == 0 )
       user = ::System::User().name_safe();
     std::string appl = application.length() == 0 ? (swobjectid + "@" + computer) : application;

///     return requestResources ( partition, swobjectid, computer, mypid, client.c_str() );
///}
///
/// long daq::rmgr::RM_ClientImpl::requestResources (const std::string& partition, const std::string& swobjectid, const std::string& computerName, unsigned long mypid, const std::string& clientName) {

     lookupRMDDB();

   try {
	//AI 030624   
        //return RMDB->requestResources ( partition.c_str(), swobjectid.c_str(), computer.c_str(), mypid, clientName.c_str());
	return RMDB->requestResources( getResOwnerInfo( user, computer, pid, appl, partition), swobjectid.c_str());
   }
   catch (const daq::rmgr::UnavailableResourcesE  & exr) {
           throw daq::rmgr::UnavailableResources ( ERS_HERE, exr.refused_msg.in() );
   }
   catch (const daq::rmgr::SoftwareNotFoundE  & ex) {
           throw daq::rmgr::SoftwareNotFound( ERS_HERE, ex.swobj.in() );
   }
   catch(CORBA::SystemException & excorb) {
              throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
   }

}
//AI 030624
void daq::rmgr::RM_ClientImpl::setProcess( long handle,  unsigned long pid) {
  if( handle <= 0)
      return;  //warning?
  if( pid <= 0 )
      return;
  lookupRMDDB();
  try {
       return RMDB->setProcess( handle, pid );
  }
  catch(CORBA::SystemException & excorb) {
              throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}


void daq::rmgr::RM_ClientImpl::freeResources (long handleId) { //AI 030624 , bool log_this_call ) {
  lookupRMDDB();
  //::CORBA::Boolean add2log(log_this_call);
  const daq::rmgr::CallerInfo& caller = getCallerInfo();

   try {
        RMDB->freeResources( handleId, caller); // add2log, caller);
   }
   catch(CORBA::SystemException & excorb) {
              throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
   }
}

void daq::rmgr::RM_ClientImpl::freeAllInPartition( const std::string&  p ) {
       daq::rmgr::ClientRequest request(daq::rmgr::RequestType::FreeAllInPartition, p.c_str(),true); //AI 030624 , true);
       performRequest(request);
}

void  daq::rmgr::RM_ClientImpl::freePartitionResource(const std::string&  p, const std::string& resource) { //AI 030624 , bool log_this_call) {
      daq::rmgr::ClientRequest request( daq::rmgr::RequestType::FreePartitionResource, p.c_str(), resource.c_str() ); //, log_this_call );
      performRequest(request);
}

void  daq::rmgr::RM_ClientImpl::freeAllOnComputer(const std::string& computer) { //, bool log_this_call) {
       daq::rmgr::ClientRequest request(daq::rmgr::RequestType::FreeAllOnComputer, computer.c_str(), true); //log_this_call, true);
       performRequest(request);
}

void  daq::rmgr::RM_ClientImpl::freeResource(const std::string& resource) { //, bool log_this_call) {
       daq::rmgr::ClientRequest request(daq::rmgr::RequestType::FreeResource, resource.c_str()); //, log_this_call);
       performRequest(request);
}

void  daq::rmgr::RM_ClientImpl::freeComputerResource(const std::string& resource, const std::string& computer, const std::string&  p) { //, bool log_this_call) {
       daq::rmgr::ClientRequest request(daq::rmgr::RequestType::FreeComputerResource, resource.c_str(), computer.c_str(), p.c_str()); //, log_this_call);
       performRequest(request);
}

void daq::rmgr::RM_ClientImpl::freeApplicationResources(const std::string& p, const std::string& swObj, const std::string& computer) { //, bool log_this_call) {
       daq::rmgr::ClientRequest request(daq::rmgr::RequestType::FreeApplicationResources, p.c_str(), swObj.c_str(), computer.c_str(), true); // log_this_call, true);
       performRequest(request);
}

std::vector<std::string> daq::rmgr::RM_ClientImpl::getResourceOwners (const std::string& rname, const std::string& p){
    lookupRMDDB();
    std::vector<std::string> retv;
    try {
	 Rmstr_lst_var list( RMDB->getResourceOwners( rname.c_str(), p.c_str() ) );
         for (size_t i = 0; i< list->length(); i++ )
           retv.push_back( static_cast<const char*>((list)[i]) );
         return retv;
    }
    catch(CORBA::SystemException & excorb) {
           throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
    }
}

void daq::rmgr::RM_ClientImpl::lookupRMDDB() {
  std::scoped_lock scoped_lock( lookup_mutex );
  try {
    RMDB = m_defpart.lookup< ::rmgr::ResMgr>(m_rmDbName.c_str());
  }
  catch (const daq::ipc::Exception & exipc) {
       throw daq::rmgr::LookupFailed( ERS_HERE, exipc);
      // throw daq::rmgr::CommunicationException( ERS_HERE, "IPC exception", exipc);
  }
  catch(CORBA::SystemException & excorb) {
       throw daq::rmgr::CommunicationException( ERS_HERE, "Communication exception in lookup: ", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

long daq::rmgr::RM_ClientImpl::requestResourceForMyProcess(
                        const std::string& partition,
                        const std::string& resource,
                        const std::string& computer,
                        const std::string& clientName) {
   std::ostringstream text;
   unsigned long mypid =  getpid();
   return requestResource( partition, resource, computer, clientName, mypid );
}

long daq::rmgr::RM_ClientImpl::requestResource(
                        const std::string& partition,
                        const std::string& resource,
                        const std::string& computer,
                        const std::string& clientName, 
                        int process_id ) {
   lookupRMDDB();
   std::ostringstream text;
   daq::rmgr::Res_ID_list res_lst;
   res_lst.length(1);
   res_lst[0] = CORBA::string_dup(resource.c_str());

   try {
        return RMDB->requestResourcesForProcess(partition.c_str(), res_lst, computer.c_str(), process_id, clientName.c_str());
   }
   catch (const daq::rmgr::UnavailableResourcesE  & exr) {
           throw daq::rmgr::UnavailableResources ( ERS_HERE,  exr.refused_msg.in() );
   }
   catch (const daq::rmgr::ResourceNotFoundE & excres) {
           throw daq::rmgr::ResourceNotFound ( ERS_HERE, excres.rid.in() );
   }
   catch( CORBA::SystemException & excorb) {
               throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
   }
}


void daq::rmgr::RM_ClientImpl::freeProcessResources(
                        const std::string& partition,
                        const std::string& computer,
                        unsigned long pid
                        //AI 030624 ,bool log_this_call
) {
  lookupRMDDB();
  //::CORBA::Boolean add2log(log_this_call);
  const daq::rmgr::CallerInfo& caller = getCallerInfo();

  try {
        return RMDB->freeProcessResources(partition.c_str(), computer.c_str(), pid, caller); //add2log, caller);
  }
  catch(CORBA::SystemException & excorb) {
            throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

void daq::rmgr::RM_ClientImpl::freeAllProcessResources( long handleId,
                        //AI 030624 removed const std::string& partition,
                        const std::string& computer,
                        unsigned long pid
                        //AI 030624 ,bool log_this_call
) {
   lookupRMDDB();
   //::CORBA::Boolean add2log(log_this_call);
   const daq::rmgr::CallerInfo& caller = getCallerInfo();

  try {
        return RMDB->freeAllProcessResources( handleId, /*partition.c_str(),*/  computer.c_str(), pid, caller ); //add2log, caller );
  }
  catch(CORBA::SystemException & excorb) {
            throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

/*
Configuration* daq::rmgr::RM_ClientImpl::getConfiguration( const std::string& dbname ) {
   std::ostringstream text;
   Configuration * sw_conf = 0;
   try{
        sw_conf = new Configuration(dbname);
   }
   catch (daq::config::Exception & ex) {
      delete sw_conf;
      text << ex;
      throw daq::rmgr::ConfigurationException( ERS_HERE, text.str().c_str()  );
   }
   return sw_conf;
}


//create and fill daq::rmgr::Config_update structure
void daq::rmgr::RM_ClientImpl::setRMData(::Configuration * conf, daq::rmgr::Config_update & rm_conf) {
  const std::string compHW("Computer");
  //get all resources
  std::vector<const daq::core::RM_Resource *> resources;
  conf->get(resources);
  int iswr = 0, ihwr = 0;
  //calculate number of each type
  for(const auto& ir0 : resources) {
    if(ir0->castable(&daq::core::RM_SW_Resource::s_class_name))
      iswr++;
    else if(ir0->castable(&daq::core::RM_HW_Resource::s_class_name))
      ihwr++;
  }
  //now add resources
   rm_conf.swr_lst.length( iswr); //if 0?
   rm_conf.hwcr_lst.length( ihwr );
   iswr = ihwr = 0;
  for (const auto& ir : resources) {
    if (const daq::core::RM_SW_Resource* sw_r = ir->cast<daq::core::RM_SW_Resource>()) {
      rm_conf.swr_lst[iswr].rid = CORBA::string_dup( sw_r->UID().c_str() );
      rm_conf.swr_lst[iswr].max_total = sw_r->get_MaxCopyTotal();
      rm_conf.swr_lst[iswr++].max_pp = sw_r->get_MaxCopyPerPartition();
    }
    else if (const daq::core::RM_HW_Resource* hw_r = ir->cast<daq::core::RM_HW_Resource>()) {
      rm_conf.hwcr_lst[ihwr].rid = CORBA::string_dup(hw_r->UID().c_str());
      rm_conf.hwcr_lst[ihwr].max_total = hw_r->get_MaxCopyTotal();
      rm_conf.hwcr_lst[ihwr].max_pp = hw_r->get_MaxCopyPerPartition();
      //if Computer - set as empty
      if (compHW != hw_r->get_HardwareClass())
        rm_conf.hwcr_lst[ihwr].hwClass = CORBA::string_dup( hw_r->get_HardwareClass().c_str());
      else
        rm_conf.hwcr_lst[ihwr].hwClass = CORBA::string_dup( "" );
      ihwr++;
    }
  }

 //sw objects
  std::vector<const daq::core::SW_Object *> sw_objects;
  conf->get(sw_objects);
  //send all sw_objects
  int  inum = 0;
  rm_conf.swobj_lst.length( sw_objects.size() );
  for (const auto& j : sw_objects) {
     rm_conf.swobj_lst[inum].swid = CORBA::string_dup( j->UID().c_str() );
     int len = j->get_Needs().size();
     rm_conf.swobj_lst[inum].res_list.length(len );
     //add res references
     for ( size_t ii=0; ii < j->get_Needs().size(); ii++ )
        rm_conf.swobj_lst[inum].res_list[ii].id = CORBA::string_dup( j->get_Needs()[ii]->UID().c_str());
     inum++;
  }
}
*/

void daq::rmgr::RM_ClientImpl::rmConfigurationUpdate ( const std::string& partition ) {
  lookupRMDDB();
  try {
    const std::string user = ::System::User().name_safe();
    RMDB->rmConfigUpdate( partition.c_str(), user.c_str() );
  }
  catch( const daq::rmgr::ConfigExceptionE& ex ) {
        throw daq::rmgr::ConfigurationException( ERS_HERE, *daq::idl2ers_issue(ex.issue) );
  }
  catch(CORBA::SystemException & excorb) {
       std::string tmpstr = excorb._name();
       if(tmpstr == "UNKNOWN") {
           daq::rmgr::ConfigurationException ex(ERS_HERE,
                                                std::string("The RM server configuration could not be updated because inconsistent and/or corrupted; ") +
                                                    std::string("please, look at the RM server log for additional details"),
 ers::Message( ERS_HERE, "Corba exception" + static_cast<const std::string>( excorb._name() ) ) );
           throw ex;
       }
       throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

daq::rmgr::HandleInfo
daq::rmgr::RM_ClientImpl::getHandleInfo ( long handleId ){
  lookupRMDDB();

   try {
	std::unique_ptr<Handle_Info> inf( RMDB->getHandleInfo(handleId) );
	Handle_InfoHelper hlp(inf.get());
        HandleInfo hi( hlp.getHandleInfo());
	return hi;
   }
   catch(CORBA::SystemException & excorb) {
           throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
   }
}

daq::rmgr::AllocatedInfo
daq::rmgr::RM_ClientImpl::getPartitionInfo (const std::string& partition) {
  lookupRMDDB();
  const daq::rmgr::CallerInfo& caller = getCallerInfo(true);

  try {
     std::unique_ptr<Allocated_Info> inf( RMDB->getPartitionInfo( partition.c_str(), caller ) );
     Allocated_InfoHelper  hlp(inf.get());
     AllocatedInfo ai( hlp.getAllocatedInfo());
     return ai;
  }
  catch(CORBA::SystemException & excorb) {
           throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
  catch( const daq::rmgr::CannotVerifyTokenE& ex ) {
        throw daq::rmgr::CannotVerifyToken( ERS_HERE, *daq::idl2ers_issue(ex.issue) );
  }
  catch( const daq::rmgr::AcceessAnyDeniedE& exdenied) {
        throw daq::rmgr::AcceessAnyDenied( ERS_HERE, exdenied.user.in(), exdenied.host.in() );
  }

}

daq::rmgr::AllocatedInfo
daq::rmgr::RM_ClientImpl::getResourceInfo(const std::string& partition,
                                   const std::string& rname) {
  lookupRMDDB();

  try {
       std::unique_ptr<Allocated_Info> inf( RMDB->getResourceInfo( partition.c_str(), rname.c_str()) );
       Allocated_InfoHelper  hlp(inf.get());
       AllocatedInfo ai( hlp.getAllocatedInfo());
       return ai;
  }
  catch(CORBA::SystemException & excorb) {
           throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

daq::rmgr::AllocatedInfo daq::rmgr::RM_ClientImpl::getFullResInfo(const std::string& rname) {
  lookupRMDDB();

  try {
     std::unique_ptr<Allocated_Info> inf( RMDB->getFullResInfo(rname.c_str()) );
     Allocated_InfoHelper  hlp(inf.get());
     AllocatedInfo ai( hlp.getAllocatedInfo());
     return ai;
  }
  catch(CORBA::SystemException & excorb) {
           throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

daq::rmgr::AllocatedInfo daq::rmgr::RM_ClientImpl::getComputerInfo(const std::string& computer) {
  lookupRMDDB();

  try {
     std::unique_ptr<Allocated_Info> inf( RMDB->getComputerInfo(computer.c_str()) );
     Allocated_InfoHelper  hlp(inf.get());
     AllocatedInfo ai( hlp.getAllocatedInfo());
     return ai;
  }
  catch(CORBA::SystemException & excorb) {
             throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

const daq::rmgr::AllocatedInfo daq::rmgr::RM_ClientImpl::getComputerResourceInfo(const std::string& computer, const std::string& partition, const std::string& resource) {
  lookupRMDDB();

  try {
    std::unique_ptr<Allocated_Info> cinf(RMDB->getComputerResourceInfo(computer.c_str(), partition.c_str(), resource.c_str()));
    Allocated_InfoHelper hlp(cinf.get());
    daq::rmgr::AllocatedInfo update(hlp.getAllocatedInfo());
    return update;
  }
  catch(CORBA::SystemException & excorb) {
            throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}

void daq::rmgr::RM_ClientImpl::performRequest(daq::rmgr::ClientRequest& req) {
  lookupRMDDB();
  ////AI 030624 ::CORBA::Boolean add2log(req._log_this_call);
  const daq::rmgr::CallerInfo& caller = req._useDaqToken ? getCallerInfo(true) : getCallerInfo();

  daq::rmgr::RequestType type(req.type);

  try {
        switch (type) {
          case daq::rmgr::RequestType::FreeAllInPartition:
              RMDB->freeAllInPartition( req.param1, caller ); //AI 030624 ;freePartition( req.param1, caller );
              break; 
          case daq::rmgr::RequestType::FreeAllOnComputer:
              RMDB->freeAllOnComputer( req.param1, caller ); //AI 030624 freeComputerAll( req.param1, caller ); //add2log, caller );
              break;
          case daq::rmgr::RequestType::FreeResource:
              RMDB->freeResource( req.param1, caller ); //add2log, caller );
              break;
          case daq::rmgr::RequestType::FreePartitionResource:
              RMDB->freePartitionResource( req.param1, req.param2, caller ); //add2log, caller );
              break;
          case daq::rmgr::RequestType::FreeComputerResource:
              RMDB->freeComputerResource(  req.param1, req.param2, req.param3, caller ); //add2log, caller );
              break;
         case daq::rmgr::RequestType::FreeApplicationResources:
              RMDB->freeApplicationResources(  req.param1, req.param2, req.param3, caller ); //add2log, caller );
              break;
        }
  }
  catch( const daq::rmgr::CannotVerifyTokenE& ex ) {
        throw daq::rmgr::CannotVerifyToken( ERS_HERE, *daq::idl2ers_issue(ex.issue) );
  }
  catch( const daq::rmgr::AcceessAnyDeniedE& exdenied) {
        throw daq::rmgr::AcceessAnyDenied( ERS_HERE, exdenied.user.in(), exdenied.host.in() );
  }
  catch(CORBA::SystemException & excorb) {
              throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }

}

///ONLY FOR TESTS
void daq::rmgr::RM_ClientImpl::rmConfigurationUpdate4MyTest ( ) {
/*
  std::ostringstream text;
  try {
    lookupRMDDB();
    RMDB->rmConfigUpdate( idl_conf );
  }
  catch (const daq::config::Exception & ex) {
       daq::rmgr::ConfigurationException( ERS_HERE, "Configuration exception", ex );
  }
  catch (const daq::ipc::Exception & exipc) {
       throw daq::rmgr::CommunicationException( ERS_HERE, "IPC exception", exipc);
  }
  catch(CORBA::SystemException & excorb) {
       throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
  */
  rmConfigurationUpdate( "TestPArtition" );
}

daq::rmgr::ConfigUpdate daq::rmgr::RM_ClientImpl::getConfigInfo() {
  lookupRMDDB();
  try{
    std::unique_ptr<Config_update> cinf(RMDB->getConfigInfo());
    Config_updateHelper hlp(cinf.get());
    daq::rmgr::ConfigUpdate update(hlp.getConfigUpdate());
    return update;
  }
  catch(CORBA::SystemException & excorb) {
     throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}




/*
daq::rmgr::Config_updateHelper daq::rmgr::RM_ClientImpl::getConfigInfo() {
  lookupRMDDB();
  unique_ptr<Config_update> cinf = RMDB->getConfigInfo();
  try {
     return new daq::rmgr::Config_updateHelper( RMDB->getConfigInfo() );
  }
  catch( const daq::rmgr::CommunicationException&  excom) {
     throw;
  }
  catch (const daq::ipc::Exception & exipc) {
     throw daq::rmgr::CommunicationException( ERS_HERE, "IPC exception", exipc);
  }
  catch(CORBA::SystemException & excorb) {
     throw daq::rmgr::CommunicationException( ERS_HERE, "CORBA exception", daq::ipc::CorbaSystemException( ERS_HERE, &excorb ) );
  }
}
*/

daq::rmgr::CallerInfo daq::rmgr::RM_ClientImpl::getCallerInfo( bool useDaqTok )
{
  static const std::string s_host(::System::LocalHost().full_name());
  static const std::string s_name(::System::User().name_safe());

  std::string mname;

  try
    {
      mname = (useDaqTok && daq::tokens::enabled()) ? daq::tokens::acquire(daq::tokens::Mode::Reuse) : s_name;
    }
  catch (const daq::tokens::CannotAcquireToken& ex)
    {
      daq::rmgr::CannotAcquireToken issue( ERS_HERE, ex);
      ers::error(issue);
      throw issue;
    }

  daq::rmgr::CallerInfo caller;

  caller.user = CORBA::string_dup(mname.c_str());
  caller.host = CORBA::string_dup(s_host.c_str());

  return caller;
}

daq::rmgr::ResOwnerInfo daq::rmgr::RM_ClientImpl::getResOwnerInfo( const std::string& user, const std::string& host, unsigned long pid, const std::string& application, const std::string& partition) {
  daq::rmgr::ResOwnerInfo owner;
  owner.user = CORBA::string_dup(user.c_str());
  owner.host = CORBA::string_dup(host.c_str());
  owner.pid = pid;
  owner.application = CORBA::string_dup(application.c_str());
  owner.partition = CORBA::string_dup(partition.c_str());
  return owner;
}



