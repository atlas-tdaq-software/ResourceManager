#ifndef idlhelper_H_INCLUDED
#define idlhelper_H_INCLUDED
#include <iostream>
#include <string>
#include <memory>

#include <rmgr/rmgr.hh>
#include <ResourceManager/rminfo.h>

namespace daq
{
namespace rmgr
{

struct Handle_Info;
struct Allocated_Info;
struct  Rmstr_lst;
struct Config_update;

class Handle_InfoHelper {
private:
  friend std::ostream& operator<<(std::ostream& str, Handle_InfoHelper& helper);
public:
  Handle_Info* info;
  Handle_InfoHelper( Handle_Info* inf) : info(inf) {}
//  ~Handle_InfoHelper();
  const std::string getAsString() const;
  const HandleInfo getHandleInfo();
};

std::ostream& operator<<(std::ostream& str, Handle_InfoHelper& helper);

class Allocated_InfoHelper {
private:
  friend std::ostream& operator<<(std::ostream& str, Allocated_InfoHelper& helper);
public:
  Allocated_Info* allocInfo;
  Allocated_InfoHelper( Allocated_Info* inf ) : allocInfo(inf) {}
//  ~Allocated_InfoHelper();
  const AllocatedInfo getAllocatedInfo();
};

std::ostream& operator<<(std::ostream& str, Allocated_InfoHelper& helper);

class Rmstr_lstHelper {
private:
  friend std::ostream& operator<<(std::ostream& str, const Rmstr_lstHelper& helper);
public:
  Rmstr_lst* list;
  Rmstr_lstHelper( std::unique_ptr<Rmstr_lst> lst );
  // ~Rmstr_lstHelper();
  std::vector<std::string> getList();
};

std::ostream& operator<<(std::ostream& str, const Rmstr_lstHelper& helper);

class Config_updateHelper {
private:
  friend std::ostream& operator<<(std::ostream& str, const Config_updateHelper& helper);
public:
  Config_update* cinfo;
  Config_updateHelper( Config_update* inf );
//  Config_updateHelper(  std::unique_ptr<Config_update> inf );
//  ~Config_updateHelper();
  const ConfigUpdate getConfigUpdate();
};

std::ostream& operator<<(std::ostream& str, const Config_updateHelper& helper);

} //rm
} //daq
#endif /* idlhelper_H_INCLUDED */
