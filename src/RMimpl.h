#ifndef RMimpl_H_INCLUDED
#define RMimpl_H_INCLUDED

#include <iostream>
#include <rmgr/rmgr.hh>
#include "ResourceManager/exceptions.h"

#include <config/Configuration.h>
#include <dal/SW_Object.h>
#include <dal/RM_Resource.h>

#include <boost/config.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>

#include <fstream>
#include <iostream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/split_member.hpp>

#include <string>
#include <map>
#include <set>
#include <vector>
#include <memory>
#include <utility>
#include <functional>
#include <chrono>
#include <atomic>
#include <daq_tokens/verify.h>
//#include <ctime>  //AI 030624
#include <chrono>

namespace mi = boost::multi_index;

namespace daq
{
namespace rmgr
{
const std::string compClass = "Computer";
/**
 * Config DB resources
 *
 * **/
class RM_Resource {
 public:
    RM_Resource()
            : _id(""), _type(SWR), _maxTot(0), _maxPP(0) {
    }
    RM_Resource(const std::string& id, long maxTot,long maxPP, daq::rmgr::ResourceType type)
            : _id(id), _type(type), _maxTot(maxTot), _maxPP(maxPP) {
    }
    RM_Resource(const std::string& id, long maxTot,long maxPP, const std::string& hwCls, daq::rmgr::ResourceType type)
            : _id(id), _type(type), _maxTot(maxTot), _maxPP(maxPP), _hwcls(hwCls) {
    }
    daq::rmgr::ResourceType type() const {
      return _type;
    }
    const std::string& uid() const {
      return _id;
    }
    long maxPP() const {
      return _maxPP;
    }
    long maxTot() const {
      return _maxTot;
    }
    const std::string& hwCls() const {
      return _hwcls;
    }
    //below - should be during mute
    void update(long maxTot, long maxPP ) {
      _maxTot = maxTot;
      _maxPP = maxPP;
    }
    void setHwCls(std::string hwcls ) { _hwcls = hwcls; } //AI oct 2023
 private:
  friend class boost::serialization::access;
  template<class Archive>
  void save(Archive & ar, const unsigned int )  const {
      ar & _id;
      ar & _maxTot;
      ar & _maxPP;
      unsigned int tp;
      tp = (unsigned int) _type;
      ar & tp;
      ar & _hwcls;
  }
  template<class Archive>
  void load(Archive & ar, const unsigned int ) {
     ar & _id;
     ar & _maxTot;
     ar & _maxPP;
     unsigned int tp;
     ar & tp;
     _type = (ResourceType) tp;
     ar & _hwcls;
  }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  std::string _id;
  ResourceType _type;
  long _maxTot;
  long _maxPP; 
  std::string _hwcls;
};
//BOOST_CLASS_VERSION(RM_Resource, 1)

/** 
 * This class represent an allocated resource data
 * **/
class RM_AllocatedResource {
friend class RM_Control;
  public:
    RM_AllocatedResource();
    /* AI 030624
         : _handle(0), _id(""), _swObj(""), _computer(""),
        _partition(""), _pid(0), _client(""),
        _callstamp(0),
        _hwClass("Computer"),
        _hwObj("") {
    }*/
    RM_AllocatedResource( long handle, std::string id,  std::string swObj,
	 std::string application, //AI 030624
         std::string computer, std::string partition,  unsigned  long pid,
         std::string client, 
	 long long timeCreation=0, //AI oct24
         std::string hwClass ="",  std::string hwObj="");
    /* AI 030624
      : _handle(handle), _id(id), _swObj(swObj), _computer(computer),
        _partition(partition), _pid(pid), _client(client), 
        _callstamp(callstamp),
        _hwClass(hwClass),
        _hwObj(hwObj) {
    }*/
    long getHandle() const {
      return _handle;
    }
    const std::string& getID() const {
      return _id;
    }
    const std::string& getSwObj() const {
       return _swObj;
    }
    //AI 030624
    const std::string& getApplication() const {
       return _application;
    }
    const std::string& getComputer() const {
       return _computer;
    }
    const std::string& getPartition() const {
       return _partition;
    }
    unsigned  long getPid() {
       return _pid;
    }
    const std::string& getClient() const {
       return _client;
    }
    const std::string& getHwClass() const {
       return _hwClass;
    }
    const std::string& getHwObj() const {
       return _hwObj;
    }
    //AI 030624
    const std::string& getAppliction() const {
       return _application;
    }
    long long getAllocationTime() const {
       return _allocTime;
    }

  private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int) {
      ar & _handle;
      ar & _id;
      ar & _swObj;
      ar & _application; //AI 030624
      ar & _computer;
      ar & _partition;
      ar & _pid;
      ar & _client;
      ar & _allocTime;
      //AI 030624 ar & _callstamp;
      ar & _hwClass;
      ar & _hwObj; 
      ////ar & _allocTime; //ar & _tcreation; //AI 030624
    }
    friend std::ostream& operator<<(std::ostream& str, RM_AllocatedResource& alloc);     
    static ::std::atomic_long  _lastHandle;
    long nextHandle() {
      ++_lastHandle;
      return _lastHandle;
    }
    long long  createAllocTime() {
       using namespace std::chrono;
       milliseconds ms = duration_cast< milliseconds >( system_clock::now().time_since_epoch() );
       auto value = std::chrono::duration_cast<std::chrono::milliseconds>(ms);
      // _allocTime = value.count();
      return value.count();
    }

    long _handle;
    std::string _id;
    std::string _swObj;
    std::string _application; //AI 030624
    std::string _computer;
    std::string _partition;
    unsigned  long _pid;
    std::string _client;
    long long _allocTime; //AI 030624
    //AI 030624 uint64_t _callstamp;
    std::string _hwClass;
    std::string _hwObj;
};
//BOOST_CLASS_VERSION(RM_AllocatedResource, 1)

std::ostream& operator<<(std::ostream& str, RM_AllocatedResource& alloc);

/**
 * Support all real RM functionality like allocate/free resources, get 
 * different kinds of info. Multy-map with all data about allocated resources is inside.
 * **/ 
class RM_Control {
   public:
        RM_Control() {
           /// _rmap.get<handle_key>().max_load_factor(0.9f);
        }
        void addResource(const RM_AllocatedResource& res) {
            _rmap.insert(res);
        }
        bool removeResource(long handle) {
            auto& multi_map = _rmap.get<handle_key>();
            const auto& pair = multi_map.equal_range(handle);
	    if( pair.first ==  pair.second )
		    return false; //nothing to free
            multi_map.erase(pair.first, pair.second);
	    return true;
        }
        bool removeResource(const std::string& partition, const std::string& computer, long pid) {
            auto& multi_map = _rmap.get<part_comp_pid_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(partition, computer, pid));
	    if( pair.first ==   pair.second )
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }
//AI 030624
       bool removeResource(const std::string& computer, long pid) {
            auto& multi_map = _rmap.get<comp_pid_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(computer, pid));
	    if( pair.first ==   pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }

       bool removePartition(const std::string& partition) {
            auto& multi_map = _rmap.get<partit_key>();
            const auto& pair = multi_map.equal_range(partition);
	    if( pair.first ==   pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    //std::cout << "TEST NOT EMPTY erase"<<std::endl<<std::endl;
            return true;
        }       
        bool removePartitionResource(const std::string& resource, const std::string& partition) {
            auto& multi_map = _rmap.get<uid_part_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(resource, partition));
	    if( pair.first ==  pair.second ) 
		 return false;
            multi_map.erase(pair.first, pair.second);
	    //std::cout << "TEST NOT EMPTY erase"<<std::endl<<std::endl;
	    return true;
       }
       bool removeComputerAll(const std::string& computer) {
            auto& multi_map = _rmap.get<comp_key>();
            const auto& pair = multi_map.equal_range(computer);
	    if( pair.first ==   pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }
       bool removeResource(const std::string& resource) {
            auto& multi_map = _rmap.get<uid_key>();
            const auto& pair = multi_map.equal_range(resource);
	    if( pair.first ==  pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }
       bool removeComputerResource(const std::string& resource, const std::string& computer, const std::string& partition) {
            auto& multi_map = _rmap.get<uid_part_comp_key>(); //uid_comp_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(resource, computer, partition));
	    if( pair.first ==   pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }
       bool removeApplication(const std::string& partition, const std::string& swObj, const std::string& computer) {
            auto& multi_map = _rmap.get<part_comp_sw_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(partition, computer, swObj));
	    if( pair.first ==   pair.second ) 
                 return false;
            multi_map.erase(pair.first, pair.second);
	    return true;
       }
       //AI 030624
       void updateProcess( long handle, unsigned long pid ) {
           std::vector<RM_AllocatedResource> lst = handleResources( handle );
           removeResource( handle );
           for ( auto& alloc : lst ) {
	       const RM_AllocatedResource allocRes( handle, alloc.getID(), alloc.getSwObj(),
                                                     alloc.getApplication(),
                                                     alloc.getComputer(),
                                                     alloc.getPartition(), pid, alloc.getClient(), alloc.getAllocationTime()
                                                     , alloc.getHwClass(), alloc.getHwObj());
               addResource(allocRes);
	   }
       }
 
        //use in check if available
        // sw type
        long getTotalAllocated(const std::string& resourceID) const {
            const auto& multi_map = _rmap.get<uid_key>();
            return multi_map.count(resourceID);
        }
        long getAllocatedPP(const std::string& resourceID,  const std::string& partition) const {
            const auto& multi_map = _rmap.get<uid_part_key>();
            return multi_map.count(boost::make_tuple(resourceID, partition));
        }
        //hw (Computer class)
        long getTotalAllocatedPerComputer(const std::string& resourceID, const std::string& computer) const {
            const auto& multi_map = _rmap.get<uid_comp_key>();
            return multi_map.count(boost::make_tuple(resourceID, computer));
        }
        long getComputerAllocatedPP(const std::string& resourceID, const std::string& partition, const std::string& computer) const {
            const auto& multi_map = _rmap.get<uid_part_comp_key>();
            return multi_map.count(boost::make_tuple(resourceID, partition, computer));
        }
        //hw
        long getHWObjAllocatedPP(const std::string& resourceID, const std::string& partition, const std::string& hwClass, const std::string& hwid) const {
            const auto& multi_map = _rmap.get<part_uid_hwcls_hwid_key>();
            return multi_map.count(boost::make_tuple( partition, resourceID, hwClass,hwid ));
        }
        long getHWObjAllocated(const std::string& resourceID, const std::string& hwClass, const std::string& hwid) const {
            const auto& multi_map = _rmap.get<uid_hwcls_hwid_key>();
            return multi_map.count(boost::make_tuple(resourceID, hwClass, hwid ));
        }
        long allocTot( const RM_Resource& r, const RM_AllocatedResource& allocR );
        long allocPP( const RM_Resource& r, const RM_AllocatedResource& allocR );
        bool isResourceAvailable(const RM_Resource& res, const std::string& partition, const std::string& computer,
             const std::string& swObj, const std::string& application, const std::string& client, unsigned long pid //for exception info
        );

        std::vector<RM_AllocatedResource> handleResources ( const long handle) const {
            const auto& multi_map = _rmap.get<handle_key>();
            const auto& pair = multi_map.equal_range( handle );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }  

        std::vector<std::string> getPartitions() {
             std::set<std::string> p_lst;
             for ( const auto& all: _rmap )
                p_lst.insert( all.getPartition());

             return std::vector<std::string>(p_lst.begin(), p_lst.end());
        }

        const std::vector<std::string>  getResourceOwners(  const std::string res,  const std::string p ) {
            const auto& multi_map = _rmap.get<uid_part_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(res,p));
            const std::vector<RM_AllocatedResource> myList = std::vector<RM_AllocatedResource>(pair.first, pair.second);

            std::set<std::string> o_lst;
            for( const auto& j : myList)
               o_lst.insert( j.getClient() );

            return std::vector<std::string>(o_lst.begin(), o_lst.end());
        }

        std::vector<RM_AllocatedResource> resourceInfo( const std::string p, const std::string res) {
            const auto& multi_map = _rmap.get<uid_part_key>();
            const auto& pair = multi_map.equal_range( boost::make_tuple(res,p) );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> resourceInfo( const std::string res) {
            const auto& multi_map = _rmap.get<uid_key>();
            const auto& pair = multi_map.equal_range( res );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> partitionInfo( const std::string p ) {
            const auto& multi_map = _rmap.get<partit_key>();
            const auto& pair = multi_map.equal_range( p );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> computerInfo( const std::string comp ) {
            const auto& multi_map = _rmap.get<comp_key>();
            const auto& pair = multi_map.equal_range( comp );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> computerResourceInfo( const std::string comp,  const std::string p, const std::string res ) {
            const auto& multi_map = _rmap.get<uid_part_comp_key>();
            const auto& pair = multi_map.equal_range( boost::make_tuple(res,p,comp) );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> computerResourceInfo( const std::string comp, const std::string res ) {
            const auto& multi_map = _rmap.get<uid_comp_key>();
            const auto& pair = multi_map.equal_range( boost::make_tuple(res,comp) );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> hwObjResourceInfo( const std::string res, const std::string hwClass, const std::string hwObj ) {
            const auto& multi_map = _rmap.get<uid_hwcls_hwid_key>();
            const auto& pair = multi_map.equal_range( boost::make_tuple(res,hwClass,hwObj) );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
        std::vector<RM_AllocatedResource> applicationInfo( const std::string p, const std::string swObj, const std::string comp) {
            const auto& multi_map = _rmap.get<part_comp_sw_key>();
            const auto& pair = multi_map.equal_range( boost::make_tuple(p,comp,swObj) );
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        } 
        std::vector<RM_AllocatedResource> partitionCompProcInfo(const std::string& partition, const std::string& computer, long pid) {
            auto& multi_map = _rmap.get<part_comp_pid_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(partition, computer, pid));
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }
	//AI 030624
	std::vector<RM_AllocatedResource> compProcInfo(const std::string& computer, long pid) {
            auto& multi_map = _rmap.get<comp_pid_key>();
            const auto& pair = multi_map.equal_range(boost::make_tuple(computer, pid));
            return std::vector<RM_AllocatedResource>(pair.first, pair.second);
        }

bool isOwner( const std::string& partit, const std::string& swObj, const std::string& comp, const std::string& client);

   private:
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int) {
         ar & _rmap;
      }
      void putNoResourceMess( std::ostringstream& mess, const std::string& resource, const std::string& computer, bool isTot ); 
      void putResStatMess( std::ostringstream& mess, const daq::rmgr::RM_Resource& res, const long allocated, bool isTot);   
      void putHandleMess( std::ostringstream& mess,  const std::string& computer, const std::string& swObj, const std::string& application, const std::string& client,  const std::string& p, unsigned long pid, long hndl, const std::string& hwObj, const std::string& hwCl );
      void putHandleMess(  std::ostringstream& mess,std::vector<RM_AllocatedResource>& resLst, bool isTot, const std::string partition );
      bool isCorrectAppName( const std::string application, const std::string computer);
     // Simple keys
      struct uid_key {};
      struct handle_key {};
      struct partit_key {};
      struct comp_key {}; 
   //AI 030624   struct callstamp_key {};
     // Composite keys
      struct uid_part_key {};
      struct uid_comp_key {};
      struct uid_part_comp_key {};
      struct part_comp_pid_key {};
      struct comp_pid_key {};  //AI 030624
      struct part_comp_sw_key {};
      struct part_swid_hwcls_hwid_key {};
      struct uid_hwcls_hwid_key {};
      struct part_uid_hwcls_hwid_key {};

      typedef ::boost::multi_index::multi_index_container<
                   daq::rmgr::RM_AllocatedResource,
                ::boost::multi_index::indexed_by<
                   ::boost::multi_index::hashed_non_unique<  ::boost::multi_index::tag<handle_key>,::boost::multi_index::member< daq::rmgr::RM_AllocatedResource, const long, &daq::rmgr::RM_AllocatedResource::_handle>>
                                                >
                                                   > RM_Resourcetest;
      typedef ::mi::multi_index_container<
          daq::rmgr::RM_AllocatedResource,
                    mi::indexed_by<
                        mi::hashed_non_unique<mi::tag<handle_key>, mi::member< daq::rmgr::RM_AllocatedResource, const long, &daq::rmgr::RM_AllocatedResource::_handle>>,
                        mi::ordered_non_unique<mi::tag<uid_key>, mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>>,
                        mi::ordered_non_unique<mi::tag<partit_key>, mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>>,
                        mi::ordered_non_unique<mi::tag<comp_key>, mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>>,
/*AI 030624
                        mi::ordered_non_unique<mi::tag<callstamp_key>, mi::member<RM_AllocatedResource, const uint64_t , &RM_AllocatedResource::_callstamp>>,
*/
                        mi::ordered_non_unique<mi::tag<uid_part_key>,
                                                        mi::composite_key<RM_AllocatedResource,
                                                            mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>,
                                                            mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>
                                                        >
                        >,
                        mi::ordered_non_unique<mi::tag<uid_comp_key>,
                                                        mi::composite_key<RM_AllocatedResource,
                                                            mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>,
                                                            mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>
                                                        >
                        >,
                        mi::ordered_non_unique<mi::tag<uid_part_comp_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>
                                                                      >
                        >,
                        mi::ordered_non_unique<mi::tag<part_comp_pid_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>,
                                                                          mi::member<RM_AllocatedResource, const unsigned long, &RM_AllocatedResource::_pid>
                                                                      >
                        >,
			//AI 030624
			mi::ordered_non_unique<mi::tag<comp_pid_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
								          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>,
                                                                          mi::member<RM_AllocatedResource, const unsigned long, &RM_AllocatedResource::_pid>
                                                                      >
                        >,

                        mi::ordered_non_unique<mi::tag<part_comp_sw_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_computer>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_swObj>
                                                                      >
                        >,

                        mi::ordered_non_unique<mi::tag<uid_hwcls_hwid_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_hwClass>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_hwObj>
                                                                      >
                        >,
                        mi::ordered_non_unique<mi::tag<part_uid_hwcls_hwid_key>,
                                                                      mi::composite_key<RM_AllocatedResource,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_partition>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_id>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_hwClass>,
                                                                          mi::member<RM_AllocatedResource, const std::string, &RM_AllocatedResource::_hwObj>
                                                                      >
                        >
                     >
        > RM_ResourceContainer;

        RM_ResourceContainer _rmap;
};

//AI oct 24
struct HandleAndTime {
  long hndl;
  long long time;
};

class RM_Backup;

/**
 * This is the class where the bulk of the RM functionality is implemented
 *
 * */
class RM_Impl {
public:
   typedef std::vector<RM_Resource> ResList; // Configuration resources (only SW and HW resource types there) key=resID
   typedef std::map<std::string, std::vector<std::string>> SWObjResources; //key=swpbjId, value - list of resource id's

RM_Impl(); //only for restore from archive
RM_Impl(const std::string dbname);
RM_Impl(RM_Backup* bck, const std::string dbname);
~RM_Impl();

RM_Backup* getBck() { return _bck; }

void rmConfigurationUpdate();
bool isConfigAvailable();
bool hasResources(const std::string swobjId) const;

HandleAndTime allocateResources( const std::string& partitionName, 
                        const std::string& softwareUID,
			const std::string& application,  //AI 030624
                        const std::string& computer, 
                        unsigned long pid, 
                        const std::string& client
			, long long timeCreation=0);
void setProcess( long handle, unsigned long pid); //AI 030624
HandleAndTime allocateResources4Process( const std::string& partitionName,
                                const daq::rmgr::Res_ID_list& res_lst, //no  softwareUID
				const std::string& application, //AI 030624
                                const std::string& computer, 
                                unsigned long pid, 
                                const std::string& client, long long timeCreation=0);
bool freeResource(long handle);
bool freeResource(const std::string& partition, const std::string& computer, long pid);
bool freeResource( const std::string& computer, long pid); //AI 030624
bool freeAllInPartition( const char* myHost,  const char* myName, const std::string& partition) {
      return _rm_ctrl.removePartition( partition );
}
bool freePartitionResource( const std::string& resource, const std::string& partition ) {
    return _rm_ctrl.removePartitionResource( resource, partition);
}
bool freeAllOnComputer(const std::string& computer) {
    return _rm_ctrl.removeComputerAll( computer );
}
bool freeResource( const std::string& resource ) {
   return _rm_ctrl.removeComputerAll( resource );
}
bool freeComputerResource(const std::string& resource, const std::string& computer, const std::string& partition) {
  return _rm_ctrl.removeComputerResource( resource, computer, partition );
}
bool freeApplicationResources(  const std::string& p,  const std::string& swObj,  const std::string& computer) {
  return _rm_ctrl.removeApplication( p, swObj, computer );
}
bool isOwner( const std::string& partit, const std::string& swObj, const std::string& comp, const std::string& client) {
  return _rm_ctrl.isOwner( partit, swObj, comp, client );
}

bool isSoleOwner( const char* p, const std::string& user); //AI 030624

// different info
daq::rmgr::Handle_Info* getHandleInfo(long handle);
daq::rmgr::Rmstr_lst* getPartitionsList();
daq::rmgr::Rmstr_lst* getResourceOwners(const char* rname, const char* pname);
daq::rmgr::Allocated_Info* getResourceInfo(const char* p, const char* rn);
daq::rmgr::Allocated_Info* getResourceInfo( const std::string& res);
daq::rmgr::Allocated_Info* getPartitionInfo( const std::string& p );
daq::rmgr::Allocated_Info* getComputerInfo( const std::string& comp );
daq::rmgr::Allocated_Info* getComputerResourceInfo( const std::string& comp,  const std::string& p, const std::string& res);
daq::rmgr::Allocated_Info* getApplicationInfo( const std::string& partit, const std::string& swObj, const std::string& comp);
daq::rmgr::Allocated_Info* getPartitionCompProcInfo(const std::string& partition, const std::string& computer, long pid);
//AI 030624
daq::rmgr::Allocated_Info* getCompProcInfo(const std::string& computer, long pid);

const RM_Resource& getResource( const std::string id) {
   const auto& iter = _conf_res_lst.find( id );
   return iter->second;
} //throw no resource
void setMemStr( CORBA::String_member& toVal, const std::string& frVal ) {
     toVal = CORBA::string_dup( frVal.c_str() );
}
void setElemStr(_CORBA_String_element toVal, const std::string& frVal ) {
     toVal = CORBA::string_dup( frVal.c_str() );
}

daq::rmgr::Config_update* getConfigInfo();
void rmConfigurationUpdate(::Configuration * conf);
void configWarningMess( RM_Resource* oldVal, RM_Resource* newVal);
void configWarningMess( const std::pair< std::string, std::vector<RM_Resource> > oldV, std::string& str, const std::vector<const daq::core::RM_Resource*>& newVal  );
bool listResourcesDiffer( std::vector<RM_Resource> oldV, const std::vector<const daq::core::RM_Resource*>& newVal );
void configWarningMess(  std::string& str );
//backup manager related
bool isDoBackup();
const std::string getBckPath() const ;
bool isRestoreBackup();
void resetBackup(RM_Backup* nBck) { if(nBck) _bck = nBck; }
bool checkAccesPermission( const std::string mname, const std::string mhost, const char * function_name); 
std::string getUser(const std::string clientDaqTok); //for daqtoken process

std::string getConfDBName() const { return _config_db_name; }
void setConfDBName( const std::string dbname ) { _config_db_name = dbname; }
bool restoreDone(); 
private:
//const char* TDAQ_RM_CONFIG = "TDAQ_RM_CONFIG";
std::string _config_db_name;  //configuration DB name
void handleInit( long val = 0) { _lastHandle = val; }
long nextHandle() {
   ++_lastHandle;
   return _lastHandle;
}

//Access Manager in use
 bool _use_am_auth;
 void setUseAM();
 bool isUseAM() { return _use_am_auth; }
 bool isNotComputerHWcls( RM_Resource& r ) {
    if(r.type() == ResourceType::HWR
       && r.hwCls().length() > 0
       && r.hwCls() != compClass)
          return true;
     return false; 
 }

  friend class boost::serialization::access;
  template<class Archive>
  void save(Archive & ar, const unsigned int) const {
    long lastHnd;
    lastHnd = (long) _lastHandle;
    ar & lastHnd;
    ar & _conf_res_lst;
    ar & _swobj_map;
    ar & _rm_ctrl;    
  }
  template<class Archive>
  void load(Archive & ar, const unsigned int) {
     long lastHnd;
     ar & lastHnd;
     handleInit( lastHnd );
     ar & _conf_res_lst;
     ar & _swobj_map;
     ar & _rm_ctrl;
  }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

    std::atomic_long  _lastHandle; //keep last value of generated handle

    // all ConfDb resources that are passed by the clients in requests
    std::map<std::string, RM_Resource> _conf_res_lst;
   // This map establishes a relationship between the SWObj and the associated resources
   //from Config DB (passed in client requests)
   std::map< std::string, std::vector<RM_Resource> > _swobj_map;
   //control map with allocated resources
   RM_Control _rm_ctrl;

        // When a command is received to reload the information, it should
        // be enough to update the two previous containers
        // NOTE: after a reload we should check if some inconsistency is created
        // with respect to the already allocated resources. Example: the total number of
        // allowed resources is reduced from 10 to 1 - some message should be sent to
        // make evident that now there are more allocated resources than allowed by the
        // new configuration

   RM_Backup* _bck; //_backup manager
   daq::rmgr::Allocated_Info* createAllocatedInfo(std::vector<RM_AllocatedResource>& lst, daq::rmgr::Allocated_Info* info);
////   std::map<std::string, std::shared_ptr<boost::mutex>> _mutex_map;
//////   std::map<std::string, std::shared_ptr<std_mutex>> _mutex_map;

daq::rmgr::HandleAndTime allocateResources( const std::string& partitionName,
                        const std::string& softwareUID,
			const std::string& application, //AI 060324
                        const std::string& computer,
                        unsigned long pid,
                        const std::string& client,
			long long timeCreation, //AI oct24
                        const std::string& hwClass, //oct 2023 = "Computer",
                        const std::string& hwId);
};

  } //end rm
} //end daq


#endif  /* RMimpl_H_INCLUDED */                              


