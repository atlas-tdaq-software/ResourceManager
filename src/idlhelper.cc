#include "src/idlhelper.h"
#include <rmgr/rmgr.hh>
#include <chrono>

//common useful functions
//daq::rmgr::Handle_InfoHelper::~Handle_InfoHelper() {
//    delete info;
//    info = nullptr;
//}

static const std::string getTimeAsString( long long lltime ) {
    long time = lltime;
    std::chrono::milliseconds dur(time);
    std::chrono::time_point<std::chrono::system_clock> dt(dur);
    std::time_t timet = std::chrono::system_clock::to_time_t(dt);
    std::ostringstream txt;
    txt << timet;
    return txt.str();
}

const std::string daq::rmgr::Handle_InfoHelper::getAsString() const {
    if(info == nullptr)
      return "Handle_Info={ empty }";
    std::string handleStr( std::to_string( info->handle ) );
    std::string pidStr;
    pidStr = std::to_string( info->pid );
    std::string allocTimeStr( std::to_string( info->allocTime ) );
    std::string retv("Handle_Info={ handle=" + handleStr + ", SWObj="
                       + (const std::string)info->swObj + ",application="
		       + (const std::string)info->application 
		       + " computer="
                       + (const std::string)info->computer + ", partition=" 
                       + (const std::string)info->partition
                       + ", client=" + (const std::string)info->client 
                       + ", pid=" + pidStr 
		       + ",allocTIme=" + allocTimeStr
		       + " Resources=");
    const std::string types[] = { "SW", "HW" };
    for(unsigned int i=0; i<info->resDetails.length(); i++) {
       retv += " { resource=" + (const std::string)info->resDetails[i].resource
                  + ", type=" + types[info->resDetails[i].type]
                  + ", maxPP=";
       retv += std::to_string( info->resDetails[i].maxPP );
       retv += ", maxTotal=";
       retv += std::to_string( info->resDetails[i].maxTotal );
       retv += ", allocPP=";
       retv += std::to_string( info->resDetails[i].allocPP );
       retv += ", allocTotal=";
       retv += std::to_string( info->resDetails[i].allocTotal );
       retv += " } \n";
    }
    return retv;
}

std::ostream& daq::rmgr::operator<<(std::ostream& str, daq::rmgr::Handle_InfoHelper& helper) {
   daq::rmgr::Handle_Info* info = helper.info;
   if(info == nullptr) {
     str << "Handle_Info={ empty }" <<std::endl;
     return str;
   }
   str << "Handle_Info={" << std::endl;
   str << " handle=" << info->handle << std::endl;
   str << " swObj=" << info->swObj;
   str << " computer=" << info->computer;
   str << " partition=" << info->partition;
   str << " client=" << info->client;
   str << " pid=" << info->pid << std::endl;
   str << " allocTime=" << info->allocTime;
   str << "  Resources:" << std::endl;
   const std::string types[] = { "SW", "HW" };
   for(unsigned int i=0; i<helper.info->resDetails.length(); i++) {
     str << "  {";
     str << "   resource=" << info->resDetails[i].resource;
     str << "   type=" << types[info->resDetails[i].type];
     str << "   maxPP=" << info->resDetails[i].maxPP;
     str << "   maxTotal=" << info->resDetails[i].maxTotal << std::endl;
     str << "   allocPP=" << info->resDetails[i].allocPP;
     str << "   allocTotal=" << info->resDetails[i].allocTotal;
     str << "  }"<< std::endl;
   }
   return str;
}

const daq::rmgr::HandleInfo daq::rmgr::Handle_InfoHelper::getHandleInfo() {
 daq::rmgr::HandleInfo hi;
 hi._handle = info->handle;
 hi._swObj = info->swObj;
 hi._application = info->application; //AI 030624
 hi._computer = info->computer;
 hi._partition = info->partition;
 hi._client = info->client;
 hi._pid = info->pid;
 hi._allocTime = info->allocTime;
 
 for(unsigned int i=0; i<info->resDetails.length(); i++) {
   bool isHWR = info->resDetails[i].type == daq::rmgr::ResourceType::HWR;
   ResInfo ri( (std::string)info->resDetails[i].resource, isHWR,
               (long)info->resDetails[i].maxPP, (long)info->resDetails[i].maxTotal,
               (long)info->resDetails[i].allocPP,  (long)info->resDetails[i].allocTotal);
   hi._resDetails.push_back( ri ); 
 }
 return hi;
}

//daq::rmgr::Allocated_InfoHelper::~Allocated_InfoHelper() {
//    delete allocInfo;
//    allocInfo = nullptr;
////}

std::ostream& daq::rmgr::operator<<(std::ostream& str,  daq::rmgr::Allocated_InfoHelper& helper) {
   const std::string types[] = { "SW", "HW" };
//   daq::rmgr::Allocated_Info_var info2(  helper.allocInfo );
//   for( unsigned int i=0; i<helper.allocInfo_var.length(); i++) {
//     str << " handle=" << info2[i].handle;
//   }
   daq::rmgr::Allocated_Info* info = helper.allocInfo;
   str << "Allocated_Info= " << std::endl;
   for( unsigned int i=0; i<helper.allocInfo->length(); i++) {
     str << "{" << std::endl;
     str << " handle=" << (*info)[i].handle;
     str << " resource=" << (*info)[i].resource;
     str << " type=" << types[(*info)[i].type];
     str << " maxPP=" << (*info)[i].maxPP;
     str << " maxTotal=" << (*info)[i].maxTotal;
     str << " hwClass=" << (*info)[i].hwClass << std::endl;	
     str << " swObj=" << (*info)[i].swObj;
     str << " appl=" << (*info)[i].application; //AI 030624
     str << " partition=" << (*info)[i].partition;
     str << " client=" << (*(helper.allocInfo))[i].client << std::endl;
     str << " computer=" << (*info)[i].computer;
     str << " pid=" << (*info)[i].pid;
     str << " hwId=" << (*info)[i].hwId;
     str << " allocPP=" << (*info)[i].allocPP;
     str << " allocTotal=" << (*info)[i].allocTotal;
      str << " allocTime=" << (*info)[i].allocTime; //AI 030624
     str << "}" << std::endl;
   }
   return str;
}

const daq::rmgr::AllocatedInfo daq::rmgr::Allocated_InfoHelper::getAllocatedInfo() {
  daq::rmgr::AllocatedInfo ainfo;
  for( unsigned int i=0; i<allocInfo->length(); i++) {
      bool isHWR = (*allocInfo)[i].type == daq::rmgr::ResourceType::HWR;
      daq::rmgr::AllocatedResource ar( (int)(*allocInfo)[i].handle, (std::string) (*allocInfo)[i].resource, isHWR, 
                            (long)(*allocInfo)[i].maxPP, (long) (*allocInfo)[i].maxTotal, (std::string) (*allocInfo)[i].hwClass, 
                            (std::string) (*allocInfo)[i].swObj, (std::string) (*allocInfo)[i].application, //AI oct24
			    (std::string) (*allocInfo)[i].partition, 
                            (std::string) (*allocInfo)[i].client, (std::string) (*allocInfo)[i].computer, 
                            (int)(*allocInfo)[i].pid, (std::string) (*allocInfo)[i].hwId, 
                            (long)(*allocInfo)[i].allocPP, (long) (*allocInfo)[i].allocTotal, (*allocInfo)[i].allocTime);
     ainfo.list.push_back( ar ); 
  }
  return ainfo;
}

daq::rmgr::Rmstr_lstHelper::Rmstr_lstHelper( std::unique_ptr<daq::rmgr::Rmstr_lst> lst ) 
{ list = lst.get(); }

std::vector<std::string> daq::rmgr::Rmstr_lstHelper::getList() {
   std::vector<std::string> retv;
   for (unsigned int i=0; i<list->length(); i++) {
     //std::string addme ( (*list)[i] );
     retv.push_back( static_cast<const char*>((*list)[i]) );
   }
   return retv;
}

std::ostream& daq::rmgr::operator<<(std::ostream& str, const daq::rmgr::Rmstr_lstHelper& helper) {
  Rmstr_lst* mylist = helper.list;
  if( (helper.list)->length() == 1) {
     str << (*(helper.list))[0];
     return str;
  }
  str << " Members= ";
  for (unsigned int i=0; i<(helper.list)->length(); i++)
     str << " { " <<(*mylist)[i] << " } ";
  str << std::endl;
  return str;
}

daq::rmgr::Config_updateHelper::Config_updateHelper( Config_update* inf ) 
       	: cinfo(inf) {}

std::ostream& daq::rmgr::operator<<(std::ostream& str, const daq::rmgr::Config_updateHelper& helper) {
  daq::rmgr::Config_update* myinfo = helper.cinfo;

  str << "Config_update = {"<<std::endl << "  SW_Resources= {";
  if( myinfo->swr_lst.length() == 0 )
     str << "  }" << std::endl;
  else
     str << std::endl;
  unsigned int i;
  for(i=0; i < myinfo->swr_lst.length(); i++) {
      str << "    {  id="<<myinfo->swr_lst[i].rid;
      str << " max_total=" << myinfo->swr_lst[i].max_total;
      str << " max_pp=" << myinfo->swr_lst[i].max_pp;
      str << "  }" <<std::endl;
  }
  if( myinfo->swr_lst.length() > 0 )
      str << "   }"<<std::endl;
  str << "   HW_Resources= {";
  if( myinfo->hwcr_lst.length() == 0 )
     str << "  }" << std::endl;
  else
     str << std::endl;
  for( i=0; i < myinfo->hwcr_lst.length(); i++) {
      str << "    {  id="<< myinfo->hwcr_lst[i].rid;
      str << " max_total=" << myinfo->hwcr_lst[i].max_total;
      str << " max_pp=" << myinfo->hwcr_lst[i].max_pp;
      str << " hwClass=" << myinfo->hwcr_lst[i].hwClass;
      str << "  }" <<std::endl; 
  }
  if( myinfo->hwcr_lst.length() > 0 )
      str << "   }"<<std::endl;
  str << "    SWObjects= {";
  if( myinfo->swobj_lst.length() == 0 )
      str << "  }" << std::endl;
  else
      str<<std::endl;
  for(i=0; i < myinfo->swobj_lst.length();  i++) {
     str << "      swObj="<<myinfo->swobj_lst[i].swid<< "  Resources={ ";
     for(unsigned int j=0; j < myinfo->swobj_lst[i].res_list.length(); j++)
         str << " {"<<myinfo->swobj_lst[i].res_list[j] << "}";
     str << "  }"<<std::endl;
  }
  str << "   }"<<std::endl<<"} end of Config_update"<<std::endl;
  return str;
}

const daq::rmgr::ConfigUpdate daq::rmgr::Config_updateHelper::getConfigUpdate() {
  daq::rmgr::ConfigUpdate info;
  unsigned int i;
  for(i=0; i <  cinfo->swr_lst.length(); i++) {
    daq::rmgr::SWResource r( (std::string) cinfo->swr_lst[i].rid, (long) cinfo->swr_lst[i].max_total,
                  (long) cinfo->swr_lst[i].max_pp );
    info.swr.push_back( r );
  }
  for( i=0; i < cinfo->hwcr_lst.length(); i++) {
    daq::rmgr::HWResource r( (std::string) cinfo->hwcr_lst[i].rid, (std::string) cinfo->hwcr_lst[i].hwClass,
                  (long) cinfo->hwcr_lst[i].max_total,
                  (long) cinfo->hwcr_lst[i].max_pp );
    info.hwr.push_back( r );
  }
  for(i=0; i < cinfo->swobj_lst.length();  i++) {
     daq::rmgr::SWObject obj((std::string) cinfo->swobj_lst[i].swid );
     for(unsigned int j=0; j < cinfo->swobj_lst[i].res_list.length(); j++) {
        std::string rrid( cinfo->swobj_lst[i].res_list[j]);
        obj._rlist.push_back((std::string) rrid );
     }
     info.swo.push_back( obj );
  }
  return info; 
}
