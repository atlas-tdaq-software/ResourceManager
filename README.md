# Resource manager (RM)
The Resources Manager (RM) is one of the components of the ATLAS TDAQ system. The TDAQ contains many hardware and software resources which can not be shared and so their usage must be controlled to avoid conflicts. The purpose of the RM is to marshal multiple accesses to resources in a manner that is similar to a lock manager in other software systems. If access to a piece of software or hardware is limited then an instance of this resource should be created and associated to the application or piece of hardware using the configuration data bases. During run-time, application should start only if all associated resources are available. When an application asks the RM permission to use resources, the RM first checks how many copies of the required resources were already allocated in the partition and the whole TDAQ system scope. If the number of allocated resources is less than the permitted limit then the new request is granted by handle and the requested number of copies of resources are allocated. If the maximum number of permitted copies have been allocated already then the request is refused. When an application terminates its allocated resources should be released and become available for other applications to use.

The RM component is essentially divided into RM server and client parts. The RM server is a passive component and it reacts only to client requests. The RM client performs requests to the RM server using CORBA based Communication using public interface described in IDL. The Resource manager User’s Guide describes the client public interfaces including IDL, C++ and java API and binaries of the RM component.

There are two main use cases of the RM usage in TDAQ:
* the resources are associated with SW object desribed in OKS, and are allocated by the Process Manager, when an application using such SW object is going to be started;
* the resources are allocated by an application itself using RM client API.
## RM idl interface 
`daq::rmgr` idl module was produced for the RM. Module includes definitions of required structures, such as different types of RM respurces, software object , allocted resources and other useful structures. It keeps exceptions and define ResMgr interface. The interface keeps method for request resources, fre them and geting different information concerning allocated resources.
## RM client C++ interface
RM_Client class is the implementation of the C++ API. User should link rmgr library and include RM_Client.h, exceptions.h and rminfo.h hrader file in order to use this API.
There are four types of methods in `RM_Client` class: request resources methods, free resource methods, getting different information concerning allocated resources and method to update configuration data. 

The RM server keeps configuration software objects that have links to RM resources. Software object identifier is used to request correspond resources. Software object identifier and host plays role of application that needs resources. Also processes need resource in some cases, resource identifier is used in those methods as input parameter.  
Typical additional parameters for request resources methods are:
- partition within which resources should be allocated
- computer (host) where application or process that ask resources should start
- user who performs request

RM returns handle in case if request to get resources was successful. This handle can be used in free resource methods in order to release all resources that have been allocated in one request. There are also couple of methods that release resources using such parameters as partition, computer, software object, resource identifier. 

There are a set of methods to get different information about resources in different scopes, for example all partition resources, resources allocated for some handle, for process and so on. Special classes produced to keep such information in convenient for user view. These classes defined in rminfo.h header file. 

`rmConfigurationUpdate` method should be used in order to update on the RM server resource related configuration data.

An expert role of Access Manager is required in order to use follows methods:
- getPartitionInfo
- freeAllInPartition
- freeAllOnComputer
- freeApplicationResources (deprecated)

## RM client java interface
The package `daq.rmgr` is created in order to implement RM java API to call to RM server. This package consists of `RMClientImpl` class that implement needed requests to RM server. Some additional classes added to define useful classes for resource information description and some new exceptions definition.
The details of class methods can be found in
https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/code/javadoc/tdaq-99-00-00/index.html

## Binaries of the RM
**rmgr_server**                     Start Resource Manager server

**rmgr_request_resourcces**         Request resources for a software object or one resource to run a process on a specified computer. Resources can be requested for a software object or for a process, but not for both at the same time 

**rmgr_free_computer_resources**    Free all resources that have been allocated on the computer 

**rmgr_free_partition_resources**   Free all resources in partition

**rmgr_get_config_info**            Get information about configuration RM resources and software objects loaded into the Resource Manager

**rmgr_get_partition_info**         Get information about granted in partition resources

**rmgr_get_partitions_list**        Show a list of all partitions that have been registered with the Resource Manager. Information about each partition is enclosed in parentheses.For each partition, a unique partition ID and configuration database name are displayed, separated by a semicolon.

**rmgr_update_config**               Update configuartion data in the Resource Manager
                     
