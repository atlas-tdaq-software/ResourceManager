#ifndef rminfo_H_INCLUDED
#define rminfo_H_INCLUDED

#include <vector>
#include <iostream>
#include <string>

namespace daq
{
namespace rmgr
{

class AllocatedResource {
private:
  friend std::ostream& operator<<(std::ostream& str,  const daq::rmgr::AllocatedResource& info);
public: 
 AllocatedResource() {}
 AllocatedResource(int handle,const std::string rname, bool isHWR, int maxPP, int maxTotal, const std::string hwClass, const std::string swObj, 
		 const std::string appl, //AI oct24
		 const std::string p, const std::string client, const std::string comp, int pid,const  std::string hwId, long allocPP, long allocTotal, long long allocTime) 
      : _handle(handle), _resource( rname ), _isHWR(isHWR), _maxPP( maxPP ), _maxTotal( maxTotal ),
       _hwClass(hwClass), _swObj(swObj), _application(appl), _partition(p), _client(client), _computer(comp),
       _pid(pid), _hwId(hwId), _allocPP(allocPP), _allocTotal(allocTotal), _allocTime(allocTime)  {}

 int _handle;
 std::string _resource;
 bool _isHWR;
 long _maxPP;
 long _maxTotal;
 std::string _hwClass;
 std::string _swObj;
 std::string _application; //AI 030624
 std::string _partition;
 std::string _client;
 std::string _computer;
 int _pid;
 std::string _hwId;
 long _allocPP;
 long _allocTotal;
 long long _allocTime; //AI 030624
};

std::ostream& operator<<(std::ostream& str,  const daq::rmgr::AllocatedResource& ar);

class AllocatedInfo {
private:
  friend std::ostream& operator<<(std::ostream& str, const AllocatedInfo& info);
public:
 AllocatedInfo() {}
std::vector<AllocatedResource> list;
};

std::ostream& operator<<(std::ostream& str,const daq::rmgr::AllocatedInfo& info);



class ResInfo {
//private:
//  friend std::ostream& operator<<(std::ostream& str, ResInfo& info);
public:
 ResInfo() {}
 ResInfo(const std::string rname, bool isHWR, long maxPP, long maxTotal, long allocPP, long allocTotal)
          :  _resource( rname ), _isHWR(), _maxPP( maxPP ), _maxTotal( maxTotal ),  _allocPP(allocPP), _allocTotal(allocTotal) {}

 std::string _resource;
 bool _isHWR;
 long _maxPP;
 long _maxTotal;
 long _allocPP;
 long _allocTotal;
};

//std::ostream& operator<<(std::ostream& str, ResInfo& info);

class HandleInfo {
private:
  friend std::ostream& operator<<(std::ostream& str,const  HandleInfo& hinfo);
public:
 HandleInfo() {}
 HandleInfo( int handle,const std::string swObj, 
		 const std::string application, //AI 030624
		 const std::string computer, const std::string partition, const std::string client, int pid, long long allocT) : _handle(handle), _swObj(swObj), _application(application), _computer(computer), _partition(partition), _client(client), _pid(pid), _allocTime(allocT) {}
 int _handle;
 std::string _swObj;
 std::string _application;
 std::string _computer;
 std::string _partition;
 std::string _client;
 int _pid;
 long long _allocTime;
 std::vector<ResInfo> _resDetails;
};

std::ostream& operator<<(std::ostream& str, const HandleInfo& hinfo);

//config related
class SWResource {
public:
SWResource() {}
SWResource( const std::string rid, long maxTotal, long maxPP ) 
          : _rid(rid), _maxTotal(maxTotal), _maxPP(maxPP) {}
std::string _rid;
long _maxTotal;
long _maxPP;
};

class HWResource {
public:
HWResource() {}
HWResource( const std::string rid, const std::string hwClass, long maxTotal, long maxPP )
          : _rid(rid), _hwClass(hwClass), _maxTotal(maxTotal), _maxPP(maxPP) {}
std::string _rid;
std::string _hwClass;
long _maxTotal;
long _maxPP;
};

class SWObject {
public:
SWObject() {}
SWObject( const std::string swid ) : _swid(swid) {}
std::string _swid;
std::vector<std::string> _rlist;
};

class ConfigUpdate {
private:
friend std::ostream& operator<<(std::ostream& str, const daq::rmgr::ConfigUpdate& cinfo);
public:
ConfigUpdate() {}
std::vector<SWResource> swr;
std::vector<HWResource> hwr;
std::vector<SWObject> swo;
};

std::ostream& operator<<(std::ostream& str, const daq::rmgr::ConfigUpdate& cinfo);


} // namespace rmgr
} // namespace daq

#endif 
