#ifndef RM_Client_H_INCLUDED
#define RM_Client_H_INCLUDED

////#include <ResourceManager/idlhelper.h>
#include <ResourceManager/rminfo.h>
#include <ResourceManager/exceptions.h>

class Configuration;

namespace daq   {

namespace rmgr  {

class RM_ClientImpl;

/*! \brief This class provides a public API for all the Resource manager (RM) functionality.
    \ingroup public
    \sa
 */
class RM_Client
{
protected:
  RM_ClientImpl* impl;
public:

//constructors

  /*! Creates the object which represents the default client to be interacted with Resource Manager server with name RM_Server
  */
  RM_Client();

  /*! Creates the object which represents the client to be interacted with Resource Manager server with name sid
  *   \param sid	 Resource Manager server name
  */
  RM_Client(const char *sid);

  ~RM_Client();

//loading of configuration data into RM server

/*!  Reload on RM server the configuration from the DB that were used during RM server start.
 *   \param  partition  Partition on which scoope the configuration refreshing was initiated
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server
 *   \throw    daq::rmgr::ConfigurationException  Problems on server with configuration update.
 *   \throw    daq::rmgr::CommunicationException    system CORBA exception is the reason of this exception
 */
void rmConfigurationUpdate ( const std::string& partition );

// resource allocation methods

/*!   Requests resources in partition for software object to run on the requested computer.
  *   \param  partition   Partition name. Partition where resources to be allocated
  *   \param  swobjectid   Software object identifier. Software object that has association with required resources
  *   \param  computer   Computer name. Host on which application to be started.
  *  \param   user     User that will be the owner of the requested resources. This name is stored in the Resource Manager server as the owner of according resources. 
  *  \param  applecation  Name of the application for which resources are being requested. In case of default value "" application name is set to $swobjectid@$computer
  *  \param  pid ID of the process for which resources are being requested. Default value is 0. 
  *   \return      Resource Manager handle identifier. This positive value can  be used to free these allocated resources when according process will be stopped. Throws exception if failed to allocate resources on any mentioned below reason.
  *   \throw    daq::rmgr::UnavailableResources    There are no enough resources for the application.
  *   \throw     daq::rmgr::SoftwareNotFound        swobjectid is absent in the RM server. There are only software objects that have references at least to one resource is kept in the RM.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.   
*/
 long requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, //id in confdb
                        const std::string& computer,
                        const std::string& user = "",
			const std::string& application = "",
			unsigned long pid = 0);

 /*!   Sets the process id for all allocated resources that have correspond Resource Manager handle.
  *    \param  handle  Resource Manager handle 
  *    \param  pid     PID of the process for which resources were allocated
  */
void setProcess( long handle,  unsigned long pid);


/*!   Requests resources for process. Requested resources are those that associated with software object.
  *   \param  partition   Partition name where resources to be allocated
  *   \param  swobjectid  Software object id that has association with required resources
  *   \param  computer    The computer on which the process that requires resources is to run.
  *   \param  pid         PID of process that needs resource
  *  \param clientName     Client name. This name is stored in the Resource Manager server as the owner of according resources.
  *   \return   Resource Manager handle identifier. This positive value can  be used to free these allocated resources when according process will be stopped. Throws exception if failed to allocate resources on any mentioned below reason.
  *   \throw    daq::rmgr::UnavailableResources    There are no enough resources for the application.
  *   \throw    daq::rmgr::SoftwareNotFound        swobjectid is absent in the RM server. There are only software objects that have references at least to one resource is kept in the RM. 
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
 long requestResources (
                        const std::string& partition,
                        const std::string& swobjectid, 
                        const std::string& computer, 
                        unsigned long mypid,
                        const std::string& clientName = "NO");

/*!   Requests resource for process. This method will get process id automatically from the system and this value is used in the Resource Manager server during request processing.
  *   \param  partition   Partition name.
  *   \param  resource RM resource unique ID.
  *   \param  computer Computer name.
  *  \param clientName     Client name. This name is stored in the Resource Manager server as the owner of according resources.
  *   \return      Resource Manager handle identifier. This positive value can  be used to free these allocated resources when according process will be stopped. Throws exception if failed to allocate resources on any mentioned below reason.
  *   \throw    daq::rmgr::UnavailableResources    There are no enough resources for the process.
  *   \throw    daq::rmgr::ResourceNotFound        Resource  is absent in the RM server.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/

 long requestResourceForMyProcess(
                        const std::string& partition,
                        const std::string& resource,
                        const std::string& computer,
                        const std::string& clientName = "NO");

/*!   Requests resource for a given process.
 *    \param  partition   Partition name.
 *    \param  resource RM resource unique ID.
 *    \param  computer Computer name.
 *    \param clientName  Useer name. This name is stored in the Resource Manager server as the owner of according resources.
 *    \param process_id  Id of the process for which resources are requested.
 *    \return      Resource Manager handle identifier. This positive value can  be used to free these allocated resources when according process will be stopped. Throws exception if failed to allocate resources on any mentioned below reason.
 *    \throw    daq::rmgr::UnavailableResources    There are no enough resources for the process.
 *    \throw    daq::rmgr::ResourceNotFound  Rerource is absent in the RM server.
 *    \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *    \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
 */
long requestResource( const std::string& partition, const std::string& resource, const std::string& computer, const std::string& clientName, int process_id );

//free resources methods

/*! Frees resources that were granted with the RM handle.
  *   \param handleId  RM handle identifier.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
 void freeResources (long handleId); //, bool log_this_call=true);

/*! [[deprecated("log_this_call does not used.")]]
 */
void freeResources (long handleId, bool log_this_call) {
    freeResources(handleId);
}

/*!   Free all resources in partition.
 *   \param  p partition name
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
 *   \throw    daq::rmgr::CannotAcquireToken raised if falied to get daq token.
 *   \throw    daq::rmgr::CannotVerifyToken raised if failed to verify daq token on server.
 *   \throw    daq::rmgr::AcceessAnyDenied raised if Access Manager refuse to perform required action.
*/
void freeAllInPartition(const std::string&  p );

/*! [[deprecated("Use freeAllInPArtition instead.")]]
 */
void freePartition(const std::string&  p );

/*!   Free all resources on commputer for any partition 
 *   \param  computer  computer id
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
 *   \throw    daq::rmgr::CannotAcquireToken raised if falied to get daq token.
 *   \throw    daq::rmgr::CannotVerifyToken raised if failed to verify daq token on server.
 *   \throw    daq::rmgr::AcceessAnyDenied raised if Access Manager refuse to perform required action.
*/
void freeAllOnComputer(const std::string& computer); //, bool log_this_call=true);

/*! [[deprecated("Use freeAllOnComputer instead.")]]
 */
void freeComputer(const std::string& computer) { //, bool log_this_call=true);
    freeAllOnComputer( computer );
}
//oct24
/*! [[deprecated  Parameter log_this_call is ignored
 */
void freeComputer(const std::string& computer, bool log_this_call) {
   freeAllOnComputer( computer );
}


/*!  [[deprecated("Will be removed in next release.")]] 
 *   Free one resource in all  partitions.
 *   \param  resource resource id
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
void freeResource(const std::string& resource); //, bool log_this_call=true);

/*!  [[deprecated("Will be removed in next release.")]]   
 * Free one resource in one partition
 *   \param  p partition
 *   \param  resource resource id
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
void freePartitionResource(const std::string&  p, const std::string& resource); //, bool log_this_call=true);

/*!  [[deprecated("Will be removed in next release.")]] 
 *   Free one resource in one partition for one computer
 *   \param  resource resource id
 *   \param  computer  computer id
 *   \param  p partition
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
void freeComputerResource(const std::string& resource, const std::string& computer, const std::string&  p); //, bool log_this_call=true);

/*!  [[deprecated("Will be removed in next release.")]] 
 *   Free application resources
 *   \param  p partition
 *   \param  swObj software object id
 *   \param  computer  computer id
 *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
 *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
 *   \throw    daq::rmgr::CannotAcquireToken raised if falied to get daq token.
 *   \throw    daq::rmgr::CannotVerifyToken raised if failed to verify daq token on server.
 *   \throw    daq::rmgr::AcceessAnyDenied raised if Access Manager refuse to perform required action.
*/
void freeApplicationResources(const std::string& p, const std::string& swObj, const std::string& computer); //, bool log_this_call=true);

/*!   Free ALL allocated resources of the process. Resource that was granted for process with the same partition, resource and computer UID will be released in the RM and can be used for other clients. Also all resources  that where allocated with handle handleId also will be free. 
  *   \param handleId  RM handle identifier.
  *   \param  partition   Partition unique ID.
  *   \param  computer Computer unique ID.
  *   \param pid Child process id.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
void freeAllProcessResources( long handleId, //AI 030624 const std::string& partition,
                           const std::string& computer, unsigned long pid); //, bool log_this_call=true );

/*! [[deprecated("Parameters partition and log_this_call are not used.")]]
 */
void freeAllProcessResources( long handleId, const std::string& partition,
                           const std::string& computer, unsigned long pid, bool log_this_call=true ) {
    freeAllProcessResources(handleId, computer, pid);
}

/*!   Free resources of the process. Resource that was granted for process with the same partition, resource and computer UID will be released in the RM and can be used for other clients.
  *   \param  partition   Partition unique ID.
  *   \param  computer Computer unique ID.
 *    \param pid Process id.
      \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
      \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
void freeProcessResources(
                        const std::string& partition,
                        const std::string& computer,
                        unsigned long pid); //, bool log_this_call=true);

/*! [[deprecated("Parameter log_this_call is not used.")]]
 */
void freeProcessResources(
                        const std::string& partition,
                        const std::string& computer,
                        unsigned long pid, bool log_this_call) {
       freeProcessResources( partition, computer, pid);
}



//different info

/*! Gets the list of the partitions for which any resource was allocated.
  * \return list of partitions names in std::vector<std::string>.
  * \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  * \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
std::vector<std::string>  getPartitionsList();

/*!   Gets those client names that were used for allocation $rname resource in partition $p.
  *   \param rname Name of the resource.
  *   \param p Name of the partition.
  *   \return   List of client names in std::vector<std::string>.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
std::vector<std::string> getResourceOwners(const std::string& rname, const std::string& p);

/*! Gets information about resources that were granted with requested handle indentifier.
  *   \param  handleId   Handle identifier.
  *   \return    daq::rmgr::HandeInfo that keeps data about resources that were granted with requested handle indentifier.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
const daq::rmgr::HandleInfo getHandleInfo(long handleId);


/*! Gets information about all allocated resources in the partition.
  *   \param  partition   Name of the partition.
  *   \return   daq::rmgr::AllocatedInfo that keeps information about allocted in partition resources.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
  *   \throw    daq::rmgr::CannotAcquireToken raised if falied to get daq token
  *   \throw    daq::rmgr::CannotVerifyToken raised if failed to verify daq token on server.
  *   \throw    daq::rmgr::AcceessAnyDenied raised if Access Manager refuse to perform required action.
*/
const daq::rmgr::AllocatedInfo getPartitionInfo( const std::string& partition );

/*! Gets information about resource in the partition.
  *   \param  partition   Name of the partition.
  *   \param  rname   Name of the resource.
  *   \return   daq::rmgr::AllocatedInfo that keeps information about allocted resources.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
const daq::rmgr::AllocatedInfo getResourceInfo( const std::string& partition, const std::string& rname);

/*!  Gets information about resource.
  *  \param  resource Resource ID.
  *  \return  daq::rmgr::AllocatedInfo that keeps information about all allocted resources.
  *  \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *  \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
const daq::rmgr::AllocatedInfo getFullResInfo(const std::string& resource);

/*!  Gets information about all resources that were allocated for computer.
  *  \param  computer Computer name.
  *  \return   daq::rmgr::AllocatedInfo that keeps information about all resources allocted for computer resources.
  *  \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *  \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
const daq::rmgr::AllocatedInfo getComputerInfo(const std::string& computer);

/*! Gets information about resource that was allocated for computer in the partition.
  *   \param  computer Computer name.
  *   \param  partition   Name of the partition.
  *   \param   resource Resource ID.
  *   \return    daq::rmgr::AllocatedInfo that keeps information about  allocted resource for computer in partition.
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
const daq::rmgr::AllocatedInfo getComputerResourceInfo(const std::string& computer, const std::string& partition, const std::string& resource);

/*!   Gets information about configuration resources and software objects that were loaded into server. 
  *   \return    Pointer to daq::rmgr::Config_updateHelper with according information
  *   \throw    daq::rmgr::LookupFailed Exception raised if failed to find RM server.
  *   \throw    daq::rmgr::CommunicationException System CORBA exception is the reason of this exception.
*/
//daq::rmgr::Config_updateHelper* 
const daq::rmgr::ConfigUpdate getConfigInfo();

};

} // namespace rm
} // namespace daq

#endif				      /* RM_Client_H_INCLUDED */ 
