/*
 *  exceptions.h
 *  rmgr
 *
 */
#ifndef RM_EXCEPTIONS_H
#define RM_EXCEPTIONS_H

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/Assertion.h>

/*! \file exceptions.h Defines RM issues. 
 * \author Igor Alexandrov
 * \version 1.0 
 */

/*! \namespace daq
 *  This is a wrapping namespace for all RM exceptions.
 */
 
namespace daq {
 /*! \namespace daq::rm
 *  This is a wrapping namespace for all RM exceptions.
 */

    /*! \class rmgr::Exception
     *  This is a base class for all RM exceptions.
     */
    ERS_DECLARE_ISSUE( rmgr, Exception, ERS_EMPTY , ERS_EMPTY )

    /*! \class rmgr::ConfigurationException
     *  This exception raised in case of any daq:config exception raised.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ConfigurationException,
                       rmgr::Exception,
                       "RM failed load configuration.",
                       ERS_EMPTY,
                       
                      )

    /*! \class rmgr::PartitionNotFound
     *	This exception raised when there is no required partition in the RM.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
		       PartitionNotFound,
                       rmgr::Exception,
		       "Can not find partition \"" << partition_name << "\" in the RM ",
                       ERS_EMPTY,
		       ((std::string)partition_name )
		      )

    /*! \class rmgr::PartitionAlreadyLoaded
     *	This exception raised if one tryes to load partition into the RM more than once.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       PartitionAlreadyLoaded,
                       rmgr::Exception,
                       "Can not load partition \"" << partition_name << "\". This partition has been already loaded.",
                       ERS_EMPTY,
                       ((std::string)partition_name )
		     )

    /*! \class rmgr::ApplicationNotFound
     *  This exception raised when there is no required application in the partition.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ApplicationNotFound,
                       rmgr::Exception,
                       "Can not find application with name \"" << application_name << "\" in the partition  \"" << partition_name << "\".",
                       ERS_EMPTY,
                       ((std::string)application_name)
                       ((std::string)partition_name )
                     )

    /*! \class rmgr::ObjectNotFound
     *  This exception raised when there is no required object in the RM.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ObjectNotFound,
                       rmgr::Exception,
                       "Can not find object \"" << type << "\".",
                       ERS_EMPTY,
                       ((std::string)type )
                     )

    /*! \class rmgr::UnavailableResources
     *  This exception raised if at least some of required resources are unavailable.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       UnavailableResources,
                       rmgr::Exception,
                       "Can not grant resources." << refuse_mess, //global_resources,
                       ERS_EMPTY,
                       ((std::string) refuse_mess )
                     )
 
    /*! \class rmgr::HandleIdNotFound
     *  This exception raised when there is no required RM handle identifier in the RM.
     */
   ERS_DECLARE_ISSUE_BASE( rmgr,
                      HandleIdNotFound,
                       rmgr::Exception,
                      "Can not find RM handle identifier in the RM." ,
                      ERS_EMPTY, ERS_EMPTY
                    )

    /*! \class rmgr::ResourceNotFound
     *  This exception raised when there is no RM resource in the RM with required identifier.
     */
   ERS_DECLARE_ISSUE_BASE( rmgr,
                      ResourceNotFound,
                       rmgr::Exception,
                      "Can not find in the RM server resource \""<<rid<<"\".",
                      ERS_EMPTY,
                      ((std::string) rid)
                    )
   /*! \class rmgr::SoftwareNotFound
    *  This exception raised if there is no in the RM sofware object with identified id.
    */
   ERS_DECLARE_ISSUE_BASE( rmgr,
                      SoftwareNotFound,
                       rmgr::Exception,
                      "Can not find in the RM server sofware object \""<<swobj<<"\".",
                      ERS_EMPTY,
                      ((std::string) swobj)
                    ) 

    /*! \class rmgr::ComputerNotFound
     *  This exception raised when there is no computer with the required identifier.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                      ComputerNotFound,
                       rmgr::Exception,
                      "Can not find computer with identifier \"" << computer_name << "\" in the RM.",
                      ERS_EMPTY,
                       ((std::string)computer_name )
                    )
    /*! \class rmgr::CommunicationException
     *  This exception raised when any communication exception was appear.
     * This exception usually thorwn when any communication exceptions catched in the  RM.
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CommunicationException,
                       rmgr::Exception,
                       "CommunicationException raised with reason: " << reason,
                       ERS_EMPTY,
                       ((std::string)reason )
                      )
       /*! \class rmgr::LookupFailed
	*  This exception raised when daq.ipc.Exception was raised
	*/
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       LookupFailed,
                       rmgr::Exception,
                       "Lookup failed for the RM server ",
                       ERS_EMPTY,
                      )
       /*! \class rmgr::IPCException
	*   This exception raised when daq.ipc.Exception was raised
	*/
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       IPCException,
		       rmgr::Exception,
                       "RM server problem: "<<mess,
                       ERS_EMPTY,
                       ((std::string)mess )
                      )
	
    /*! \class rmgr::STDExceptioon
     *   std::exception raised during RM actions
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       STDExceptioon,
                       rmgr::Exception,
                       "std exception in RM : "<<reason,
                       ERS_EMPTY,
                       ((std::string) reason )
                      )

    /*! \class rmgr::ProcessNotFound
     *  This exception raised when for according computer there is no process for which any resource was granted).
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ProcessNotFound,
                       rmgr::Exception,
                       "Can not find process with pid=" << pid << " on the computer \""<< computer << "\" those that for this process any resource in the partition \"" << partition << "\" was granted by the RM.",
                       ERS_EMPTY,
                       ((std::string)partition )
                       ((std::string)pid )
                       ((std::string)computer )
                     )
    /*! \class rmgr::AcceessDenied
     *  This exception raised if Access Manager did not allow to start action
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       AcceessDenied,
                       rmgr::Exception,
                       "Access Manager did not allow to free all resources in partition \"" << partition <<"\" for user " << myname << " from host " << myhost,
                       ERS_EMPTY,
                       ((std::string)partition )
                       ((std::string)myname )
                       ((std::string)myhost )
                     )
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       AcceessAnyDenied,
                       rmgr::Exception,
                       "Access Manager did not allow to free resources or show resources info for user " << myname << " from host " << myhost,
                       ERS_EMPTY,
                       ((std::string)myname )
                       ((std::string)myhost )
                     )

    /*! \class rmgr::CanNotStartServer
     *  This exception raised if RM_Server can not start by some reason
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CanNotStartServer,
                       rmgr::Exception,
                       "RM_Server did not started. Reason:" << reason,
                       ERS_EMPTY,
                       ((std::string)reason )
                     )

    /*! \class rmgr::CannotAcquireToken
     *  This exception raised if token was not acquired from daq::tokens
     */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotAcquireToken,
                       rmgr::Exception,
                       "Cannot acquire DAQ token.",
                       ERS_EMPTY,
                       ERS_EMPTY
                     )

    /*! \class rmgr::CannotVerifyToken
    *  This exception raised if token was not verified in the RM_Server
    */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotVerifyToken,
                       rmgr::Exception,
                       "RM server cannot verify DAQ token.",
                       ERS_EMPTY,
                       
                     )
    /*! \class rmgr::CannotOpenJournal
    *   This exception raised if can not open journal
    */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotOpenJournal,
                       rmgr::Exception,
                       "Exception opening journal file \"" << log_file << "\". Exception: " << reason << " Error code: " << errcode,
                       ERS_EMPTY,
                       ((std::string) log_file )
                       ((std::string) reason )
                       ((std::string) errcode )
                     )
    /*! \class rmgr::CannotWriteToJournal
    *   This exception raised if can not write to journal
    */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotWriteToJournal,
                       rmgr::Exception,
                       "Can not write to journal \""<<log_file << "\".  Exception during write with the reason: " << reason<<" Error code: "<< errcode,
                       ERS_EMPTY,
                       ((std::string) log_file )
                       ((std::string) reason )
                       ((std::string) errcode )
                     )
    /*! \class rmgr::WrongJournal
    *  This exception raised in journal file reading
    */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       WrongJournal,
                       rmgr::Exception,
                       "Journal file reading exception. Description: \n" << reason,
                       ERS_EMPTY,
                       ((std::string)reason )
                     )
   /*! \class rmgr::CannotSaveArchive
   *  This exception raised if can not save backup file
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotSaveArchive,
                       rmgr::Exception,
                       "Can not write to backup file \""<< bck_name<<"\"",
                       ERS_EMPTY,
                       ((std::string) bck_name )
                     )
   /*! \class rmgr::FailedRestoreFromArchive
   *   This exception raised if can not restore server info from backup file
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       FailedRestoreFromArchive,
                       rmgr::Exception,
                       "Failed restore from backup file \""<<bck_name<<"\". Reason: " << reason,
                       ERS_EMPTY,
                       ((std::string) bck_name )
                       ((std::string) reason)
                     )
   /*! \class rmgr::NoBackupFile
 * This exception raised if backup file does not exist
 */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       NoBackupFile,
                       rmgr::Exception,
                       "Backup file \""<<bck_name<<"\" does not exist",
                       ERS_EMPTY,
                       ((std::string) bck_name )
                     )
   /*! \class rmgr::BackupFileIsDirectory
 * This exception raised if backup file is a directory
 */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       BackupFileIsDirectory,
                       rmgr::Exception,
                       "Backup file \""<<bck_name<<"\" is a directory. File name must be present.",
                       ERS_EMPTY,
                       ((std::string) bck_name )
                     )
   /*! \class rmgr::NoJournal
 *  This exception raised if journal does not exist
 */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       NoJournal,
                       rmgr::Exception,
                       "Journal  \"" << name << "\" does not exist",
                       ERS_EMPTY,
                       ((std::string) name ) 
                     )
   /*! \class rmgr::CopyJournalFailed
   *    This exception raised if I/O error during copy journal and cretion new one
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CopyJournalFailed,
                       rmgr::Exception,
                       "Copy journal and open new journal failed.",
                       ERS_EMPTY,
                       ERS_EMPTY
                    )
   /*! \class rmgr::closeStreamFailed
   *   This exception raised if named stream close problem appair
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       closeStreamFailed,
                       rmgr::Exception,
                       "close logout stream of "<<name<<" exception. Reason: "<<reason<<"Error code"<<errcode,
                       ERS_EMPTY,
                       ((std::string) name )
                       ((std::string) reason ) 
                       ((std::string) errcode )
                    )




   /*! \class rmgr::ServerAlreadyRunning
   *   Try to start RM_Server but it already runnig
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ServerAlreadyRunning,
                       rmgr::Exception,
                       "Resource manager server with name \""<<name<<"\"  already running in default partition.",
                       ERS_EMPTY,
                       ((std::string) name )
                    )
   /*! \class rmgr::CannotGetAllocatedinfo
   *   std::Exception when allocated info resource to be produced
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotGetAllocatedinfo,
                       rmgr::Exception,
                       "Cannot get allocated respurces info. Reason: ",
                        ERS_EMPTY,
                       ((std::string) reason )
                    )
   /*! \class rmgr::CannotCreateFreeResMess
   *   This exception raised if can not create free resource message
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       CannotCreateFreeResMess,
                       rmgr::Exception,
                       "Can not create free resource message. Reason: ",
                        ERS_EMPTY,
                       ((std::string) reason )
                    )
   /*! \class rmgr::JournalDontOpen
   *   Backup should be produced but journal file did not open
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       JournalDontOpen,
                       rmgr::Exception,
                       "Journal file with name "<<name<<" did not open.",
                       ERS_EMPTY,
                       ((std::string) name )
                    )
   /*! \class rmgr::JournalFileBadState
   *    Bad state of jopurnal file
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       JournalFileBadState,
                       rmgr::Exception,
                       "Bad state of journal file with name "<<name,
                       ERS_EMPTY,
                       ((std::string) name )
                    )
   /*! \class rmgr::ServerDntStart
   *   Server did not started
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       ServerDntStart,
                       rmgr::Exception,
                       "RM_Server did not start. Reason: ",
                       ERS_EMPTY,
                       ((std::string) reason )
                    )
   /*! \class rmgr::AccessManagerDontAllow
   *   Access Manager does not allow to call method
   */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       AccessManagerDontAllow,
                       rmgr::Exception,
                       "Access Manager does not allow to call method " << function_name << " on host " << mhost <<  " for user " << user,
                       ERS_EMPTY,
                       ((std::string) function_name )
                       ((std::string) mhost )
                       ((std::string) user )
                    )
   /*! \class rmgr::FailedCreateBckParentDir
 *     Backup file does not exist and RM server can not create parent directory for backup
 */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       FailedCreateBckParentDir,
                       rmgr::Exception,
                       "Failed to create parent directory \"" << dir << "\" for backup: " << reason << "  Error code " << ercode,
                       ERS_EMPTY,
                       ((std::string) dir)
                       ((std::string) reason)
                       ((std::string) ercode)
                    )

   /*! \class rmgr::FailedWriteToBackupDir
 *     RM server has no rights to write to backup parent directory
 */
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       FailedWriteToBackupDir,
                       rmgr::Exception,
                       "Failed to write to backup directory \"" << dir << "\". Reason: " << reason<< " Error code: " << ercode,
                       ERS_EMPTY,
                       ((std::string) dir)
                       ((std::string) reason)
                       ((std::string) ercode)
                    )
 
   /*! \class rmgr::HostUknown
 *    *  Have exception in system::Host
 *       * /
    ERS_DECLARE_ISSUE_BASE( rmgr,
                       HostUknown,
                       rmgr::Exception,
                       "Can not generate stamp for call to request resources. Exception in getting host name using system::Host ",
                       ERS_EMPTY,
                       ERS_EMPTY 
                    )
*/

}
#endif 

