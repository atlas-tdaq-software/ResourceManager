#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include <cstdlib>
#include <ctime>

#include <iostream>
#include <filesystem>
#include <queue>
#include <thread>
#include <shared_mutex>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ers/ers.h>

#include <ResourceManager/RM_Client.h>
#include "src/idlhelper.h"

daq::rmgr::RM_Client* RMC = nullptr;

std::atomic<int_least64_t> okcheck = EXIT_SUCCESS;

// resource and software objects names as defined in OKS configuration
const std::string c_res_100k_1m("hw_res_100k_1M");
const std::string c_bin_100k_1m("Binary_hw_100k_1M");


static long
test_request_resource(const std::string& partition, const std::string& resource, const std::string& application, const std::string& computer, bool request_for_my_process, int proc_id, const std::string& client)
{
  long retv = -1;

  try
    {
      if (request_for_my_process)
        {
          if (proc_id < 0)
            retv = RMC->requestResourceForMyProcess(partition, resource, computer, client);
          else
            retv = RMC->requestResource(partition, resource, computer, client, proc_id);
        }
      else
        {
          if (proc_id < 0)
            retv = RMC->requestResources(partition, resource, computer, client, application);
          else
            retv = RMC->requestResources(partition, resource, computer, client, application, proc_id);
        }

      if (proc_id < 0)
        RMC->setProcess(retv, -proc_id);
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }

  return retv;
}

static void
test_get_resources_info(const std::string& partition, const std::string& resource)
{
  try
    {
      daq::rmgr::AllocatedInfo res(RMC->getResourceInfo(partition, resource));
    }
  catch (const daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_get_full_resource_info(const std::string& resource)
{
  try
    {
      RMC->getFullResInfo(resource);
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      ers::error(exproc);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_get_computer_resource_info(const std::string& computer, const std::string& partition, const std::string& resource)
{
  try
    {
      RMC->getComputerResourceInfo(computer, partition, resource);
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      ers::error(exproc);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_get_computer_info(const std::string& computer)
{
  try
    {
      RMC->getComputerInfo(computer);
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      ers::error(exproc);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_partition_info(const std::string& partition)
{
  try
    {
      RMC->getPartitionInfo(partition);
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      ers::error(exproc);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_partition_list()
{
  try
    {
      RMC->getPartitionsList();
    }
  catch (const daq::rmgr::Exception& ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_handle_info(long hndl)
{
  try
    {
      RMC->getHandleInfo(hndl);
    }
  catch(const daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_get_resource_owners( const std::string& partition, const std::string& resource)
{
  try
    {
      RMC->getResourceOwners(resource, partition);
    }
  catch (const daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}


static void
test_free_partition(const std::string& partition)
{
  try
    {
      RMC->freeAllInPartition(partition);
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_free_computer(const std::string& computer)
{
  try
    {
      RMC->freeAllOnComputer(computer);
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_config_update(const std::string& partition)
{
  try
    {
      RMC->rmConfigurationUpdate( partition );
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static void
test_config_info()
{
  try
    {
       RMC->getConfigInfo();
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
}

static bool
test_free_resource_by_handle(unsigned long handle)
{
  try
    {
      RMC->freeResources( handle );
      return true;
    }
  catch ( daq::rmgr::Exception & ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
      return false;
    }
}


static void
test_free_process_resource(const std::string partition, const std::string computer, unsigned long mypid)
{
  try
    {
      RMC->freeProcessResources(partition, computer, mypid);
    }
  catch (daq::rmgr::Exception &ex)
    {
      ers::error(ex);
      okcheck = EXIT_FAILURE;
    }
} 

std::vector<std::string> s_partitions, s_computers;
std::atomic<uint_least64_t> tests_count = 0;
unsigned int tests_num(100000);
unsigned int partition_num(16);
unsigned int computer_num(1024);
unsigned int clear_computer_sleep(1);
unsigned int clear_partition_sleep(10);
unsigned int log_threshold = 1000;
unsigned int log_low_threshold = 200;

struct ResInfo {
  long m_handler;
  uint16_t m_pid;
  uint16_t m_partition_idx;
  uint16_t m_host_idx;

  ResInfo(long handler, uint16_t pid, uint16_t partition_idx, uint16_t host_idx) :
    m_handler(handler), m_pid(pid), m_partition_idx(partition_idx), m_host_idx(host_idx)
    {
    }

  static ResInfo get()
  {
    {
      std::shared_lock lock(s_resources_mutex);
      if (!s_resources.empty())
        return s_resources.front();
    }

    return ResInfo(0, 0, 0, 0);
  }

  static std::queue<ResInfo> s_resources;
  static std::shared_mutex s_resources_mutex;
};

std::queue<ResInfo> ResInfo::s_resources;
std::shared_mutex ResInfo::s_resources_mutex;

void request_resource(int thread_id)
{
  std::string client_id("client_");
  client_id += std::to_string(thread_id);

  std::string application_id("application_");
  application_id += std::to_string(thread_id);

  while (tests_count < tests_num && okcheck != EXIT_FAILURE)
    {
      auto x = rand();
      int16_t pid = x % std::numeric_limits<int16_t>::max() + 1;
      int16_t part_idx = x % partition_num;
      int16_t host_idx = x % computer_num;
      bool test_sw_obj = ((x % 2) == 1);

      auto handler = test_request_resource(s_partitions[part_idx], (test_sw_obj ? c_bin_100k_1m : c_res_100k_1m), application_id, s_computers[host_idx], (test_sw_obj == false), ((x % 16) < 8 ? pid : -pid), client_id);

      tests_count++;

      if (tests_count % log_threshold == 0)
        ERS_LOG("insert thread [" << thread_id << "]: requested " << tests_count << " resources in total");

      std::unique_lock lock(ResInfo::s_resources_mutex);
      ResInfo::s_resources.emplace(handler, pid, part_idx, host_idx);
    }
}

// free resource by handler
void free_resource(int thread_id)
{
  while (okcheck != EXIT_FAILURE)
    {
      auto info = ResInfo::get();

      if (info.m_handler == 0)
        {
          if (tests_count < tests_num)
            {
              std::this_thread::sleep_for(std::chrono::milliseconds(1));
              continue;
            }
          else
            {
              ERS_LOG("free resources thread [" << thread_id << "] terminates");
              break;
            }
        }

      if (info.m_handler && test_free_resource_by_handle(info.m_handler) == true)
        {
          std::unique_lock lock(ResInfo::s_resources_mutex);

          if (ResInfo::s_resources.front().m_handler == info.m_handler)
            {
              static unsigned int freed_count(0);

              ResInfo::s_resources.pop();

              freed_count++;

              if (freed_count % log_threshold == 0)
                ERS_LOG("free by handle thread [" << thread_id << "]: freed " << freed_count << " resources in total");
            }
        }
    }
}

// free resources by pid, computer or partition
void clear_resources(int thread_id)
{
  static std::atomic<int_least64_t> clear_count(0);

  while (okcheck != EXIT_FAILURE)
    {
      auto x11 = rand() % 11;

      if (x11 < 2)
        {
          test_free_partition(s_partitions[rand() % partition_num]);
          std::this_thread::sleep_for(std::chrono::milliseconds(clear_partition_sleep));
        }
      else if (x11 < 7)
        {
          test_free_computer(s_computers[rand() % computer_num]);
          std::this_thread::sleep_for(std::chrono::milliseconds(clear_computer_sleep));
        }
      else
        {
          auto info = ResInfo::get();

          if (info.m_handler)
            {
              test_free_process_resource(s_partitions[info.m_partition_idx], s_computers[info.m_host_idx], info.m_pid);
            }
          else if (tests_count < tests_num)
            {
              std::this_thread::sleep_for(std::chrono::milliseconds(1));
              continue;
            }
          else
            {
              ERS_LOG("clear resources thread [" << thread_id << "] terminates");
              break;
            }
        }

      clear_count++;

      if (clear_count % log_low_threshold == 0)
        ERS_LOG("clear thread [" << thread_id << "]: performed " << clear_count << " calls in total");
    }
}

void get_info(int thread_id)
{
  static std::atomic<int_least64_t> info_count(0);

  while (okcheck != EXIT_FAILURE)
    {
      auto x29 = rand() % 29;

      if (x29 < 3)
        {
          test_get_computer_info(s_computers[rand() % computer_num]);
        }
      else if (x29 < 6)
        {
          test_partition_info(s_partitions[rand() % partition_num]);
        }
      else if (x29 < 9)
        {
          test_partition_list();
        }
      else if (x29 < 11)
        {
          test_get_resource_owners(s_partitions[rand() % partition_num], c_res_100k_1m);
        }
      else if (x29 < 13)
        {
          auto info = ResInfo::get();
          test_get_computer_resource_info(s_computers[info.m_host_idx], s_partitions[info.m_partition_idx], c_res_100k_1m);
        }
      else if (x29 < 16)
        {
          test_get_resources_info(s_partitions[rand() % partition_num], c_res_100k_1m);
        }
      else if (x29 < 18)
        {
          test_get_full_resource_info(c_res_100k_1m);
        }
      else if (x29 < 20)
        {
          test_config_info();
        }
      else if (x29 < 21)
        {
          test_config_update(s_partitions[rand() % partition_num]);
        }
      else
        {
          auto info = ResInfo::get();

          if (info.m_handler)
            {
              test_handle_info(info.m_handler);
            }
          else
            {
              if (tests_count >= tests_num)
                {
                  ERS_LOG("get info thread [" << thread_id << "] terminates");
                  break;
                }
              else
                test_partition_list();
            }
        }

      info_count++;

      if (info_count % log_threshold == 0)
        ERS_LOG("get infos thread [" << thread_id << "]: performed " << info_count << " calls in total");
    }
}

int main(int argc, char **argv)
{
  try
    {
      IPCCore::init(argc,argv);
    }
  catch( daq::ipc::Exception & ex ) {
      ers::fatal( ex );
    }

  unsigned int insert_threads(4);
  unsigned int remove_threads(4);
  unsigned int clear_threads(2);
  unsigned int info_threads(2);

  try
    {
      boost::program_options::options_description desc(
        "Stress check for RM server to provoke race conditions"
        "\n"
        "Available options are:");

      desc.add_options()
        ("num,n", boost::program_options::value<unsigned int>(&tests_num)->default_value(tests_num), "number of resources to be inserted")
        ("partitions,p", boost::program_options::value<unsigned int>(&partition_num)->default_value(partition_num), "number of partitions")
        ("computers,c", boost::program_options::value<unsigned int>(&computer_num)->default_value(computer_num), "number of computers")
        ("number-insert-threads,i", boost::program_options::value<unsigned int>(&insert_threads)->default_value(insert_threads), "number of insert threads")
        ("number-remove-threads,r", boost::program_options::value<unsigned int>(&remove_threads)->default_value(remove_threads), "number of remove threads by resource handle")
        ("number-clear-threads,x", boost::program_options::value<unsigned int>(&clear_threads)->default_value(clear_threads), "number of clear threads (random by partition, computer, pid)")
        ("number-info-threads,l", boost::program_options::value<unsigned int>(&info_threads)->default_value(info_threads), "number of info threads")
        ("partition-clear-sleep,P", boost::program_options::value<unsigned int>(&clear_partition_sleep)->default_value(clear_partition_sleep), "sleep interval in ms after partition resources clear")
        ("computer-clear-sleep,C", boost::program_options::value<unsigned int>(&clear_computer_sleep)->default_value(clear_computer_sleep), "sleep interval in ms after computer resources clear")
        ("verbose,v", "Report details of tests")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      boost::program_options::notify(vm);
    }
  catch (const std::exception& ex)
    {
      std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
      return EXIT_FAILURE;
    }

  log_threshold = tests_num / 10;
  log_low_threshold = tests_num / 50;

  for (unsigned int i = 1; i <= partition_num; ++i)
    s_partitions.push_back(std::string("part_") + std::to_string(i));

  for (unsigned int i = 1; i <= computer_num; ++i)
    s_computers.push_back(std::string("pc-test-") + std::to_string(i));

  std::srand(std::time({}));

  RMC = new daq::rmgr::RM_Client("RM_Server_check");

  std::vector<std::thread*> threads;
  threads.reserve(insert_threads+remove_threads+info_threads+clear_threads);

  for (unsigned int i = 0; i < insert_threads; ++i)
    threads.push_back(new std::thread(request_resource, i));

  ERS_LOG("started " << insert_threads << " insert threads");

  for (unsigned int i = 0; i < remove_threads; ++i)
    threads.push_back(new std::thread(free_resource, i));

  ERS_LOG("started " << remove_threads << " free resource threads");

  for (unsigned int i = 0; i < clear_threads; ++i)
    threads.push_back(new std::thread(clear_resources, i));

  ERS_LOG("started " << clear_threads << " clear resources threads");

  for (unsigned int i = 0; i < info_threads; ++i)
    threads.push_back(new std::thread(get_info, i));

  ERS_LOG("started " << info_threads << " get info threads");

  for (auto& x : threads)
    {
      x->join();
      delete x;
    }

  delete RMC;

  ERS_LOG("Stress test " << (okcheck == EXIT_SUCCESS ? "passed" : "failed"));

  return okcheck;
}

