#include <iostream>
#include <unistd.h>
#include <time.h>
#include <boost/program_options.hpp>

#include <ipc/core.h>
#include "ers/ers.h"
#include <ResourceManager/RM_Client.h>

  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;
int main(int argc, char** argv)
{
/*
 	// Declare arguments
 	CmdArgStr	partition_name('p', "partition", "partition_id", "partition identifier (mandatory).", CmdArg::isREQ);
        CmdArgStr       binary_name('b', "binary", "binary_id", "Software  object (binary) identifier. Object for  which resources to be granted.", CmdArg::isREQ);
        CmdArgStr       computer_name('c', "computer", "computer_id", "identifier of the computer on which binary supposed to be started.", CmdArg::isREQ);
 	CmdArgStr  rm_server_name('n', "rm_server", "rm_server_name", 
			  "resource manager server name; use only for tests.");
        CmdArgStr  resource_id('r', "resource", "resource_id", "!!Used only for test with \'t\' parameter. UID of the RM resource to be requested for process.");
        CmdArgBool test_process   ('t', "test_process",
                                              "!!! Only for tests!! Request resources for process that will be started by this binary.");
 CmdArgInt       startime_v('T', "startime", "startime_v", "Time when to start request to RM_Server.");
        
	// Declare command object and its argument-iterator
 	CmdLine cmd(*argv, &partition_name, //&application_name, 
                   &binary_name, &computer_name, 
		     &resource_id, &test_process,
                    &rm_server_name, &startime_v, NULL);
 	CmdArgvIter arg_iter(--argc, ++argv);
        cmd.description("Request Resource Manager resources for binary to be started on the computer on time");
*/

  std::string rm_server_name("");
  std::string partition_name;
  std::string binary;
  std::string computer;
  std::string resource_id("");
  bool test_process( false );
  int startime_v(0);

  try {
    boost::program_options::options_description desc(
      "Request Resource Manager resources for binary to be started on the computer on time"      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("partition,p",   boost::program_options::value<std::string>(&partition_name)->required(), "partition within which resources should be requested (mandatory)")
        ("computer,c",  boost::program_options::value<std::string>(&computer)->required(), "computer running the application whose resources should be requested (mandatory)")
        ("binary,b", boost::program_options::value<std::string>(&binary)->required(), "Software  object (binary) identifier. Object for  which resources to be granted (mandatory).")
        ("resource,r", boost::program_options::value<std::string>(&resource_id)->default_value(resource_id), "!!Used only for test with \'t\' parameter. UID of the RM resource to be requested for process.")
        ("test_process,t", "!!! Only for tests!! Request resources for process that will be started by this binary.")
        ("startime,T", boost::program_options::value<int>(&startime_v)->default_value(startime_v), "Time when to start request to RM_Server.")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }
      if( vm.count("test_process") ) {
         if( vm.count("resource") )
            test_process = true;
         else {
             ers::error(ers::Message( ERS_HERE, "For test_process true resource should be set as well"));
             return EXIT_FAILURE;          
         }
      }
      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }

  startime_v = 0; ////?

  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
  }

  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found
  int proc = test_process ? 1 : 0;
  long handleId;
  std::ostringstream txt ;
  if (partition_name.empty())
  {
        txt<<"rm_allocate_resources: No partition name in input.";
        ers::warning (ers::Message( ERS_HERE, txt.str()));
  }
  else if (binary.empty() && !proc)
  {
        txt<<"rm_allocate_resources: No binary ID in input.";
        ers::warning (ers::Message( ERS_HERE, txt.str()));
  }
  else if (!resource_id.empty() && proc) {
//only for tests for some time
    try {
      handleId = RMC->requestResourceForMyProcess( partition_name, resource_id, computer, "test");
      txt<<"Handle ID = " <<  handleId;
      ers::info( ers::Message( ERS_HERE, txt.str()));
    }
    catch ( daq::rmgr::Exception & ex) {
          ers::error(ex);
    }
  }
  else
  {
    try {
        if( !(startime_v == 0) ) {
           long sec0 = startime_v - (long)time(NULL);
           unsigned int sec = sec0 > 0 ? (unsigned int) sec0 : 0 ;
           std::cerr<<"                       sleep="<<sec<<std::endl;
           sleep(sec);
        }
        if( !(startime_v == 0) )
	   std::cerr<<"     ###  RMC->requestResources start ... time="<<time(NULL)<<std::endl;

      	handleId = RMC->requestResources( partition_name, binary, computer );

        if( !(startime_v == 0)  ) {
            std::cerr<<"     @@@@@ RMC->requestResources finished. Time="<<time(NULL)<<std::endl;
            txt<<"Handle ID = " << handleId;
            ers::info( ers::Message( ERS_HERE, txt.str()));
            ers::info( ers::Message( ERS_HERE, txt.str()));
        }
    }     
    catch ( daq::rmgr::Exception & ex) {
          ers::error(ex);
    } 
  }
  delete RMC;

  if( !(startime_v == 0)  )
     std::cerr<<"     $$$$$$$$$ exit requestResources time="<<time(NULL)<<std::endl;
}
