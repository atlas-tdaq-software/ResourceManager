#include <cstdlib>
#include <boost/program_options.hpp>
#include <string>
#include <stdio.h>
#include <iostream>
#include <ipc/core.h>

#include "src/RM_ClientImpl.h"
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <ers/ers.h>
#include <boost/spirit/include/karma.hpp>

static std::string prefixComp = "lnxatd";
static std::string postfixComp = ".cern.ch";  

struct MyResVal {
 daq::rmgr::ResourceType type;
 int maxpp;
 int max;
 MyResVal( daq::rmgr::ResourceType t, int mp, int mt) { type=t; maxpp=mp; max=mt; }
};

/*
struct MyCompVal {
 int mem;
 int freq;
 int cores;
 MyCompVal(int m, int f, int c) { mem=m; freq=f; cores=c; }
};
*/

class MyRMResource {
public:
//here resource data and allocation data
  std::string id;
  int resType; //sw,hw,calc
  
  std::string  client;
  std::string  computer;
  std::string  swObj;
  long maxPP, grantedPP;
  int mem, freq, cores;
  int gotmem, gotfreq, gotcores, handle;
  unsigned int pid;

  MyRMResource( std::string& id1, int resType1, std::string & client1, std::string& computer1, std::string& swObj1, long  maxPP1,  long grantedPP1, long handle1, unsigned int pid1 )
   : id(id1), resType(resType1), client(client1),
     computer(computer1), swObj(swObj1), maxPP(maxPP1),
     grantedPP(grantedPP1), mem(0), freq(0), cores(0), gotmem(0),
     gotfreq(0), gotcores(0), handle(handle1), pid(pid1)  {
   ;
 }
  MyRMResource( std::string& id1, int resType1, std::string & client1, std::string& computer1, std::string& swObj1, int mem1, int freq1, int cores1, int gotmem1, int gotfreq1, int gotcores1 , long handle1, unsigned int pid1)
   : id(id1), resType(resType1), client(client1),
     computer(computer1), swObj(swObj1), maxPP(1),
     grantedPP(1), mem(mem1), freq(freq1), cores(cores1), gotmem(gotmem1),
     gotfreq(gotfreq1), gotcores(gotcores1), handle(handle1), pid(pid1)  {
   ;
 }

};

namespace daq {
namespace rmgr {
 std::string& int2str( int inp, std::string& outstr ) {
    char buf[32];
    char* ptr;
    ptr = buf;
    boost::spirit::karma::generate(ptr, boost::spirit::int_, inp);
    outstr.assign(buf, ptr - buf);
    return outstr;
 }

}
}

  enum RmOper { 
     opRequestResources, 
     opFreeResources, 
     opFreeAllPartitionResources,
     opRequestResourceForProcess, 
     opFreeProcessResources, 
     opFreeAllHostResources, 
     opGetResourceInfo, 
     opFreeAllAppResources, 
     opUpdateConfig,
     opLast } ;

  struct RMoperation {
    RmOper op; //operation to be performed
    std::string name;
    unsigned  int w; //weight;
  };
  
  static RMoperation operations[] = { 
        { opRequestResources,"opRequestResources", 5000 },
        { opFreeResources, "opFreeResources", 3500 },
        { opFreeAllPartitionResources, "opFreeAllPartitionResources", 20 },
        { opRequestResourceForProcess, "opRequestResourceForProcess", 650 },
        { opFreeProcessResources, "opFreeProcessResources", 320 },
        { opFreeAllHostResources, "opFreeAllHostResources", 100 },
        { opGetResourceInfo, "opGetResourceInfo", 0 },
        { opFreeAllAppResources, "opFreeAllAppResources", 300 },
        { opUpdateConfig, "opUpdateConfig", 2 }
      };
 static int opCallBound[opLast];

//// static int boundLen = opLast;

//for debug only
 static long opN[opLast] = {0,0,0,0,0,0,0,0};
 static long successHandles = 0;

static RmOper generateRmOper() {
   static int count;
   static bool boundsInited = false;
 if( !boundsInited ) {
   timeval time2;
   gettimeofday( &time2, 0 );
   long time02 = time2.tv_sec;
   unsigned int init = time02;
   srand( init);
   rand();
   //normalize weighs to 1000
   unsigned int all = 0;
   int i;
   std::cerr<<"Weights on input:";
   for(i=0; i< opLast; i++) {
      all += operations[i].w;
      std::cerr<<" " <<operations[i].name<<"="<<operations[i].w<<"; ";
   }
   std::cerr<<std::endl<<std::endl;

   double scale = 10000. / all;
   std::cerr<<"Normalized to 1000 weights:";
   for(i=0; i< opLast; i++) {
       operations[i].w = (int) (scale * operations[i].w);
       std::cerr<<" " <<operations[i].name<<"="<<operations[i].w<<"; ";
   }
   std::cerr<<std::endl<<std::endl;
   count = 0; //test
   boundsInited = true;
   int cur = 0;
   std::cerr<<"Acrroding values of bounds: ";
   for (int i0 = 0; i0 < (opLast-1); i0++ ) {
      cur += operations[i0].w;
      opCallBound[ i0 ] =  cur;
      std::cerr<<" " <<cur;       
   }  
   std::cerr<<std::endl;
 }

   int ropind = rand() % 10000;
//just for first tests To be removed
   if( count < 6 )
      std::cerr<<"    ropind="<<ropind<<std::endl;
   count++;
  
   for( int j=0; j< (opLast-1); j++ )
     if( ropind < opCallBound[ j ] ) {
       opN[j]++;
       if( count < 6 )  std::cerr<<" operaton: "<<operations[j].name<<std::endl;
       return operations[j].op;
     }
   opN[opLast-1]++;
   return operations[opLast-1].op;
}

 static MyResVal swres[] = { MyResVal(daq::rmgr::SWR,1,1000),
         MyResVal(daq::rmgr::SWR,1,100),
         MyResVal(daq::rmgr::SWR,1000,1000),
         MyResVal(daq::rmgr::SWR,5,10),
         MyResVal(daq::rmgr::HWR,1,1),
         MyResVal(daq::rmgr::HWR,1,1),
         MyResVal(daq::rmgr::HWR,3,5) };
 static int swresSize = sizeof(swres)/sizeof(MyResVal);
//// static MyCompVal clcres[] = { MyCompVal( 100, 2000, 1),
////                         MyCompVal( 30, 900, 2) };
//// static int clcresSize = sizeof(clcres)/sizeof(MyCompVal);
 //num of SW,HW resources, SWObjects
 static int numswr = 0;
 static int numhwr = 0;
 static int numswo = 0;
 static long freNum = 0;
 
 typedef std::multimap<std::string, MyRMResource *, std::less<std::string> > MyResMap;
 static MyResMap resources;

void create_conf_data( std::string& pname, int numOfHosts, int no_common_r, daq::rmgr::Config_update & idl_conf) {
    std::cerr<<"  create_conf_data!!! num of hosts="<<numOfHosts<<std::endl;

    std::string compName = "lnxatd00.cern.ch_";
    std::string swName = !no_common_r ? "_SW_ID_" : pname + "_SW_ID_";
    std::string resNameHw ("RM_HW_ID_");
    if(no_common_r)
         resNameHw = pname + resNameHw;
    std::string resNameSw ("RM_SW_ID_");
    if(no_common_r)
         resNameSw = pname + resNameSw;
    std::string resName;
    std::string numstr;
    //calculate number of HW SW res
    for(int ii1=0; ii1<swresSize; ii1++)
       if( swres[ii1].type == daq::rmgr::SWR )
          numswr++;
       else
          numhwr++;
     std::string rn;
 
    //SW, HW resources
    idl_conf.swr_lst.length( numswr );
    idl_conf.hwcr_lst.length( numhwr);
    //SWObjects 
    ////idl_conf.swobj_lst.length( numswr + numhwr + clcresSize );
    idl_conf.swobj_lst.length( numswr + numhwr );
      numswr = -1;
      numhwr = -1;
      numswo = -1;

    for( int ii2=0; ii2 < swresSize; ii2++ ) {
        if( swres[ii2].type == daq::rmgr::SWR ) {
         rn = resNameSw + daq::rmgr::int2str( ++numswr, numstr); 
         idl_conf.swr_lst[numswr].rid = CORBA::string_dup( rn.c_str() );
         idl_conf.swr_lst[numswr].max_total = swres[ii2].max;
         idl_conf.swr_lst[numswr].max_pp = swres[ii2].maxpp;
        }
        else {
         rn = resNameHw + daq::rmgr::int2str( ++numhwr, numstr);
         idl_conf.hwcr_lst[numhwr].rid = CORBA::string_dup( rn.c_str() );
         idl_conf.hwcr_lst[numhwr].max_total = swres[ii2].max;
         idl_conf.hwcr_lst[numhwr].max_pp = swres[ii2].maxpp;
        }
        //now SWObject 
        numswo++;
        std::string swn = swName + daq::rmgr::int2str(numswo, numstr);
        idl_conf.swobj_lst[numswo].swid = CORBA::string_dup( swn.c_str() );
        idl_conf.swobj_lst[numswo].res_list.length( 1 );
        idl_conf.swobj_lst[numswo].res_list[0]  = CORBA::string_dup( rn.c_str() );
    }
numswo++;
numhwr++;
numswr++;
std::cerr<<"        AFTER CALC numhwr="<<numhwr<<"   numswr="<<numswr<<std::endl;
std::cerr<<"        AFTER CALC numswo="<<numswo<<std::endl;
}

long getMaxPP( int ind) {
//get maxPP using SWObject index (for HW,SW resources)
  if(ind < numswr)
   return swres[ind].maxpp;
  else
   return EXIT_FAILURE; //error
}

static int res_type_max = 100;

static daq::rmgr::ResourceType generateType() {
   static int iGenType;
   iGenType = rand() % res_type_max;
   if(iGenType < 80)
      return daq::rmgr::HWR;
   else 
      return daq::rmgr::SWR;
}

static int numOfComp = 100;

std::string& generateComp( std::string& retv ) {
  int ihscale;
  ihscale = rand() % numOfComp;
  std::string numstr;
 retv = prefixComp + daq::rmgr::int2str( ihscale, numstr )  + postfixComp;
 return retv;
}

static int generateResIndex(daq::rmgr::ResourceType type) {
  int ind;
  switch ( type ) {
    case daq::rmgr::HWR:
      ind = rand() % numhwr;
      ind += numswr;
      break;
    default:
      ind = random() % numswr;
      break;
  }
  return ind;
}

long getHandleByIndex( int ind1 ) {
 if( opN[1]< 6 )
      std::cerr<<" getHandleByIndex: ind1="<<ind1<<" number of handles="<< resources.size()<<std::endl;

  int search = 0;
  for ( MyResMap::iterator i=resources.begin() ; i != resources.end(); i++) {
     if(search == ind1) {
       MyRMResource* rs = (*i).second;
       return rs->handle;
     }
     search++;
  }
  return EXIT_FAILURE;
}

void removeResource( long hndl) {
  for ( MyResMap::iterator i=resources.begin() ; i != resources.end(); ) {
       MyRMResource* rs = (*i).second;
       if( rs->handle == hndl) {
         resources.erase(i++);
         freNum++;
         delete rs;
         return;
       }
       else
        ++i;
  }
}

void removeResources( std::string& comp, std::string& bin ) {
  for ( MyResMap::iterator i=resources.begin() ; i != resources.end(); ) {
       MyRMResource* rs = (*i).second;
       if( ((comp.size() == 0) &&  (bin.size() == 0) )
          || ( (bin.size() == 0) && rs->computer == comp )
          || ( rs->computer == comp && rs->swObj == bin)
         ) {
          resources.erase(i++);
          freNum++;
          delete rs;
       }
       else
        ++i;
  }
}

void removeResources( unsigned int  mpid ) {
  for ( MyResMap::iterator i=resources.begin() ; i != resources.end(); ) {
       MyRMResource* rs = (*i).second;
       if( rs->pid > 0 && rs->pid == mpid ) {
          resources.erase(i++);
          freNum++;
          delete rs;
       }
       else
        ++i;
  }
}


MyRMResource* getMyRMResourceInstance(daq::rmgr::Config_update & idl_conf, int ires, daq::rmgr::ResourceType itype, std::string& comp, std::string& swobj, std::string& client, long granted, long handleId, unsigned int proc) {
  MyRMResource * res = 0;
  std::string resId( idl_conf.swobj_lst[ires].res_list[0] );
  res = new MyRMResource( resId, itype, client, comp, swobj,
  getMaxPP(ires), granted, handleId, proc ); //always granted==1
  return res;
}

  daq::rmgr::RM_ClientImpl* RMC = (daq::rmgr::RM_ClientImpl*)0;

int main(int argc, char** argv)
{

/*
   if( weights_val.count() ) {
     for (unsigned int iw = 0; iw < weights_val.count() ; iw++)
        if( iw < ( (unsigned int) (boundLen-1) ) )
           operations[iw].w = weights_val[iw];
     std::cerr << "\n New " << weights_val.count()<<" weights on input"<<std::endl;
     std::cerr<<" New weights now:"<<std::endl;
     for (int i01 = 0; i01 < (boundLen-1); i01++ )
        std::cerr<<operations[i01].name<<"="<< operations[i01].w<<std::endl;
   }
*/
  std::string rm_server_name("RM_Server_test");
  std::string partition_name("partition_1");
  int num(100);
  int time_v(0);
  bool no_common_res_name(false);
  std::vector<int> weights;

  int no_common_r = no_common_res_name ? 1 : 0;
  try {
    boost::program_options::options_description desc(
      "Test partition. Run until killed and request randomly different RM methods for partition"
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("partition,p",   boost::program_options::value<std::string>(&partition_name)->required(), "partition within which different methods are generated (mandatory)")
        ("num,N", boost::program_options::value<int>(&num)->default_value(num),"Number of computers.")
        ("time,t", boost::program_options::value<int>(&time_v)->default_value(time_v), "Time of running in minutes.")
        ("no_common_res_name,S", "Generate separate resources (those that did not used in other partitions). Default - common (not separate) for partitions.")
        ("weights,W", boost::program_options::value< std::vector<int> >(&weights)->multitoken(),"List of operations weight. Default operation generation weight : \n opRequestResources=500 \n opFreeResources=350  \n opFreeAllPartitionResources=2 \n opRequestResourceForProcess=65 \n opFreeProcessResources=30 \n opFreeAllHostResources=10 \n opGetResourceInfo=0 \n opFreeAllAppResources=10 \n opUpdateConfig=1 \n")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }
      if( vm.count("no_common_res_name") )
         no_common_res_name = true;
      if( vm.count("weights") )
        for (unsigned int iw = 0; iw < weights.size() ; iw++ ) {
           if( iw >= (opLast-1) )
              break;
           operations[iw].w = weights[iw];
        }

      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }

        std::cerr << "partition_name=" << partition_name << std::endl;
        std::cerr << "number of computers=" << num << std::endl;
        std::cerr << "rm_server_name="  << rm_server_name << std::endl;
        std::cerr << "Time in minutes to run (if ==0->infinite loop): "<< time_v<< std::endl;

        
   long time0;
   long timedif = time_v;
   timeval time1;
   gettimeofday( &time1, 0 );
   time0 = time1.tv_sec; // / 60;
std::cerr<<"time0="<<time0;
   time0 += (timedif * 60);
std::cerr<<" time_en in sec=" << time0;
     // Initialization
     std::string p;
     std::string df;
  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
  }

     RMC = new daq::rmgr::RM_ClientImpl(rm_server_name.c_str());
     std::cerr<< "RM_Client for RM_server=\""<< rm_server_name <<"\" was created for tests"<<std::endl; 
  if (RMC == 0){
     std::cerr<< " No server running" << std::endl;
     return EXIT_FAILURE;  // server do not found
  }

  daq::rmgr::Config_update idl_conf;
  std::string pname(partition_name);
  std::cerr<<std::endl<< "Creating conf data ..."<<std::endl;
  create_conf_data( pname, num, no_common_r, idl_conf); //, pname, rs );
  std::cerr<< "..... conf created."<<std::endl;
  std::cerr<< " !!!!! update configuration data.";
  // TODO save generated configuration data in file that used for server start (-c parameter)
  try{
     //RMC->rmConfigurationUpdate4MyTest( idl_conf );
     RMC->rmConfigurationUpdate4MyTest();
  }
  catch ( daq::rmgr::Exception & ex) {
    ers::error( ex );
  }

  std::cerr<< " Done."<<std::endl<<std::endl;

 return EXIT_SUCCESS;

//==========================
   numOfComp = num; //100;
   RmOper opind;
   long granted = 1;
   std::string tmp_client = "client1";

 long reqNum = 0;
 long iloop = 0;
 unsigned int  mypid = getpid();

 while ( true ) {
   opind = generateRmOper();
   iloop++;
 
   switch( opind ) {
     case opRequestResources: 
        {
          daq::rmgr::ResourceType itype = generateType();
          std::string comp;
          comp = generateComp( comp);
          int ires = generateResIndex( itype ); 
          std::string swobj( idl_conf.swobj_lst[ires].swid );
	  std::string application = swobj + "@" + comp; //AI 030624

          try{
              reqNum++;
	      //AI 030624
              //long handleId = RMC->requestResources( pname, swobj, comp);
	      long handleId = RMC->requestResources( pname, swobj, comp, "someOwner", application, mypid ); 
              successHandles++;
              MyRMResource * res = getMyRMResourceInstance(idl_conf, ires, itype, comp,swobj, tmp_client, granted, handleId,  0); //temp? Save proc only for allocated by process
              if(res) {
                 resources.insert( MyResMap::value_type( res->id /*resId*/, res ) );
              }
          }
          catch ( daq::rmgr::Exception & ex) {
              ers::error( ex );
          }
       }
          break;      
     case opFreeResources:
        {
          int numHandles = resources.size();
          if(numHandles < 1)
            break;
          int ind1 = rand() % numHandles;
          long hndl = getHandleByIndex( ind1);
          if( hndl > 0 ) {
             try{
                 RMC->freeResources(hndl);
                 removeResource( hndl );              
             }
             catch ( daq::rmgr::Exception & ex) {
               ers::error( ex );
             }
          }
        }
          break;
     case opFreeAllPartitionResources:
       {
         std::string comp = "";
         std::string bin = "";
          try {
            RMC->freeAllInPartition(pname); //freeAllResources( pname,"*");  
            removeResources( comp, bin );
          }
          catch (daq::rmgr::Exception & ex) {
             ers::error(ex);
          }
       }
          break;
     case opFreeAllAppResources:
       {
          daq::rmgr::ResourceType itype = daq::rmgr::HWR; //generateType();
          std::string comp;
          comp = generateComp(comp);
          int ires = generateResIndex( itype );
          std::string swobj( idl_conf.swobj_lst[ires].swid );
          try {
            RMC->freeApplicationResources( pname, swobj, comp);
            removeResources( comp, swobj );
          }
          catch (daq::rmgr::Exception & ex) {
             ers::error(ex);
          }

       }
          break;
     case opRequestResourceForProcess:
       {
          daq::rmgr::ResourceType itype = generateType();
          std::string comp;
          comp = generateComp(comp);
          int ires = generateResIndex( itype );
          std::string rid( idl_conf.swobj_lst[ires].res_list[0] );
          std::string swobj("");
          try {
             long handleId = RMC->requestResourceForMyProcess( pname, rid, comp, tmp_client);
             MyRMResource * res = getMyRMResourceInstance(idl_conf, ires, itype, comp, swobj, tmp_client, granted, handleId,  mypid );
              if(res)
                 resources.insert( MyResMap::value_type( res->id, res ) );
          }
          catch ( daq::rmgr::Exception & ex) {
             ers::error(ex);
          }
       }
          break;
     case opFreeProcessResources:
       {
    ///      daq::rmgr::ResourceType itype = generateType();
          std::string comp;
          comp = generateComp(comp);
          try {
              RMC->freeProcessResources(pname, comp, mypid);
              removeResources( mypid );
          }
          catch ( daq::rmgr::Exception & ex) {
              ers::error(ex); 
          }
       }
          break;
     case opFreeAllHostResources:
       {
     ///     daq::rmgr::ResourceType itype = daq::rmgr::HWR;
          std::string comp;
          comp = generateComp(comp);
          std::string bin = "";
          try {
            RMC->freeAllOnComputer(comp);  //freeAllResources("*","*", comp); 
            removeResources( comp, bin );
          }
          catch (daq::rmgr::Exception & ex) {
             ers::error(ex);
          }

       }
          break;
     case opUpdateConfig:
          try{
	    // TODO save generated configuration data in file that used for server start (-c parameter)
            //RMC->rmConfigurationUpdate4MyTest( idl_conf );
	    RMC->rmConfigurationUpdate4MyTest();
          }
          catch ( daq::rmgr::Exception & ex) {
              ers::error(ex);
          }
          break;
     case opGetResourceInfo:
        {
         try{
           RMC->getPartitionInfo(pname.c_str());
         }
         catch (daq::rmgr::Exception & ex) {
         }
        }
          break;
     default:
          ;
          break;
   }
   if( timedif ) {
      gettimeofday( &time1, 0 );
      if( time1.tv_sec  > time0 )
         break; 
   } 
  }

 std::cerr<<"Time to run is over. Request resources called: "<<reqNum<<" times, successful: "<<successHandles<< " times.  Free handles: "<<freNum<<std::endl;
  std::cerr<<"Number of handles in partition \""+pname+"\" is "<<resources.size()<<std::endl;
  std::cerr<<"Number of called operations:"<<std::endl;
  for(int ii=0; ii < opLast; ii++)
     std::cerr<<operations[ii].name << "=" << opN[ii]<<std::endl;

  for (auto& ir : resources)
     delete ir.second;

  resources.clear();

}
