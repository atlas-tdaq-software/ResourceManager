#!/bin/sh

######################################################################################
#
# Usage: ../../ResourceManager/test/check.sh `pwd` `pwd`/../../ResourceManager [debug]
#
######################################################################################


rm_server_pid=0
ipc_server_pid=0

DIR=`mktemp -d`
ipc_file="$DIR/ipc_root.$$.ref"

CONFIG_DIR="${DIR}/data/daq"

echo "copy test configuration files from $1/data/daq to $DIR/daq/data"
mkdir -p "${CONFIG_DIR}"
echo cp -r "$2/data/daq" "${CONFIG_DIR}/.."
cp -r "$2/data/daq" "${CONFIG_DIR}/.."

debug=0
if [ "$3" == "debug" ]
then
  echo "run in debug mode"
  debug=1
fi

Cleanup () {
  if [ $rm_server_pid -ne 0 ]
  then
    echo "kill rmgr_server process $rm_server_pid"
    kill $rm_server_pid
    sleep 0.25
  fi

  if [ $ipc_server_pid -ne 0 ]
  then
    echo "kill ipc_server process $ipc_server_pid"
    kill $ipc_server_pid
  fi

  if [ $debug -eq 0 ]
  then
    echo "rm -rf ${DIR}"
    rm -rf "${DIR}"
  else
    echo "keep directory ${DIR} for debug"
  fi
}

trap 'echo "caught signal: destroy running ipc programs ..."; Cleanup; exit 1' 1 2 15

################################################################################

startup_timeout='10'

################################################################################

# run ipc_server

TDAQ_IPC_INIT_REF="file:${ipc_file}"
export TDAQ_IPC_INIT_REF

echo "run: \"ipc_server > /dev/null 2>&1 &\""
ipc_server > /dev/null 2>&1 &
ipc_server_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  test_ipc_server
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo ' * waiting ipc_server startup ...'
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run ipc_server (timeout after $startup_timeout seconds)"
  Cleanup
  exit 1
fi

################################################################################

# run rmgr_server

TDAQ_AM_AUTHORIZATION='off'
export TDAQ_AM_AUTHORIZATION

TDAQ_RM_CONFIG="oksconfig:${CONFIG_DIR}/pp1.data.xml"

rm_server_name='RM_Server_check'
backup_file="$DIR/${rm_server_name}.backup"

echo "run: \"$1/rmgr_server -c $TDAQ_RM_CONFIG  -n ${rm_server_name} -l ${backup_file} > ${DIR}/server.log 2>&1 &"
$1/rmgr_server -c $TDAQ_RM_CONFIG -n "${rm_server_name}" -l "${backup_file}" > "${DIR}/server.log" 2>&1 &
rm_server_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  test_corba_server -c 'rmgr/ResMgr' -n "${rm_server_name}"
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo ' * waiting rmgr_server startup ...'
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run rmgr_server (timeout after $startup_timeout seconds)"
  Cleanup
  exit 1
fi

################################################################################

test_partition='par1'

################################################################################

if [ $debug -eq 0 ]
then
  echo "run: \"$1/rmgr_check_short -s $2 -w ${DIR} -p ${test_partition}"
  $1/rmgr_check_short -s $2 -w "${DIR}" -p "${test_partition}"
else
  echo "run: \"$1/rmgr_check_short -s $2 -w ${DIR} -p ${test_partition} -v"
  $1/rmgr_check_short -s $2 -w "${DIR}" -p "${test_partition}" -v
fi
status=$?

################################################################################

partition_info_dump_1="$DIR/${test_partition}_info_dump.1.txt"
echo "run: \"$1/rmgr_get_partition_info -p ${test_partition} -n ${rm_server_name} > ${partition_info_dump_1}" 
$1/rmgr_get_partition_info -p "${test_partition}" -n "${rm_server_name}" 2>&1 | sed 's/.* INFO/INFO/' > "${partition_info_dump_1}"
status=$(( $status | $? ))

################################################################################

echo "kill and restart RM server from backup..."

echo "kill -9 $rm_server_pid"
kill -9 ${rm_server_pid}

sleep 1

echo "run: \"$1/rmgr_server -c $TDAQ_RM_CONFIG -n ${rm_server_name} -l ${backup_file} -r > ${DIR}/server.2.log 2>&1 &"
$1/rmgr_server -c $TDAQ_RM_CONFIG -n "${rm_server_name}" -l "${backup_file}" -r > "${DIR}/server.2.log" 2>&1 &
rm_server_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  test_corba_server -c 'rmgr/ResMgr' -n "${rm_server_name}"
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo ' * waiting rmgr_server startup ...'
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run rmgr_server (timeout after $startup_timeout seconds)"
  Cleanup
  exit 1
fi

################################################################################

partition_info_dump_2="$DIR/${test_partition}_info_dump.2.txt"
echo "run: \"$1/rmgr_get_partition_info -p ${test_partition} -n ${rm_server_name} > ${partition_info_dump_2}"
$1/rmgr_get_partition_info -p "${test_partition}" -n "${rm_server_name}" 2>&1 | sed 's/.* INFO/INFO/' > "${partition_info_dump_2}"
status=$(( $status | $? ))

echo "run: \"diff ${partition_info_dump_2} ${partition_info_dump_1}\""
if diff ${partition_info_dump_2} ${partition_info_dump_1}
then
  echo 'TEST differences after restore from backup: PASSED'
else
  echo 'TEST differences after restore from backup: FAILED'
  status=1
fi

################################################################################

bad_names="$DIR/${test_partition}_bad_backup_info.txt"

echo "run: grep \"###bad-\" ${backup_file}*"
grep "###bad-" "${backup_file}"* > ${bad_names}

echo "run: grep 999999999 ${backup_file}*"
grep '999999999' "${backup_file}"* >> ${bad_names}

if [ -s ${bad_names} ]; then
  echo "TEST bad names in backup files: FAILED"
  echo "The bad names in backup are:"
  cat ${bad_names}
  status=1
else
  echo "TEST bad names in backup files: PASSED"
fi

################################################################################

$1/rmgr_check_stress
status=$(( $status | $? ))

Cleanup

################################################################################

if [ $status -ne 0 ]
then
  echo 'CHECK FAILED'
else
  echo 'CHECK PASSED'
fi

exit $status

################################################################################
