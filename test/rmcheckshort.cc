#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include <iostream>
#include <filesystem>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ers/ers.h>

#include <ResourceManager/RM_Client.h>
#include "src/idlhelper.h"

daq::rmgr::RM_Client* RMC = nullptr;

int okcheck = EXIT_SUCCESS;

const int pid = 55555;

static void
test_passed()
{
  std::cout << "PASSED" << std::endl;
  std::cout.flush();
}

static void
test_failed()
{
  okcheck = EXIT_FAILURE;
  std::cout << "FAILED" << std::endl;
  std::cout.flush();
}

static void
report_test_status(bool success)
{
  if (success)
    test_passed();
  else
    test_failed();
}

static const char *
result(bool expect_success)
{
  return (expect_success ? "\"SUCCESS\": " : "\"UNSUCCESS\": ");
}

static const std::string getAsStr( const  std::string headMess, const std::vector<std::string>& list ) {
   std::ostringstream txt;
   txt << headMess << " {";
   for( auto& i : list )
      txt << i << ',';
   return txt.str().substr(0,txt.str().length()-1) + "}";
}

static long
test_request_resource(const std::string& partition, const std::string& resource, const std::string& application, const std::string& computer, bool request_for_my_process, int proc_id, const std::string& client, bool expect_success, bool verbose)
{
  long retv = -1;
  bool failure_reported = false;

  try
    {
      if (request_for_my_process)
        {
           //use proc_id if its value > 0
          if (proc_id < 0)
            {
              std::cout << "TEST requestResourceForMyProcess(partition: \"" << partition << "\", resource: \"" << resource << "\", host: \"" << computer << "\", client: \""
                  << client << "\") expecting " << result(expect_success);
              retv = RMC->requestResourceForMyProcess(partition, resource, computer, client);
            }
          else
            {
              std::cout << "TEST for process requestResource(partition: \"" << partition << "\", resource: \"" << resource << "\", host: \"" << computer << "\", client: \""
                  << client << "\", process_id: " << proc_id << ") expecting " << result(expect_success);
              retv = RMC->requestResource(partition, resource, computer, client, proc_id);
            }
        }
      else
        {
          if (proc_id < 0)
            {
              std::cout << "TEST requestResources(partition: \"" << partition << "\", software object: \"" << resource << "\", host: \"" << computer << "\", client: \"" << client
                  << ", application: \"" << application << "\") and setProcess(" << -proc_id << ") expecting " << result(expect_success);

              retv = RMC->requestResources(partition, resource, computer, client, application); //AI 030624 application added
              if (verbose)
                std::cout << "Handle info after requestResources() call: " << RMC->getHandleInfo(retv) << std::endl;

              RMC->setProcess(retv, -proc_id);
              daq::rmgr::HandleInfo inf(RMC->getHandleInfo(retv));
              if (inf._pid != -proc_id)
                {
                  test_failed();
                  failure_reported = true;
                }

              if (verbose)
                std::cout << "Handle info after setProcess() call: " << RMC->getHandleInfo(retv) << std::endl;
            }
          else
            {
              std::cout << "TEST requestResources(partition: \"" << partition << "\", software object: \"" << resource << "\", host: \"" << computer << "\", client: \"" << client
                  << ", application: \"" << application << "\") expecting " << result(expect_success);

              retv = RMC->requestResources(partition, resource, computer, client, application, proc_id);
            }
        }

      if (!failure_reported)
        report_test_status(expect_success == (retv > 0));

      if (retv > 0 && (verbose || !expect_success))
        std::cout << "granted handle " << retv << ": " << RMC->getHandleInfo(retv) << std::endl;
    }
  catch ( daq::rmgr::UnavailableResources& ex )
    {
      if (expect_success)
        {
          test_failed();
          ers::error(ex);
        }
      else
        {
          test_passed();
          if (verbose)
            std::cout << "exception details: " << ex << std::endl;
        }
    }
  catch ( daq::rmgr::ResourceNotFound& ex )
    {
      if (expect_success)
        {
          test_failed();
          ers::error(ex);
        }
      else
        {
          test_passed();
          if (verbose)
            std::cout << "exception details: " << ex << std::endl;
        }
    }
  catch ( daq::rmgr::SoftwareNotFound & ex )
    {
      if (expect_success)
        {
          test_failed();
          ers::error(ex);
        }
      else
        {
          test_passed();
          if (verbose)
            std::cout << "exception details: " << ex << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }

  std::cout.flush();

  return retv;
}

//AI 030624
static long
test_request_resource(const std::string& partition, const std::string& resource, const std::string& computer, bool request_for_my_process, int proc_id, const std::string& client, bool expect_success, bool verbose)
{
   std::string appl = "appl_" + resource;
   return test_request_resource( partition, resource, appl, computer, request_for_my_process,  proc_id, client, expect_success, verbose);
}

static void
test_get_resources_info(const std::string& partition, const std::string& resource, bool expected, bool verbose)
{
  try
    {
      std::cout << "TEST get resources info in frame of partition \"" << partition
                << "\" for \"" << resource << "\" expecting " << (expected ? "non-" : "") << "empty result: ";

      daq::rmgr::AllocatedInfo res(RMC->getResourceInfo(partition, resource));
      report_test_status(expected == (res.list.size() > 0));

      if (verbose)
        std::cout << res << std::endl;
    }
  catch (const daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }

  std::cout.flush();
}

static void
test_get_full_resource_info(const std::string& resource, unsigned int expected, bool verbose)
{
  try
    {
      std::cout << "TEST get full resource info for \"" << resource << "\" expecting " << expected << " AllocatedResource elements: ";

     daq::rmgr::AllocatedInfo res(RMC->getFullResInfo(resource));
      if (res.list.size() != expected)
        {
          test_failed();
          std::cout << "allocated: " << res << std::endl;
        }
      else
        {
          test_passed();

          if (verbose)
            std::cout << res << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

static void
test_get_computer_resource_info(const std::string& computer, const std::string& partition, const std::string& resource, unsigned int expected, bool verbose)
{
  try
    {
      std::cout << "TEST get info for resource \"" << resource << "\" allocated on computer \""
                << computer << "\" in partition \"" << partition 
                << "\". Expecting " << expected << " handles for which resource was allocated: ";

      daq::rmgr::AllocatedInfo res(RMC->getComputerResourceInfo(computer, partition, resource));
      if (res.list.size() != expected)
        {
          test_failed();
          std::cout << "allocated: " << res << std::endl;
        }
      else
        {
          test_passed();

          if (verbose)
            std::cout << res << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

static void
test_get_computer_info(const std::string& computer, unsigned int expected, bool verbose)
{
  try
    {
      std::cout << "TEST get computer info for computer \"" << computer << "\" expecting " << expected << " AllocatedResource elements with resources allocated on computer: ";

      daq::rmgr::AllocatedInfo res(RMC->getComputerInfo(computer));
      if (res.list.size() != expected)
        {
          test_failed();
          std::cout << "allocated: " << res << std::endl;
        }
      else
        {
          test_passed();

          if (verbose)
            std::cout << res << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

static void
test_partition_info(const std::string& partition, unsigned int expected, bool verbose)
{
  std::cout << "TEST get info for partition \"" << partition << "\" expecting " << expected << " resources allocated: ";

  try
    {
      daq::rmgr::AllocatedInfo data(RMC->getPartitionInfo(partition));
      
      if (data.list.size() != expected)
        {
          test_failed();
          std::cout << "allocated: " << data << std::endl;
        }
      else
        {
          test_passed();

          if (verbose)
            std::cout << data << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

//AI 030624
static bool isUseAM(bool verbose) {
   bool use_am_auth;
   if (char * envAM = getenv( "TDAQ_AM_AUTHORIZATION" ))
     use_am_auth = (strcmp(envAM, "on") == 0);
   else
     use_am_auth = 1;
   if(verbose) {
     std::string enb = use_am_auth ? "enabled" : "disabled";
     std::cout << "Access Manager is " << enb << std::endl;
   }
   return use_am_auth;
}
static void
test_partition_info_priviliges(const std::string& partition, bool hasPriviliges, bool onlyMyResources, bool expect_success, bool verbose)
{
   bool use_am_auth = isUseAM(verbose);
   std::string useAMstr = use_am_auth ? "ON" : "OFF";
   std::string onlyMyResourcesStr = onlyMyResources ? "TRUE" : "FALSE";
   std::string privStr= hasPriviliges ? " no " : " ";

  try
    {
           std::cout << "TEST priviliges for get info for partition \"" << partition 
		     << "\"  with Access Manager authorization " << (use_am_auth ?  "ON": "OFF")
		     << " with only my resources in partition "<<onlyMyResourcesStr
		     <<".   with user has" << (onlyMyResources ? " no " : " ") << " priviliges."<<std::endl
                     << "Expecting " << result(expect_success)<< std::endl;
      daq::rmgr::AllocatedInfo data(RMC->getPartitionInfo(partition));

      if( !expect_success ) //if (data.list.size() != expected)
        {
          test_failed();
          std::cout << "allocated: " << data << std::endl;
        }
      else
        {
          test_passed();

          if (verbose)
            std::cout << data << std::endl;
        }
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      if( hasPriviliges || onlyMyResources || (use_am_auth && hasPriviliges) ) {
        test_failed();
        ers::error(exproc);
      }
      else
        {
          if (verbose) {
            std::cout << "No priviliges and  more than 1 owner of resources in partition. " << std::endl;
            ers::error(exproc);
          }

          test_passed();
        }
    }

  std::cout.flush();
}


static void
test_partition_list(unsigned int expected)
{
  std::cout << "TEST get partition list expecting " << expected << " partitions: ";

  try
    {
      std::vector<std::string> list(RMC->getPartitionsList());
      if (list.size() != expected)
        {
          test_failed();
          std::cout << getAsStr("method returned unexpected data: ", list) << std::endl;
        }
      else
        {
          test_passed();
        }
    }
  catch (const daq::rmgr::Exception& ex)
    {
      test_failed();
      ers::error(ex);
    }
}

static void
test_handle_info(long hndl, unsigned int expected, bool verbose) {
  try
    {
      std::cout << "TEST get info by handle  " << hndl << " with expcted " << expected 
                << " number of allocated resources: ";
      daq::rmgr::HandleInfo inf(RMC->getHandleInfo(hndl));
      if(inf._resDetails.size() != expected)
        {
          test_failed();
          std::cout << " method returned unexpected data: " << inf << std::endl;
        }
      else
        test_passed();

      test_passed();
      if (verbose)
        std::cout << "info: " << inf << std::endl;
    }
  catch(const daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }
}

static void
test_get_resource_owners( const std::string& partition, const std::string& resource, unsigned int expected, bool verbose ) {
  try
    {
      std::cout << "TEST get resource owners for \"" << resource << "\" expecting " << expected <<" owners: ";
      std::vector<std::string> list( RMC->getResourceOwners(resource, partition) );
      report_test_status(expected == list.size());

      if (verbose)
        std::cout << getAsStr("Owners list: ",list) << std::endl;
    }
  catch (const daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }

  std::cout.flush();
}


static void
test_free_partition(const std::string& partition)
{
  try
    {
      std::cout << "TEST free all resources for partition \"" << partition << "\": ";
      RMC->freeAllInPartition(partition);
      test_passed();
    }
  catch ( daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }
}

static void
test_free_computer(const std::string& computer)
{
  try
    {
      std::cout << "TEST free all resources on computer \"" << computer << "\": ";
      RMC->freeAllOnComputer(computer);
      test_passed();
    }
  catch ( daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }
}

static void
test_config_update(const std::string& partition, bool expect_success, bool verbose)
{
  std::cout << "TEST configuration update using partition \"" << partition << "\": ";

  try
    {
      RMC->rmConfigurationUpdate( partition );
      report_test_status(expect_success == true);

      if (verbose)
        {
          daq::rmgr::ConfigUpdate  helper0(RMC->getConfigInfo());
          std::cout << helper0 <<std::endl;
        }
    }
  catch ( daq::rmgr::Exception & ex)
    {
      if (expect_success)
        {
          test_failed();
          ers::error(ex);
        }
      else
        {
          test_passed();
          if (verbose)
            std::cout << "exception details: " << ex << std::endl;
        }
    }
}

static void
test_free_resource_by_handle(unsigned long handle)
{
  std::cout << "TEST free by handle " << handle << ": ";

  try
    {
      RMC->freeResources( handle );
      test_passed();
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}


static void
test_free_process_resource(const std::string partition, const std::string computer, unsigned long mypid, bool expect_success, bool verbose)
{
  std::cout << "TEST free resource in partition \"" << partition << "\" for computer \"" << computer << "\" for process id " << mypid << " expecting " << result(expect_success);

  try
    {
      RMC->freeProcessResources(partition, computer, mypid);
      report_test_status(expect_success);
    }
  catch (daq::rmgr::Exception &ex)
    {
      if (expect_success)
        {
          test_failed();
          ers::error(ex);
        }
      else
        {
          test_passed();
          if (verbose)
            std::cout << "exception details: " << ex << std::endl;
        }
    }
}

static void
test_free_resource_by_resource_id(const std::string& resource)
{
  std::cout << "TEST free resource by id \"" << resource << "\": ";

  try
    {
      RMC->freeResource( resource );
      test_passed();
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

static void
test_free_partition_resource(const std::string& partition, const std::string& resource)
{
  std::cout << "TEST free resource \"" << resource << "\" in partition \"" << partition << "\": ";

  try
    {
      RMC->freePartitionResource(partition, resource);
      test_passed();
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}

static void
test_free_partition_resource_on_computer(const std::string& partition, const std::string& resource, const std::string& computer)
{
  std::cout << "TEST free resource \"" << resource << "\" in partition \"" << partition << "\" on \"" << computer << "\": ";

  try
    {
      RMC->freeComputerResource(resource, computer, partition);
      test_passed();
    }
  catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }

  std::cout.flush();
}


static void
copy_oks_data(const std::filesystem::path& src_dir, const std::filesystem::path& dst_dir, const std::filesystem::path& src_file, const std::filesystem::path& dst_file)
{
  std::filesystem::path src(src_dir / src_file);
  std::filesystem::path dst(dst_dir / dst_file);

  try
    {
      std::cout << "copy " << src << " to " << dst << std::endl;
      std::filesystem::copy(src, dst, std::filesystem::copy_options::overwrite_existing | std::filesystem::copy_options::recursive);
    }
  catch (const std::filesystem::filesystem_error& ex)
    {
      okcheck = EXIT_FAILURE;
      std::cout << "ERROR: failed to copy " << src << " to " << dst << ": " << ex.code().message() << std::endl;
    }

  std::cout.flush();
}

//series of request of available and unavailbale resources in partition and total scope
// use partition[0] and (if exists) partition[1] for tests
// use computer[0] only for sowtware resouces and computer[0].computer[1] for hardware resources
// use proc_id <0 for all cases except checking request resources for given proc_id
static void
test_request_series_resources( const std::string partition[2], const std::string& swobj,  const std::string& application,  const std::string computer[], int size, bool request_for_my_process, int proc_id, const std::string& client, int perP, int perTot, bool verbose )
{
   std::cout << "TEST series of request resources for partitions \""<< partition[0]
             << "\" and \""<< partition[1]<<"\"."<<std::endl;
   int num = -1;
   //request resource until get false for per partition value 
   bool result = ++num < perP;
   if( verbose )
     std::cout<<"    Wait "<<perP<<" success for partition \""<<partition[0]<<"\" on computer \""
              <<computer[0]<<"\" for \""<<swobj<<"\" and application \'"<<application<<"\'"<<std::endl;
   while( result ) {
     test_request_resource(partition[0], swobj, application, computer[0], request_for_my_process, proc_id, client, result, verbose);
     result = ++num < perP;
   }
   //try once more for 1 fail in partition scope
   test_request_resource(partition[0], swobj, application, computer[0], request_for_my_process, proc_id, client, result, verbose);
   //check for another partition for "total" in use
   if( partition[1].length() > 0 ) {
      result = num < perTot;
      if( verbose ) {
        int numWait = perTot-perP;
        std::cout<<"    Wait "<<numWait<<" times success for partition \""<<partition[1]
                 <<"\" on computer \""<<computer[0]<<"\""<<std::endl;
      }
      while( result ) {
        test_request_resource(partition[1], swobj, application, computer[0], request_for_my_process, proc_id, client, result, verbose);
        result = ++num < perTot;
      }
      //try once more for 1 fail in total scope
      test_request_resource(partition[1], swobj, application, computer[0], request_for_my_process, proc_id, client, result, verbose);

      test_free_partition( partition[1]);
   }
   //check for another computer, here if hardware in swobject (only for this case size>1
   if( size > 1 ) {
     if( verbose)
        std::cout<<"    Wait "<<perP<<"success for partition \""<<partition[0]
                 <<"\" on computer \""<<computer[1]<<"\""<<std::endl;
     num = -1;
     result = ++num < perP;
     while( result ) {
        test_request_resource(partition[0], swobj, application, computer[1], request_for_my_process, proc_id, client, result, verbose);
        result = ++num < perP;
     }
     //try once more for 1 fail in partition scope
     test_request_resource(partition[0], swobj, application, computer[1], request_for_my_process, proc_id, client, result, verbose);
   }
   
   test_free_partition( partition[0] );
   if( partition[1].length() > 0 )
     test_free_partition( partition[1] );
}

int main(int argc, char **argv)
{
  try
    {
      IPCCore::init(argc,argv);
    }
  catch( daq::ipc::Exception & ex ) {
      ers::fatal( ex );
    }

  std::filesystem::path source_dir(".."), working_dir("/tmp");
  std::string pname("par1");
  std::string pname2("par2");
  std::string pname3("par3");
  bool verbose = false;

  try
    {
      boost::program_options::options_description desc(
        "Short RM check"
        "\n"
        "Available options are:");

     desc.add_options()
        ("source,s", boost::program_options::value<std::filesystem::path>(&source_dir), "path to the Resource Manager source directory")
        ("working,w", boost::program_options::value<std::filesystem::path>(&working_dir), "path to working area to create temporary files")
        ("partition,p",   boost::program_options::value<std::string>(&pname)->default_value(pname), "partition name")
        ("verbose,v", "Report details of tests")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      if (vm.count("verbose"))
        verbose = true;

      boost::program_options::notify(vm);
    }
  catch (const std::exception& ex)
    {
      std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
      return EXIT_FAILURE;
    }

  std::filesystem::path data_working_dir = working_dir / "data/daq";

  try
    {
      if (std::filesystem::exists(data_working_dir) == false)
        {
          std::cout << "create " << data_working_dir << std::endl;
          std::filesystem::create_directories(data_working_dir);
        }
    }
  catch (const std::filesystem::filesystem_error& ex)
    {
      std::cerr << "Failed to create " << data_working_dir << ": " << ex.code().message() << std::endl;
      return EXIT_FAILURE;
    }


  RMC = new daq::rmgr::RM_Client("RM_Server_check");
  //check success and fails for request resources for SWR,HWR, process respurces for SWR and HWR
  std::string parray[] = {"p1","p2"};
  std::string compArray1[] = {"fca29"};
  std::string compArray2[] = {"fca29", "fca30"};


  test_request_series_resources( parray, "bin2023_swr1", "appl_bin2023_swr1", compArray1, 1, false, -pid, "Client1", 1, 2, verbose );
  test_request_series_resources( parray, "bin2023_hwr1", "appl_bin2023_hwr1", compArray2, 2, false, -pid, "Client1", 1, 2, verbose );
  test_request_series_resources( parray, "swr2023_2_1", "appl_swr2023_2_1", compArray1, 1, true, -pid, "Client1", 1, 2, verbose );
  test_request_series_resources( parray, "hwr2023_2_1", "appl_hwr2023_2_1", compArray2, 2, true, -pid, "Client1", 1, 2, verbose );

  //request resources for binary with >1 resources, check success and unsuccess cases
  //for success check free all resources by one handle
  long hndl = test_request_resource("p0", "Binary1_hw2_fca33", "appl_Binary1_hw2_fca33","fca33", false, -pid, "client1", true, verbose);
  test_request_resource("p1", "Binary1_sw2_fca30", "fca33", false, -pid, "client1", true, verbose);
  test_request_resource("p2", "bin2023_hwr1", "fca33", false, -pid, "client1", true, verbose);
  test_request_resource("p1", "bin2023_hw2_sw2_hw2023", "fca30", false, -pid,  "client1", true, verbose);
  test_request_resource("p1", "bin2023_hw2_sw2_hw2023", "fca30", false, -pid, "client2", false, verbose);
  if( hndl > 0 )
    test_free_resource_by_handle(hndl);
  hndl = test_request_resource("p2", "bin2023_hw2_sw2_hw2023", "fca30", false, -pid, "client2", true, verbose);
  test_handle_info( hndl, 3, verbose );
  // 
  test_request_resource("p1", "bin2023_hw2_sw2_hw2023", "fca30", false, -pid,  "client3", false, verbose);
  test_partition_list(2);
  //test different get info methods
  test_get_full_resource_info("sw_res2", 3, verbose);
  test_get_full_resource_info("hwr2023_2_1", 3, verbose);
  test_get_computer_resource_info("fca30", "p1", "hwr2023_2_1", 1, verbose);
  test_get_computer_info("fca30", 6, verbose); 

  //free all resources from previouse tests
  test_free_partition("p1");
  test_free_partition("p2");

    //AI 030624
  //test partition info without priviliges.
  //only one resources owner in partition
  test_request_resource("check_privilige", "Binary1_sw2_fca30", "fca33", false, -pid, "NO", true, verbose);
  test_request_resource("check_privilige", "bin2023_hw2_sw2_hw2023", "fca30", false, -pid,  "NO", true, verbose);
  test_partition_info_priviliges( "check_privilige", false, true, true, verbose); // list = 4 lines
  //more than 1 owner of resources in partition
  test_request_resource("check_privilige", "Binary1_sw1_fca29", "fca29", false, -pid, "client2222", true, verbose);
  if(  isUseAM(false) )
     test_partition_info_priviliges( "check_privilige", false, false, false, verbose); //should be exception
  else
     test_partition_info_priviliges( "check_privilige", false, true, true, verbose); // list = 5 lines
  test_free_partition("check_privilige");

  //restore original (before changes in db) config data
  copy_oks_data(source_dir, working_dir, "data/daq", "data/daq");
  copy_oks_data(source_dir, working_dir, "data/daq/orig", "data/daq"); 

  test_config_update("p1", true, verbose);

  long hndlId;

  // request resource for my process and free it. Wait zero lines for both partition info actions
  if (long hndlidProc = test_request_resource(pname, "hw_res1", "fca31", true, -pid, "mio", true, verbose))
   test_free_resource_by_handle(hndlidProc);
  test_partition_info(pname, 0, verbose);
  test_partition_list(0);

  //new method test
  // request resource for process with id 3000, get partition info and free those resources
  hndlId = test_request_resource(pname, "hw_res1", "fca31", true, 3000, "mio", true, verbose);
  test_partition_info(pname, 1, verbose);
  if( hndlId > 0)
    test_free_resource_by_handle( hndlId );
  test_partition_info(pname, 0, verbose);
  test_partition_list(0);

  std::string comp[] = {"fca29","fca30","fca33","fca31"}; //IDs

  //request available HW,SW and !existing resource
  std::cout << "request 3 resources for the same process on the same computer and then free them by single process free request\n";
  test_request_resource(pname, "hw_res1", comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, "sw_res2", comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, "sw_res3", comp[3], true, -pid, "mio", false, verbose);//no sw_res3

  std::string resources[] = {"computer_res1", "hw_res1", "sw_res1"}; //have also hw_res0 , hw_res2

  //get resource info about 2 existing and 1 unknown resource
  test_get_resources_info(pname, resources[1], true, verbose);
  test_get_resources_info(pname, "sw_res2", true, verbose);
  test_get_resources_info(pname, "sw_res3", false, verbose);

  //get partition info, excpect 2 resources allocated in partition
  test_partition_info(pname, 2, verbose);

  unsigned long mypid1 = getpid();
  std::cout << "TEST free resources by process id " << mypid1 << " call: ";

  try
    {
      RMC->freeProcessResources( pname, comp[3], mypid1 );
      test_passed();
    }
 catch ( daq::rmgr::Exception & exproc)
    {
      test_failed();
      ers::error(exproc);
    }
  //all resource were released -> all get resource actions should be failed
  test_get_resources_info(pname, resources[1], false, verbose);
  test_get_resources_info(pname, "sw_res2", false, verbose);
  test_get_resources_info(pname, "sw_res3", false, verbose);
  //no allocated resources -> wait 0 lines in output
  test_partition_info(pname, 0, verbose);

  //try to ask resources for unknown binary
  test_request_resource(pname, "kukuruza_bin_id", "fca29", false, -pid,  "any", false, verbose);

  std::string swobjs[] = { "Binary1_sw1_fca29", "Binary1_sw2_fca30", "Binary3_hw1_fca29", "Binary2_hw1_fca33" };
  //computers names for appl[] in the same order. Now ID instead of names
  std::string comps[] = { "fca29", "fca30", "fca29", "fca33" };
  int applsize = 4;
  std::string client0 = "client";
  std::string client;
  std::string ival;

  // Try to allocate 5 times for different clients, resources,swobj and computer
  for (int i = 0; i < applsize; i++)
    {
      ival = std::to_string( i );
      client = client0 + ival;
      test_request_resource(pname, swobjs[i], comps[i], false, -pid, client, true, verbose);
    }

  //test get clients of granted resources
  test_get_resource_owners(pname, "sw_res1", 1, verbose);
  test_get_resource_owners(pname, "sw_res2", 1, verbose);
  test_get_resource_owners(pname, "hw_res1", 2, verbose);

  /* do not need test template application ??   */
  std::string swobjsTempl[] = { "Binary1_sw2_fca30", //"Binary2_comp1_fca30", //SW object IDs for applTempl[]
        "Binary3_hw1_fca29", "Binary4_hw2_fca30", "Binary2_hw1_fca33"};
  std::string compsTempl[] = { "fca30", "fca29", "fca30", "fca33" }; //IDs
  //allocate all in arrayt applTeml[] (appl[0] already done).

  for (int jt=0; jt<applsize; jt++)
    test_request_resource(pname, swobjsTempl[jt], compsTempl[jt], false, -pid, "NO", jt == 0 || jt == 2, verbose);

  test_partition_info(pname, 6, verbose);

  //allocate all in arrayt appl[] (appl[0] already done).
  for (int j=1; j<applsize; j++)
    test_request_resource(pname, swobjs[j], comps[j], false, -pid, "NO", false, verbose);

  test_partition_info(pname, 6, verbose);

//test request resource for process
  test_request_resource(pname, resources[1], comp[0], true, -pid, "mio", false, verbose);
  test_request_resource(pname, resources[1], comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, "hw_res0", comp[3], true, -pid, "mio2", true, verbose);
  test_request_resource(pname, "hw_res0", comp[1], true, -pid,  "mio2", true, verbose);

/* NO SUCH ACTION
//test saveBackup
 std::cerr << "Backup start after resource4process ..." << std::endl;
  try{
    RMC->saveBackup();
    std::cerr << "Backup finished for resource4process ..." << std::endl;
  }
  catch ( daq::rmgr::Exception & exproc) {
          ers::error(exproc);
  }

 
 std::cerr << "**************************************************************" <<std::endl;
*/
 
  //+============================================================
// Get token info for all that was allocated in "par1"
//
  test_partition_info(pname, 9, verbose);

/*  std::cerr << "Partition info for all resources in partition "<< pname<<":" << std::endl;
 try {
  std::cerr << *RMC->getPartitionInfo(pname);
 }
 catch ( daq::rmgr::Exception & exproc) {
      ers::error(exproc);
 }*/

  //+============================================================
 // update configuration data
  std::cout << "updating configuration info ..." << std::endl;
  copy_oks_data(source_dir, working_dir, "data/daq/orig/binary_1applchanged.data.xml", "data/daq/binary.data.xml");                                                                           
  copy_oks_data(source_dir, working_dir, "data/daq/orig/res_compres1changed.data.xml", "data/daq/res.data.xml");

  test_config_update(pname, true, verbose);

  test_partition_info(pname, 9, verbose);

  //+============================================================

// Now let us free  resources (using 2 handles)
  for(int ii=1; ii<2; ii++) {
      test_free_resource_by_handle(ii);
/*     long hndlId = ii;
     try {
          RMC->freeResources(hndlId);
          std::cerr  << "freeResources operation for handleId=" << hndlId<<std::endl;
     }
     catch ( daq::rmgr::Exception & ex) {
         ers::error(ex);
     }*/
  }
// std::cerr << "**************************************************************" <<std::endl;
//free process resources test
//were: resources[1], comp[0]: unsucces in request
//      resources[1], comp[3]: success
//      "hw_res0", comp[1]: success

  unsigned long mypid = getpid();
  test_free_process_resource( pname, comp[0], mypid, true, verbose ); 
  test_free_process_resource( pname, comp[3], mypid, true, verbose );
  test_free_process_resource( pname, comp[1], mypid, true, verbose );
//AI 030624
  test_free_process_resource( pname, comp[0], pid, true, verbose );
  test_free_process_resource( pname, comp[3], pid, true, verbose );
  test_free_process_resource( pname, comp[1], pid, true, verbose );

  test_partition_info(pname, 1, verbose);

  std::cout << "again allocate process resources before ALL free operation..." << std::endl;

  test_request_resource(pname, resources[1], comp[3], true,-pid,  "mio", true, verbose);
  test_request_resource(pname, "hw_res0", comp[3], true, -pid,  "mio2", true, verbose);

  test_partition_info(pname, 3, verbose);

  test_free_partition(pname);
  test_partition_info(pname, 0, verbose);

  //+============================================================
// test request resourses

  int binSize=3, compSize=3;
  std::string bin[] = {"Binary1_comp1_fca29", "Binary1_sw2_fca30","Binary2_hw1_fca33"};
  unsigned int count = 0;
  for(int j=0;j<binSize;j++)
    for(int jj=0;jj<compSize;jj++)
      {
        int kk = j==2 ? 2 : 1;
        for(int ll=0; ll<kk; ll++) {
		//some debug after update config in use
//          test_request_resource(pname, bin[j], comp[jj], false, -pid, "NO", (j == 1 && jj == 0) || (j == 2 && ++count % 2), verbose);
	  ++count;
	  bool succs = (j == 1 && jj == 0) || (j == 2 && count % 2);
          test_request_resource(pname, bin[j], comp[jj], false, -pid, "NO", succs, verbose);
	}
      }
  try
    {
      hndlId = 29;
      std::cout << "TEST access info by handle " << hndlId << ": ";
      daq::rmgr::HandleInfo inf(RMC->getHandleInfo(hndlId));
      test_passed();
      if (verbose)
        std::cout << "info: " << inf << std::endl;
    }
  catch(const daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }

   //unload all partitions
  std::cout << "unload partition resources (remove partition from RM_Server)..."  <<  std::endl;
  test_free_partition(pname);

  std::cout << "test request by sw+comp, by process and free by handle and process in one request, allocate twice  by process..."<<std::endl;

  test_request_resource(pname, resources[1], comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, resources[1], comp[3], true, -pid, "mio", false, verbose);

  //check hardware resources
  copy_oks_data(source_dir, working_dir, "data/daq/orig/binary.data.xml", "data/daq/binary.data.xml");
  copy_oks_data(source_dir, working_dir, "data/daq/orig/res.data.xml", "data/daq/res.data.xml");

  test_config_update("p2", true, verbose);

  test_request_resource(pname, "rc_empty_controller", comp[3], false, -pid, "mio", true, verbose);
  hndlId = test_request_resource(pname, "rc_empty_controller", comp[3], false, -pid, "mio", true, verbose);
  
  try
    {
      std::cout << "TEST free by handle " << hndlId << " for pid " << pid << " and " << mypid << ": ";
      //AI 030624
      //RMC->freeAllProcessResources( hndlId, pname, comp[3], mypid );
      RMC->freeAllProcessResources( hndlId, comp[3], pid );
      RMC->freeAllProcessResources( hndlId, comp[3], mypid );
      test_passed();
    }
  catch( daq::rmgr::Exception & ex)
    {
      test_failed();
      ers::error(ex);
    }

  test_partition_info(pname, 0, verbose);

  test_request_resource(pname, "Binary1_hw11_robin", comp[2], false, -pid, "mio", true, verbose);
  test_request_resource(pname, "Binary1_hw11_comp_robin", comp[2], false, -pid, "mio", false, verbose); // the binary has: {hw_res1} and {hw_res_robin} <= allocated above

  test_request_resource(pname, "Binary1_hw11_comp_robin", comp[3], false, -pid, "mio", true, verbose);
  test_request_resource(pname, "hw_res_robin", comp[3], false, -pid, "mio", false, verbose);

  // test max per partition = 4
  test_request_resource(pname, "hw_res_4_8",  comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, "hw_res_4_8",  comp[3], true, mypid, "mio", true, verbose);
  test_request_resource(pname, "hw_res_4_8",  comp[3], true, -pid, "mio", true, verbose);
  test_request_resource(pname, "hw_res_4_8",  comp[3], true, mypid, "mio", true, verbose);
  test_request_resource(pname, "hw_res_4_8",  comp[3], true, -pid, "mio", false, verbose);

  // test max total = 8
  test_request_resource(pname2, "Binary1_hw48_fca33",  comp[3], false, mypid, "mio", true, verbose);
  test_request_resource(pname2, "Binary1_hw48_fca33",  comp[3], false, -pid, "mio", true, verbose);
  test_request_resource(pname2, "Binary1_hw48_fca33",  comp[3], false, mypid, "mio", true, verbose);
  test_request_resource(pname2, "Binary1_hw48_fca33",  comp[3], false, -pid, "mio", true, verbose);

  test_request_resource(pname3, "Binary1_hw48_fca33",  comp[3], false, -pid, "mio", false, verbose); // max 8 in total

  test_request_resource(pname, resources[1], comp[3], true, mypid, "mio", false, verbose); // "hw_res1" allocated by "Binary1_hw11_comp_robin"
  test_request_resource(pname, "hw_res0", comp[3], true, -pid, "mio2", true, verbose);
  test_request_resource(pname, "hw_res0", comp[1], true, mypid, "mio2", true, verbose);

  // test bad names
  test_request_resource(pname2, "###bad-request-sw-object-name",  comp[3], false, -pid, "mio", false, true);
  test_request_resource(pname2, "###bad-request-resource-name",  comp[3], true, mypid, "mio", false, true);

  // test bad free on computer by PID
  test_free_process_resource( pname, "###bad-host-name", pid, true, verbose );
  test_free_process_resource( pname, comp[1], 999999999, true, verbose );

  // test bad free by resource, partition and computer
  test_free_resource_by_resource_id("###bad-free-sw-object-name");
  test_free_partition_resource("###bad-free-partition-resource-name", "hw_res0");
  test_free_partition_resource(pname, "###bad-free-partition-resource-name");
  test_free_partition("###bad-free-partition-name");
  test_free_computer("###bad-free-computer-name");
  test_free_partition_resource_on_computer(pname, "hw_res0", "###bad-free-computer-partition-resource-name");
  test_free_partition_resource_on_computer(pname, "###bad-free-resource-computer-partition-name", comp[3]);
  test_free_partition_resource_on_computer("bad-free-partition-computer-resource-name", "hw_res0", comp[3]);

  // test bad configurations
  copy_oks_data(source_dir, working_dir, "test/check.sh", "data/daq/res.data.xml");
  test_config_update("partition3", false, verbose);
  copy_oks_data(source_dir, working_dir, "data/daq/orig/res.data.xml", "data/daq/res.data.xml");

  delete RMC;

  if (verbose)
    {
      std::cout << " !!!! okcheck=" << okcheck;
      if (okcheck == EXIT_SUCCESS)
        std::cout << std::endl << std::endl << "    ALL TESTS PASSED" << std::endl;
      else
        std::cout << std::endl << std::endl << " SOME OF TESTS FAILED" << std::endl;
    }

  return okcheck;
}

