# Resource manager (RM)
## tdaq-11-02-00
### Important changes
The configuration is loaded when RM server is started in accordance with TDAQCCRM-16.

The RM was updated by results of the review TDAQCCRM-20:
- Check correspondence of existing design and implementation with known client needs (PMG, IGUI, other systems).
- Identify and remove any branches of code not used anymore.
- Follow TDAQ recommendations to use ERS exceptions in c++, java and IDL
- Identify needs to add/fix doxygen and javadoc comments if missing, wrong , incomplete or unclear.

All exception mentioned below are in `daq::rmgr` namespace.
### IDL updates
#### Removed exceptions:
- PartitionNotFoundE
- ObjectNotFoundE
- HandleIdNotFoundE
- ProcessNotFoundE
- AcceessDeniedE (only AcceessAnyDeniedE in use for check if access avaiable)
#### Added exception:
- SoftwareNotFoundE
#### Changes in interface methods
##### rmConfigUpdate - parameters chanaged
In previuose version client could update configuration on server using local dbname. Now in accordings with TDAQCCRM-16 the configuration is loaded now only on server side. This method only initiates server configuration reload from the same database as it was used on start time. 
New signature:
**void rmConfigUpdate ( in string partition, in string user ) raises( ConfigExceptionE);**
Parameters reflect which user and from which partition the reload of configuration was initiated. Exception is raised if configuration update failed on server side.
##### requestResources - exceptions changed
Removed: 
- exception ObjectNotFoundE
Added:
- SoftwareNotFoundE  Raised if failed to find software object with given in the RM
##### requestResourcesForProcess - exceptions changed
Removed: 
- exception ObjectNotFoundE
Added:
- ResourceNotFoundE Raised if failed to find resource with given in the RM
##### requestResourcesForProcess -  removed in accordance with ATDAQCCRM-4
##### freeProcessResources  - exceptions changed
Removed exceptions: 
- PartitionNotFoundE
- ProcessNotFoundE
- ObjectNotFoundE
##### freeAllProcessResources  - exceptions changed
Removed exceptions:
- HandleIdNotFoundE
- PartitionNotFoundE
- ProcessNotFoundE
- ObjectNotFoundE
##### freeResources - exceptions changed
Removed: 
- exception HandleIdNotFoundE
##### freePartition is depricated and will be removed in next release. Use freeAllInPartition instead
##### freeAllInPartition 
The behavior of this method is the same as freePartition, only the name has been changed to be more clear to users.
##### freeComputerAll is depricated and will be removed in next release. Use freeAllOnComputer instead
##### freeAllOnComputer
The behavior of this method is the same as freeComputerAll, only the name has been changed to be more clear to users.
##### freePartitionResource - exceptions changed
Removed: 
- exception AcceessDeniedE
##### freeResource - exceptions changed
Removed: 
- exception AcceessDeniedE
##### getResourceInfo - exceptions changed
Removed exceptions:
- PartitionNotFoundE
- ResourceNotFoundE
##### getFullResInfo - exceptions changed
Removed: 
- exception ResourceNotFoundE
##### getHandleInfo - exceptions changed
Removed: 
- exception HandleIdNotFoundE
##### getResourceOwners - exceptions changed
Removed exceptions: 
- PartitionNotFoundE
- ResourceNotFoundE

### RM_Client API updates
Most external dependences moved from RM_Client class to new working RM_ClientImpl class that is out of client library. The Doxygen methods description are refreshed.
#### Exceptions policy
A lot of unthrown exceptions removed from all methods. Each method during connection with RM server could throw one of two exceptions:
- LookupFailed
- IPCException
Every method could throw 
- CommunicationException
##### Additional exceptions can be raised in follows methods
###### requestResources with signature `long requestResources(const std::string& partition, const std::string& swobjectid, const std::string& computerName, const std::string& clientName)`
- SoftwareNotFound(
- CommunicationException
- UnavailableResources
###### requestResources with signature `long requestResources(const std::string& partition, const std::string& swobjectid, const std::string& computerName, unsigned long mypid, const std::string& clientName)`
- SoftwareNotFound
- CommunicationException
- UnavailableResources
###### requestResource with signature `requestResource( const std::string& partition, const std::string& resource, const std::string& computer, const std::string& clientName, int process_id )`
- ResourceNotFound
- CommunicationException
- UnavailableResources
###### requestResourceForMyProcess with signature `requestResourceForMyProcess(const std::string& partition, const std::string& resource, const std::string& computerName, const std::string& clientName)`
- ResourceNotFound
- CommunicationException
- UnavailableResources
###### getPartitionInfo
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freePartition
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freeAllInPartition
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freeComputer
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freeAllOnComputer
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freeComputerResource
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
###### freeApplicationResources
- CannotVerifyToken
- AcceessAnyDenied
- CannotAcquireToken
#### RM_Client methods updates
##### rmConfigurationUpdate - input parameter changed
**Description:** RM server update the configuration from the DB that were used during RM server start.
**Signature:**
```void rmConfigurationUpdate ( const std::string& partition );```
Input parameter: Partitition in frame of which the update of configuration is activated.
##### rmConfigurationUpdate with signature `void rmConfigurationUpdate ( Configuration * conf )` - removed
##### requestHWResources -  removed in accorand will be removed in next release. Use freeAllInPartition insteaddance with ATDAQCCRM-4
##### requestHWResourceForProcess -  removed in accordance with ATDAQCCRM-4
##### requestResource with signature `long requestResource( const std::string& partition, const std::string& resource, const std::string& computer, const std::string& clientName, int process_id )` - added new method, see Doxygen documentation for details.
##### freePartition - depricated and will be removed in the next release. Use freeAllInPartition instead.
##### freeAllInPartition  - added new method
The behavior of this method is the same as freePartition, only the name has been changed to be more clear to users.
##### freeComputer - depricated and will be removed in the next release. Use freeAllInPartition instead.
##### freeAllOnComputer  - added new method
The behavior of this method is the same as freePartition, only the name has been changed to be more clear to users.

### RM server updates
#### rmgr_server
Start up without preload config was replaced by mandatory config load since this is required by new initial infrastructure implementation. As the result environment variable TDAQ_RM_CONFIG is not used but new parameter 
parameter added  in **rmgr_server
 -c [ --configdb ] dbname**  The configuration database from which software objects and resources are loaded at startup. Mandatory. RM server reports ERS FATAL error and terminates if the database can not be loaded.

rmgr_server catch all thrown in RM_Server exceptions, report ers::fatal error and return EXIT_FAILURE.
