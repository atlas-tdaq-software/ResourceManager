#include <iostream>
#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>

#include "ers/ers.h"
#include <system/User.h>

static std::string
getMyUserName() {
    ::System::User myname;
    std::string user = myname.name();
    return user;
}

  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;
int main(int argc, char** argv)
{

  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return EXIT_FAILURE;
  }

  std::string rm_server_name("");
  std::string pname;
  std::string binary("");
  std::string application(""); //AI 030624
  std::string computer;
  std::string user( getMyUserName() );
  unsigned long pid(0);
  std::string resource("");
  //bool test(false);
  bool for_process(false);

  try {
    boost::program_options::options_description desc(
      "Request resources for a software object or one resource to run a process on a specified computer. Resources can be requested for a software object or for a process, but not for both at the same time."
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("partition,p",   boost::program_options::value<std::string>(&pname)->required(), "partition within which resources should be requested (mandatory)")
        ("computer,c", boost::program_options::value<std::string>(&computer)->required(), "computer on which the application or process for which resources are being requested is to run (mandatory)")
        ("binary,b", boost::program_options::value<std::string>(&binary)->default_value(binary), "Software object whose resources are being requested")
	("application,a",  boost::program_options::value<std::string>(&application)->default_value(application), "Application for which resources beeing requested")
	("user,u",  boost::program_options::value<std::string>(&user)->default_value(user), "User for whom resources beeing requested. Default value is user who started program")
	("pid,s",  boost::program_options::value<unsigned long>(&pid)->default_value(pid), "Process for which resources beeing requested. Default value 0.")
        ("resource,r", boost::program_options::value<std::string>(&resource)->default_value(resource), "Resource requested for the process")
//        ("test_process,t", "!!! Only for tests!! Request resources for process that will be started by this binary.")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }

      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }
      
  if( !binary.empty() && !resource.empty() ) {
      std::cerr << "It is not possible to request resources for both a software object and one resource for a process. Please specify either parameter \"b\" or parameter \"r\", but not both."<< std::endl;
      return EXIT_FAILURE;
  }
  else if( binary.empty() && resource.empty() ) {
      std::cerr << "Please specify binary or resource"<<std::endl;
      return EXIT_FAILURE;
  }
  else if( !resource.empty() )
      for_process = true;

  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found

  long handleId;
  std::ostringstream txt ;

  if( for_process ) {
//only for tests for some time
    try {
      handleId = RMC->requestResourceForMyProcess( pname.c_str(), resource.c_str(), computer, "test");
      txt<<"Handle ID = " <<  handleId;
      if( handleId > 0 ) {
         daq::rmgr::HandleInfo inf = RMC->getHandleInfo(handleId);
         txt << inf;
      }
      ers::info( ers::Message( ERS_HERE, txt.str()));
    }
    catch ( daq::rmgr::Exception & ex) {
          ers::error(ex);
          return EXIT_FAILURE;
    }
  }
  else
  {
    try {
      	  handleId = RMC->requestResources( pname.c_str(), binary.c_str(),  computer, user, application, pid );
          txt<<"Handle ID = " << handleId;
          ers::info( ers::Message( ERS_HERE, txt.str()));
    }     
    catch ( daq::rmgr::Exception & ex) {
          ers::error(ex);
          return EXIT_FAILURE;
    } 
  }
  delete RMC;
  return EXIT_SUCCESS;
}
