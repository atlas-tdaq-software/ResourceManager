#include <iostream>
#include <fstream> //AI 030624
#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>
#include "ers/ers.h"
  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;

//AI 030624
void updateName( std::string& name, unsigned int newLen ) {
    if( newLen <= name.length() )
       return;
    int dif = (newLen - name.length())/2;
    if(dif == 0) {
        name += " ";
        return;
    }
    std::string addch( dif, ' ');
    name = addch + name + addch;
}

const std::vector<std::string> generateTitle( const daq::rmgr::ConfigUpdate& cinfo ) {
   std::string names[] = {" SWObject", " Resource type", " ResourceId", " maxTotal", " maxPP"};
   unsigned int ntit( 5 );
   unsigned int len[ntit];
   for (unsigned int i=0; i<ntit; i++)
      len[i] = names[i].length();

   for( const auto& sw : cinfo.swo ) {
       if(sw._swid.length() > len[0])
           len[0] = sw._swid.length()+1;
   }
   for(const auto& r : cinfo.swr) {
       if(r._rid.length() > len[2])
           len[2] = r._rid.length()+1;
       std::ostringstream txt;
       txt << r._maxTotal;
       if(txt.str().length() > len[3])
           len[3] = txt.str().length()+1;
       txt.str("");
       txt << r._maxPP;
       if(txt.str().length() > len[4])
           len[4] = txt.str().length()+1;
   }
   for(const auto& r : cinfo.hwr) {
       if(r._rid.length() > len[2])
           len[2] = r._rid.length()+1;
       std::ostringstream txt;
       txt << r._maxTotal;
       if(txt.str().length() > len[3]+1)
           len[3] = txt.str().length();
       txt.str("");
       txt << r._maxPP;
       if(txt.str().length() > len[4]+1)
           len[4] = txt.str().length();
   }
   for (unsigned int i=0; i<ntit; i++) {
      if( len[i] > names[i].length() )
         updateName( names[i], len[i] );
   }
   std::vector<std::string> retv;
   for (unsigned int i=0; i<ntit; i++)
      retv.push_back(  names[i] );
   return retv;
}

void allignRight( bool right, const std::string val, std::ostringstream& txt, int& lenCurTit, const std::vector<std::string>& titles, int index) {
  std::string fill(40,' ');
  lenCurTit += titles[index].length();
  int diff = lenCurTit - txt.str().length() - val.length();
  if( !right )
      txt << val;
  if( diff > 0 )
     txt << fill.substr(0,diff);
  if( right )
     txt << val;
}


void allignPrint( std::ostringstream& txt, int& lenCurTit, const std::vector<std::string>& titles, int index) {
  std::string fill(40,' ');
  lenCurTit += titles[index].length();
  int diff = lenCurTit - txt.str().length();
  if( diff > 0 )
     txt << fill.substr(0,diff);
}

void printSWR( const daq::rmgr::SWResource& swr,  const std::vector<std::string>& titles ) {
  std::ostringstream txt;
  txt << " Software      " << swr._rid;
  int lenCurTit = titles[1].length();
  allignPrint( txt, lenCurTit, titles, 2);

  std::ostringstream oval;
  oval << swr._maxTotal;
  allignRight( true, oval.str(), txt, lenCurTit, titles, 3);
  
  oval.str("");
  oval << swr._maxPP;
  allignRight( true, oval.str(), txt, lenCurTit, titles, 4);
  std::cout<< txt.str() << std::endl;
}
void printHWR( const daq::rmgr::HWResource& hwr, const std::vector<std::string>& titles ) {
  //TODO template
  std::ostringstream txt;
  txt << " Hardware      " << hwr._rid; 
  int lenCurTit =titles[1].length();
  allignPrint( txt, lenCurTit, titles, 2);

  std::ostringstream oval;
  oval << hwr._maxTotal;
  allignRight( true, oval.str(), txt, lenCurTit, titles, 3);

  oval.str("");
  oval << hwr._maxPP;
  allignRight( true, oval.str(), txt, lenCurTit, titles, 4);
  std::cout<< txt.str() <<std::endl;
}
 
void printSWObjects( const daq::rmgr::ConfigUpdate& cinfo, const std::vector<std::string>& titles ) {
   unsigned int tableLen = 0;
   for( const auto& str : titles )
     tableLen += str.length();
   
  std::string lsep(tableLen,'-');
  std::cout << "   Software object list" << std::endl;
  //table head
  std::cout << lsep << std::endl;
  for( const auto& tt : titles )
      std::cout << tt;
  std::cout << std::endl << lsep << std::endl;
  //table body
  std::ostringstream ts;
  int lenCurTit;
  for( const auto& sw : cinfo.swo ) {
     ts << sw._swid; // << " ";
     lenCurTit = 0;
     allignPrint( ts, lenCurTit, titles, 0);
     bool found;
     for( const auto& rid : sw._rlist ) {
	found = false;
        for(const auto& r : cinfo.swr) {
	   if( r._rid == rid ) {
              found = true;
	      std::cout<< ts.str();
	      printSWR( r, titles );
	      continue;
	   }
	}
	if( !found )
          for(const auto& r : cinfo.hwr) {
            if( r._rid == rid ) {
              found = true;
	      std::cout<< ts.str();
              printHWR( r, titles );
              continue;
            }
	  }
	if( !found )
	   std::cout << "               " << rid << std::endl;
     }
     ts.str("");
  }  
}

void printResources( const daq::rmgr::ConfigUpdate& cinfo, const std::vector<std::string>& titles ) {
  unsigned int tableLen = 0;
  for( unsigned int i=1; i<titles.size(); i++ )
     tableLen += titles[i].length();
  std::string lsep(tableLen,'-');
  std::cout << "   Resources list" << std::endl;
  //table head
  std::cout << lsep << std::endl;
  for(  unsigned int i=1; i<titles.size(); i++  )
      std::cout << titles[i];
  std::cout << std::endl << lsep << std::endl;
  //body SWR
  for(const auto& r : cinfo.swr)
     printSWR( r, titles );
  //body HWR
  for(const auto& r : cinfo.hwr)
    printHWR( r, titles );
}

void printConfigInfo( const daq::rmgr::ConfigUpdate& cinfo ) {
   const std::vector<std::string> titles = generateTitle(cinfo);
   printSWObjects( cinfo, titles );
   std::cout << std::endl;
   printResources( cinfo, titles );
}

int main(int argc, char** argv)
{
  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return EXIT_FAILURE;
  }

  std::string rm_server_name("");

  try {
    boost::program_options::options_description desc(
      "Get information about configuration RM resources and software objects loaded into the Resource Manager"
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }

      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }


  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found

  std::ostringstream txt ;
  try {
       daq::rmgr::ConfigUpdate res = RMC->getConfigInfo();
       printConfigInfo( res );
  }
  catch (daq::rmgr::Exception & ex) {
           std::cerr << "Can not get configurtion info. Failed with reason: "<< ex.what() << std::endl;
           return EXIT_FAILURE;
  }

  delete RMC; 
  return EXIT_SUCCESS;
}
