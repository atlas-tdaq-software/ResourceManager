#include <cstdio>
#include <iostream>
#include <ResourceManager/RM_Client.h>

int main()
//(int argc, char **argv)
{
  RM_Client RMC;
  char * clientname, *application, *rnlist;
  CONFPART *confpart;
  RESOURCE* rcp;
   RMERROR *errpub;
  TokenID token,tid;
////  Tokenstatus stat;
////  char* strstat;
////  char* strinfo;
////  RMinfo tmpinfo;

  token = 0;

// Initialize confpart with values we want to use in future
// Warning: confpart should be deleted after use 
//("new" used in  InitConfPartition)InitConfPartitionLight
confpart = InitConfPartitionLight("Configuration1","Partition1");

  //+============================================================
//  Let us load "Partition1" into RMDDB 
//      (oksschema, oksdata and configobj on default)
  errpub = new RMERROR;
  stat= RMC.LoadPartitResources(confpart,   errpub);
  std::cout << "Load partition resource status = ";
// AI 300699 strTokenstatus: interface was changed
//  strstat = strTokenstatus(stat);
//  std::cout  << strstat << std::endl;
//  delete strstat; 
  std::cout  << strTokenstatus(stat)<< std::endl;
  errpub->pterr();  // error interpretation printout
// RMERROR object should be freing after use in any RM interface operations
  delete errpub;
//  delete confpart; 
//  //+============================================================
// Let us see what inside of "Partition1" now (for all configurations, 
//     resources, clientnames, applications and tokens)

// Initialize rcp with values we want to use in call to RM interface
// Warning: rcp should be deleted after use 
//("new" used in  InitRESOURCE)  
// AI131299 InitRESOURCElight instead of InitRESOURCE
//  rcp = InitRESOURCE("*","Partition1","*");
  rcp = InitRESOURCElight("*","Partition1","*");
  errpub = new RMERROR;
  strinfo = RMC.GetResourceInfo (0,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//  delete  strinfo;
  delete rcp;


  //+============================================================    
  clientname= (char *) "clientname1";
  application= (char *) "Application1";
  rnlist= (char *) "";

  //+============================================================
// Try to allocate 3 times the same resources for the same application
 for (int i=0; i < 3; i++){
   errpub = new RMERROR;   
  token=RMC.AskResources(clientname, confpart, application, 1, rnlist, 
                           errpub);
  std::cout << "AskResources operation tokenID = " << token << std::endl;
  std::cout << "Token status is ";
// AI 300699 strTokenstatus: interface was changed
//  strstat = strTokenstatus(RMC.CheckValidity(application,token));
//  std::cout <<  strstat  << std::endl; 
//  delete strstat; 
  std::cout << strTokenstatus(RMC.CheckValidity(application,token)) << std::endl; 
  if (errpub->ierr)
    errpub->pterr();
  else
    errpub->ptmess();    
  delete errpub;
 }
  //+============================================================

  //+============================================================
// Get token info for all that was allocated in "Partition1"
   errpub = new RMERROR; 
// AI131299 InitRESOURCElight instead of InitRESOURCE
//   rcp = InitRESOURCE("*","Partition1","*");
   rcp = InitRESOURCElight("*","Partition1","*");
  strinfo = RMC.GetTokenInfo (0,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//  delete  strinfo;
  delete rcp;   
  //+============================================================

  //+============================================================
 if (token)
   tid=token;
 else 
    // just to add to firsrt token if last AskResources result was UNSUCCESS
   tid=1;  
////// addlist = "Resource6;Resource8";
  errpub = new RMERROR;  
  token=RMC.AskAddResToTID(clientname, "Resource6;Resource8", tid,errpub);
  std::cout << "AskAddResToTID  operation tokenID = " << token << std::endl;
  if (errpub->ierr)
    errpub->pterr();
  else
    errpub->ptmess();    
  delete errpub;
  //+============================================================

  //+============================================================
  if (tid){  // resources where added
// Get token info only for token ID = tid
// AI131299 InitRESOURCElight instead of InitRESOURCE
//  rcp = InitRESOURCE("*","*","*");
  rcp = InitRESOURCElight("*","*","*");
   errpub = new RMERROR; 
  strinfo = RMC.GetTokenInfo (tid,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//   delete  strinfo;
  delete rcp; 
  }  
  //+============================================================

  //+============================================================
// Let us free just one resource "Resource1" for tokenID=1
   errpub = new RMERROR; 
   stat = RMC.FreeMyResource(1, "Resource1",  errpub);
  std::cout << "FreeMyResource operation result status = ";
// AI 300699 strTokenstatus: interface was changed
//  strstat = strTokenstatus(stat);
//  std::cout  << strstat << std::endl;
  std::cout  << strTokenstatus(stat)<< std::endl;
  errpub->pterr();
//  delete strstat;
  delete errpub;   

  //+============================================================
// Get token info for all that was allocated for tokenID=1
// AI131299 InitRESOURCElight instead of InitRESOURCE
//   rcp = InitRESOURCE("*","*","*");
  rcp = InitRESOURCElight("*","*","*");
  errpub = new RMERROR; 
  strinfo = RMC.GetTokenInfo (1,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//  delete  strinfo;
  delete rcp;  

  //+============================================================
// Now let us free all in "Partition1", use previouse rcp value
// AI131299 InitRESOURCElight instead of InitRESOURCE
//  rcp = InitRESOURCE("*","Partition1","*");
  rcp = InitRESOURCElight("*","Partition1","*");
  errpub = new RMERROR; 
  stat = RMC.FreeResourcesByName(rcp,"clientname1","*",   errpub);
  std::cout << "FreeMyResource operation result status = ";
// AI 300699 strTokenstatus: interface was changed
//  strstat = strTokenstatus(stat);
//  std::cout  << strstat << std::endl;
//  delete strstat;   
  std::cout  << strTokenstatus(stat) << std::endl;
  errpub->pterr();
  delete errpub; 
 
  //+============================================================
// And to see that all are freing now in Partition1":
// Get token info for all that was allocated in "Partition1"
  errpub = new RMERROR; 
  strinfo = RMC.GetTokenInfo (0,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//  delete  strinfo;
  delete rcp;   
  //+============================================================

  delete confpart;

  //+============================================================
//  The same for "Partition2"
  confpart = InitConfPartitionLight("Configuration1","Partition2");
  errpub = new RMERROR;
  stat= RMC.LoadPartitResources(confpart,   errpub,"SW_Resource");
  std::cout << "Load partition resource status = ";
  std::cout  << strTokenstatus(stat) << std::endl;
  errpub->pterr();  // error interpretation printout
  delete errpub;
  delete confpart; 

//+============================================================
// Let us see what inside of "Partition2" now (for all configurations, 
//     resources, clientnames, applications and tokens)

// Initialize rcp with values we want to use in call to RM interface
//("new" used in  InitRESOURCE)  
// AI131299 InitRESOURCElight instead of InitRESOURCE
//  rcp = InitRESOURCE("*","Partition1","*");
  rcp = InitRESOURCElight("*","Partition1","*");
  errpub = new RMERROR; 
  strinfo = RMC.GetResourceInfo (0,rcp,"*","*",  errpub);
  tmpinfo = strinfo;
  tmpinfo.Info_res_token_pt(); // printout info in convinient view
  delete errpub;
//  delete  strinfo;
  delete rcp; 

}

