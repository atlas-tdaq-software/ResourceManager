#include <stdio.h>
#include <iostream>
////#include <cmdl/cmdargs.h>
#include <boost/program_options.hpp>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>

  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;

const std::string getAsStr( const std::string headMess, const std::vector<std::string>& list ) {
   if( list.size() == 0)
	   return "Partitions list is empty \n.";
   std::ostringstream txt;
   txt << headMess << std::endl;
   for( auto& i : list )
      txt << i << std::endl;
   //txt <<std::endl;
   return txt.str();
}

int main(int argc, char** argv)
{
/*
 	// Declare arguments
 	CmdArgStr  rm_server_name('n', "rm_server", "rm_server_name", 
			  "resource manager server name; use only for tests.");
	// Declare command object and its argument-iterator
 	CmdLine cmd(*argv, &rm_server_name, NULL);
 	CmdArgvIter arg_iter(--argc, ++argv);
        cmd.description( "Show list all partitions that were registered in the Resource Manager. Information about each partition is enclosed in parentheses.\n Partition unique identifier and configuration DB name separated by semicolomn is shown for each partition.");
       // Parse arguments
	cmd.parse(arg_iter);
*/
  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     std::cerr << "Can not get list of partitions. Failed with reason: "<< ex.what() << std::endl;
     return EXIT_FAILURE;
  }  
  
  std::string rm_server_name("");

  try {
    boost::program_options::options_description desc(
      "Show a list of all partitions that have been registered with the Resource Manager. Information about each partition is enclosed in parentheses.\n For each partition, a unique partition ID and configuration database name are displayed, separated by a semicolon."
      "\n"
      "Available options are:");

    desc.add_options()
       ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
       ("help,h", "Print help message");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

     if (vm.count("help")) {
         std::cout << desc << std::endl;
         return EXIT_SUCCESS;
     }

     boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }

  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found
  try {
    std::ostringstream txt ;
    const std::vector<std::string> list = RMC->getPartitionsList();
    txt<< getAsStr("The following partitions are registered in the RM: ", list); // << std::endl;
    //ers::info( ers::Message( ERS_HERE, txt.str()));
    std::cout<<txt.str();
  }
  catch (daq::rmgr::Exception & ex) {
     std::cerr << "Can not get partitions list. Failed with reason: "<< ex.what() << std::endl;
     return EXIT_FAILURE;
  }
 delete RMC;
 return EXIT_SUCCESS;
}
