#include <iostream>
#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>
#include "ers/ers.h"
  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;
int main(int argc, char** argv)
{

  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return EXIT_FAILURE;
  }

  std::string rm_server_name("RM_Server");
  std::string conf_dbname("");

  try {
    boost::program_options::options_description desc(
      "Update configuartion data in the Resource Manager"
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "Resource manager server name, default name is RM_Server")
        ("dbname,d", boost::program_options::value<std::string>(&conf_dbname)->required(), "A configuration database from which software objects and resources are loaded to update this data on the server. Mandatory.")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if( vm.count("help") ) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }

      boost::program_options::notify(vm);
  }
  catch( const std::exception& ex ) {
       std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
       return EXIT_FAILURE;
  }

  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found
  std::ostringstream txt ;

  try {
      RMC->rmConfigurationUpdate( conf_dbname.c_str() );
      txt<<"Update configuration finished. ";
      ers::info( ers::Message( ERS_HERE, txt.str()));
  }
  catch ( daq::rmgr::ConfigurationException& exE) {
        ers::fatal( exE );
        return EXIT_FAILURE;
  }
  catch ( daq::rmgr::Exception & ex) {
          ers::fatal(ex);
          return EXIT_FAILURE;
  }
  delete RMC;
  return EXIT_SUCCESS;
}

