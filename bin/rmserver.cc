#include <boost/program_options.hpp>
#include "src/RM_Server.h"

 daq::rmgr::RM_Server* RMS = (daq::rmgr::RM_Server*)0;

int main(int argc,  char** argv) {

  try {
        std::list< std::pair< std::string, std::string > > opt = IPCCore::extractOptions(argc,argv);
        opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );
        opt.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), std::string("8") ) );
        opt.push_front( std::make_pair( std::string("threadPoolWatchConnection"), std::string("0") ) );
        opt.push_front( std::make_pair( std::string("supportPerThreadTimeOut"), std::string("1") ) );

     IPCCore::init( opt );
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return EXIT_FAILURE;
  }

  std::string server_name("RM_Server");
  std::string conf_dbname("");
  //std::string log_file;
  std::filesystem::path backup("");
  bool doresto(false);
  bool nobck(false);
  bool  sync_file(false);

  try {
    boost::program_options::options_description desc(
      "Start Resource Manager server."
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&server_name)->default_value(server_name), "Resource manager server name, default name is RM_Server")
        ("configdb,c", boost::program_options::value<std::string>(&conf_dbname)->required(), "The configuration database from which software objects and resources are loaded at startup. Mandatory. RM server reports ERS FATAL error and terminates if the database can not be loaded.")
        ("backup,l", boost::program_options::value<std::filesystem::path>(&backup),
//->default_value(backup),
 "Path to the backup file. A backup file is created if this parameter is given and the \"o\" option is missing. In this case, the RM server checks the directory for a backup during startup. The parent directory of the backup must exist or can be created by the RM server and is writable. Otherwise, the RM server reports ERS FATAL error and terminates.")
        ("restore,r", 
// boost::program_options::value<bool>(&doresto)->default_value(doresto), 
                     "true if the RM data must be restored from backup during startup, false otherwise. No recovery is performed even for the restore value true if a backup file  is not available. Server repors ERS FATAL error and terminates if backup or journal file exists but corruptedand restore is failed. ")
	("pmgsync,p", "Create pmg synchronization file at startup")
        ("nobackup,o", "Do not create a backup while the RM server is running. This option is useful when a backup is specified, but should only be used for the restore procedure.")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if( vm.count("help") ) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }
//      if( !vm.count("backup") )
        

      if( vm.count("nobackup") || !vm.count("backup") )
         nobck = true;

      if( vm.count("restore") )
         doresto = true;
      if( vm.count("pmgsync") )
	 sync_file = true;

      boost::program_options::notify(vm);
    }
    catch( const std::exception& ex ) {
       ers::fatal( ers::Message( ERS_HERE,"Failed to parse command line: " + static_cast<const std::string>( ex.what() ) ) );
       return EXIT_FAILURE;
    }

    if( conf_dbname.empty() ) {
       ers::fatal(  ers::Message( ERS_HERE,"rmgr_server: -c conf_dbname required." ) );
       exit( EXIT_FAILURE );
    }

   if(nobck && !doresto)
     backup = "";

  if(nobck && !doresto)
    backup = "";

  try{
    RMS = new daq::rmgr::RM_Server( conf_dbname, server_name.c_str(), backup.string().c_str(), !nobck, doresto );
    RMS->run( sync_file );
    if(RMS)
        delete RMS;
    return EXIT_SUCCESS;
  }
  catch ( const ers::Issue& ex ) {
     ers::fatal(ex);
  }
  return EXIT_FAILURE;
}
