#include <iostream>
#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>
#include "ers/ers.h"
  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;
int main(int argc, char** argv)
{
  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return -1;
  }

  std::string rm_server_name("");
  std::string pname;

  try {
    boost::program_options::options_description desc(
      "Get information if any resource have been allocated for an application or process in given partition. \n The exit code is 0 if there are no resources in partition, \n the exit code is 1 if at least one resource have been allocated for given partition, \n the exit code is-1(255) if it was impossible to connect to the server or failed to parse command line."

      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("partition,p",   boost::program_options::value<std::string>(&pname)->required(), "partition name (mandatory)")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }

      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return -1;
  }


  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return -1;  // server do not found

  std::ostringstream txt ;
  int retv;
  try {
       daq::rmgr::AllocatedInfo res = RMC->getPartitionInfo(pname);
       if(res.list.size() < 1) {
          retv = 0;
          txt<< "No resources.";
       }
       else {
          retv = 1;
          txt << "Partition has resources.";
       }
       ers::info( ers::Message( ERS_HERE, txt.str()));
  }
  catch (daq::rmgr::Exception & ex) {
           ers::error(ex);
           retv = -1;
  }

  delete RMC; 
  return retv;
}
