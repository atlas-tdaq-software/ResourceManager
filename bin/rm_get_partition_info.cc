#include <iostream>
#include <fstream> //AI 030624
#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <ResourceManager/RM_Client.h>
#include "ers/ers.h"
  daq::rmgr::RM_Client* RMC = (daq::rmgr::RM_Client*)0;

//AI 030624
void updateName( std::string& name, unsigned int newLen ) {
    if( newLen <= name.length() )
       return;
    int dif = (newLen - name.length())/2;
    if(dif == 0) {
        name += " ";
        return;
    }
    std::string addch( dif, ' ');
    name = addch + name + addch;
}

unsigned int  updateTitLen( long val, int ilen) {
   unsigned int retv;
   std::ostringstream txt;
   txt << val;
   if( txt.str().length() > ilen )
      retv = txt.str().length()+1;
   else
      retv = ilen;
   return retv;
}
//AI 0ct24
const std::string getTimeAsString( long long lltime ) {
    long time = lltime;
    std::chrono::milliseconds dur(time);
    std::chrono::time_point<std::chrono::system_clock> dt(dur);
    std::time_t timet = std::chrono::system_clock::to_time_t(dt);
    std::ostringstream txt;
    txt << std::asctime(std::localtime(&timet));
    //txt << timet;
    return txt.str();
}
unsigned int  updateTitLenTime( long long ltimeVal, int ilen) {
  const std::string timeStr = getTimeAsString( ltimeVal );
  unsigned int retv = ilen;
  if(timeStr.length() > ilen)
           retv = timeStr.length()+2;
  return retv;
}

const std::vector<std::string> generateTitle( const daq::rmgr::AllocatedInfo& info ) {
  //AllocatedInfo title
  //std::string names[] = {" Handle", " Resource type", " ResourceId", " maxTotal", " maxPP", " SWObject", " Application", " Partition", " User(owner)", " Computer", " PID", " AllocatedTot", " AllocatedPP",
//	 " CreationTime", //AI oct24
//	  " HardwareId" };
//std::string typeStr[] = {" Software      ", " Hardware      "};
  std::string names[] = {" Handle", "   Type   ", " ResId", " maxTot", " maxPP", " SWObject", " Application", " Partition", " User(owner)", " Computer", " PID", " AllocTot", " AllocPP",
         " CreationTime", //AI oct24
          " HardwareId" };
  unsigned int ntit(15); //AI oct24 was 14
  //unsigned int ntit(11); //AI oct24 was 14
  unsigned int len[ntit];
  for (unsigned int i=0; i<ntit; i++)
     len[i] = names[i].length();
  bool allhwObjIdEmty = true;
  for( const auto& ar : info.list ) {
      len[0] = updateTitLen(  ar._handle, len[0] );
      if(ar._resource.length() > len[2])
           len[2] = ar._resource.length()+1;
      len[3] = updateTitLen( ar._maxTotal, len[3] );
      len[4] = updateTitLen( ar._maxPP, len[4] );
      if(ar._swObj.length() > len[5])
	   len[5] = ar._swObj.length()+2;
      if(ar._application.length() > len[6])
		      len[6] = ar._application.length()+2;
      if(ar._partition.length() >= len[7])
	   len[7] = ar._partition.length()+2;
      if(ar._client.length() > len[8])
           len[8] = ar._client.length()+2;
      if(ar._computer.length() > len[9])
           len[9] = ar._computer.length()+2;
      len[10] = updateTitLen( ar._pid, len[10] );
      len[11] = updateTitLen( ar._allocTotal, len[11] );
      len[12] = updateTitLen( ar._allocPP, len[12] );
      len[13] = updateTitLenTime(  ar._allocTime, len[13] ); //AI oct24
      
      if(ar._hwId.length() > len[14])
	   len[14] = ar._hwId.length()+1;
      if( ar._hwId.length() > 0 )
	  allhwObjIdEmty = false;
  }
  if( allhwObjIdEmty )
      --ntit;
  for (unsigned int i=0; i<ntit; i++) {
     if( len[i] > names[i].length() )
        updateName( names[i], len[i] );
  }

  std::vector<std::string> retv;
  for (unsigned int i=0; i<ntit; i++)
      retv.push_back(  names[i] );
  return retv;
}

void allignRight( long  val, std::ostringstream& txt, int& lenCurTit, const std::vector<std::string>& titles, int index) {
  std::ostringstream oval;
  oval << val;
  std::string fill(40,' ');
  lenCurTit += titles[index].length();
  int diff = lenCurTit - txt.str().length() - oval.str().length();
  if( diff >1 )
     txt << fill.substr(0,diff-1);
  txt << val << " ";
}

void allignPrint( std::ostringstream& txt, int& lenCurTit, const std::vector<std::string>& titles, int index, bool isStrNext) {
  std::string fill(40,' ');
  lenCurTit += titles[index].length();
  int diff = lenCurTit - txt.str().length();
  if( diff > 0 )
     txt << fill.substr(0,diff);
  //AI oct24
  if(isStrNext && diff == 0)
     txt << ' ';
}
void allignPrint( std::ostringstream& txt, int& lenCurTit, const std::vector<std::string>& titles, int index) {
	allignPrint(txt, lenCurTit, titles, index, true);
}

void printPartitionInfo( const daq::rmgr::AllocatedInfo& info ) {
   const std::vector<std::string> titles = generateTitle(info);
   //std::string typeStr[] = {" Software      ", " Hardware      "};
   std::string typeStr[] = {" Software ", " Hardware "};
   unsigned int tableLen = 0;
   for( const auto& str : titles )
     tableLen += str.length();

  std::string lsep(tableLen,'-');

  //table head
  std::cout << lsep << std::endl;
  for( const auto& tt : titles )
      std::cout << tt;
  std::cout << std::endl << lsep << std::endl;
  //table body
  std::ostringstream ts;
  int lenCurTit;
  for( const auto& ar : info.list ) {
     lenCurTit = 0;
     allignRight( ar._handle, ts, lenCurTit, titles, 0);
     int j = ar._isHWR ? 1 : 0;
     ts << typeStr[j] << ar._resource;
     lenCurTit += titles[1].length();
     allignPrint( ts, lenCurTit, titles, 2);
    
     allignRight( ar._maxTotal, ts, lenCurTit, titles, 3);
     allignRight( ar._maxPP, ts, lenCurTit, titles, 4);
     ts << ar._swObj;
     allignPrint( ts, lenCurTit, titles, 5);
     ts << ar._application;
     allignPrint( ts, lenCurTit, titles, 6);
     ts << ar._partition;
     allignPrint( ts, lenCurTit, titles, 7);
     ts << ar._client;
     allignPrint( ts, lenCurTit, titles, 8);
     ts << ar._computer;
     allignPrint( ts, lenCurTit, titles, 9, false);
     
     allignRight( ar._pid, ts, lenCurTit, titles, 10);
     allignRight( ar._allocTotal, ts, lenCurTit, titles, 11);
     allignRight( ar._allocPP, ts, lenCurTit, titles, 12);
     //AI oct24
     ts << getTimeAsString(  ar._allocTime );
     allignPrint( ts, lenCurTit, titles, 13), false;
//     const std::string timeStr =  getTimeAsString(  ar._allocTime );
//     std::cout<<"!!!!!!!!!!!!!!!!!!  ar._allocTime="<< ar._allocTime<<"  timeStr="<<timeStr<<std::endl<<std::endl;

     if( titles.size() > 14 ) {
        ts << ar._hwId; // << " ";
        allignPrint( ts, lenCurTit, titles, 14);
     }
     std::cout<< ts.str() <<std::endl;
     ts.str("");
  }

}


int main(int argc, char** argv)
{
  try {
     IPCCore::init(argc, argv);
  }
  catch( daq::ipc::Exception & ex ) {
     ers::fatal( ex );
     return EXIT_FAILURE;
  }

  std::string rm_server_name("");
  std::string pname;

  try {
    boost::program_options::options_description desc(
      "Get  information about granted in partition resources."
      "\n"
      "Available options are:");

      desc.add_options()
        ("name,n", boost::program_options::value<std::string>(&rm_server_name)->default_value(rm_server_name), "resource manager server name, default name is RM_Server")
        ("partition,p",   boost::program_options::value<std::string>(&pname)->required(), "partition name (mandatory)")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help")) {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
      }

      boost::program_options::notify(vm);
  }
  catch (const std::exception& ex) {
     std::cerr << "Failed to parse command line: " << ex.what() << std::endl;
     return EXIT_FAILURE;
  }

  if (rm_server_name.empty())
    RMC = new daq::rmgr::RM_Client();
  else
    RMC = new daq::rmgr::RM_Client(rm_server_name.c_str());
  if (RMC == (daq::rmgr::RM_Client*)0)
     return EXIT_FAILURE;  // server do not found

  std::ostringstream txt ;
  try {
       daq::rmgr::AllocatedInfo res = RMC->getPartitionInfo(pname);
       //txt<< res;
       printPartitionInfo( res );
       return EXIT_SUCCESS;
  }
  catch (daq::rmgr::Exception & ex) {
           std::cerr << "Can not get partition info. Failed with reason: "<< ex.what() << std::endl;
           return EXIT_FAILURE;
  }
  delete RMC; 
  return EXIT_FAILURE;
}
