package daq.rmgr;

public class RMInvalidObjectException extends RMException {
  public RMInvalidObjectException( String mess, Exception cause ) {
    super(  mess, cause);
  }
}

