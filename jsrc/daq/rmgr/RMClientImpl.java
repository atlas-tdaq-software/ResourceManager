package daq.rmgr;

/**
 * This class implements all public API functionality for
 * the Resource manager client.
 * Creation date: (07.03.2005 17:54:58)
 * @author: Igor Aleksandrov
 */
 import java.util.ArrayList;
 import java.util.concurrent.atomic.AtomicReference;
 import java.util.concurrent.atomic.AtomicInteger;

import org.omg.CORBA.Object;
import config.Configuration;
import config.Query;
import dal.*;
import ipc.*;
import ers.Logger;
import ers2idl.*;

import rmgr.*;
import daq.tokens.*;
 
public class RMClientImpl {
   static final String  RMSERVERDEFAULT = "RM_Server";
   final AtomicReference<ResMgr> m_rmdb = new AtomicReference<ResMgr>();
   final String m_serverName;
   final AtomicInteger m_verbose = new AtomicInteger(0);

/**
 * RMClientImpl default constructor.
 */
public RMClientImpl() {
   super();
   m_serverName = RMSERVERDEFAULT;
   try {
     getDDB();
   }
   catch (RMException ex) {
     m_rmdb.set(null);
   }
}
/**
 * Sets verbosity value
 * @param int val
*/
 public void setVerbose(int val) { m_verbose.set( val ); }
/**
 * Gets verbosity value
 * @return int
*/
 public int getVerbose() { return m_verbose.get(); }

/** Constructor to create object to inteact with RM server with name ser_name 
 * @param :serv_name Name of the RM server
 */
public RMClientImpl(String serv_name) {
   super();
   if(serv_name != null && serv_name.length() > 0)
       m_serverName = serv_name;
   else
       m_serverName = RMSERVERDEFAULT;
   try {
     getDDB();
   }
   catch (RMException ex) {
     m_rmdb.set(null);
   }

}

/**
     * Returns the Resource Manager server handle.
     * If the handle is not found, a message is printed at standard output
     * and RM exceptions raised.
*/
 private void getDDB() throws RMInvalidPartitionException, RMInvalidObjectException, RMException 
 {
      ipc.Partition p = new ipc.Partition();
      try {
	  m_rmdb.set( p.lookup(rmgr.ResMgr.class, m_serverName) );
      }
      catch( InvalidPartitionException ex1) {
           throw new RMInvalidPartitionException( ex1.getMessage(), ex1);
      }
      catch( InvalidObjectException ex2) {
           throw new RMInvalidObjectException( "Exception in RM_Client. ", ex2);
      }
      catch(Exception e) {
	  throw new RMException("Can not lookup RM server object", e);
      }
 }

private config.Configuration getConfiguration(String dbname) throws config.ConfigException {
  config.Configuration conf = new config.Configuration(dbname);
  return conf;
}

private int getMyPid() {
    int process_id = 0;

    try {
      process_id = Integer.parseInt( ( new java.io.File("/proc/self")).getCanonicalFile().getName() );
    }
    catch (NumberFormatException e) {
      ers.Logger.error( e );
    }
    catch (java.io.IOException e) {
       ers.Logger.error( e );
    }
    return process_id;
}

/**
 * Requests resource allocation for a requester process. This method automatically 
 * retrieves the process ID from the system and uses it as one of the input 
 * parameters when querying the server. 
 * @return int  Resource Manager handle identifier. This positive value can  
 * be used to free these allocated resources when according process will be 
 * stopped. Throws exception if failed to allocate resources on any mentioned 
 * below reason.
 * @param partition java.lang.String Name of the partition
 * @param resource  java.lang.String RM resource unique ID.
 * @param computerID  java.lang.String Computer name
 * 
 * @exception daq.rmgr.UnavailableResources   There are no enough resources for the application.
 * @exception daq.rmgr.ResourceNotFound Requested resource was not found in configuration.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
*/
public int requestResourceForMyProcess( String partition, String resource,  String computerID )  throws  
              UnavailableResources, ResourceNotFound, RMException              
 {
     String user = System.getProperty("user.name", "unknown");
     return requestResource( partition, resource, computerID, user );     
}

/**
 *  * Requests resource allocation for a process.
 *  @return int  Resource Manager handle identifier. This positive value can
 *  be used to free these allocated resources when according process will be
 *  stopped. Throws exception if failed to allocate resources on any mentioned
 *  below reason.
 *  @param partition java.lang.String Name of the partition
 *  @param resource  java.lang.String RM resource unique ID.
 *  @param computer  java.lang.String Computer name
 *  @param user  java.lang.String User name
 *  @param process_id int process id for which resources are requested
 *  @exception daq.rmgr.UnavailableResources   There are no enough resources for the application.
 *  @exception daq.rmgr.ResourceNotFound Requested resource was not found in configuration.
 *  @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 **/
public int requestResource( String partition, String resource,  String computer, String user)  throws
              UnavailableResources, ResourceNotFound, RMException {
     getDDB();
     int process_id = getMyPid();
     String[] res_lst = new String[1];
     res_lst[0] = resource;
       long tm = System.currentTimeMillis();
       tm = (tm << 5) >> 5;
       int time = (int) tm;
     try {
       return m_rmdb.get().requestResourcesForProcess( partition, res_lst, computer, process_id, user);
     }
     catch( daq.rmgr.UnavailableResourcesE exu ) {
          throw new UnavailableResources( exu );
     }
     catch( daq.rmgr.ResourceNotFoundE exr ) {
          throw new ResourceNotFound( exr );
     }

}


/**
 *  Free resources of the process. Resource that was granted for process with 
 *  the same partition, resource and computer UID will be released in the RM 
 *  and can be used for other users.
 * @param partition java.lang.String Name of the partition
 * @param computerID  java.lang.String Computer name.
 * @param pid int Prosecc id
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public void freeProcessResources( String partition, String computerID, int pid) 
        throws RMException
{
     getDDB();
//     try {
        m_rmdb.get().freeProcessResources( partition, computerID, pid, getCallerInfo() ); //AI 030624 is_log removed
//     }
//     catch( PartitionNotFoundE ex1 ) {
//        throw new PartitionNotFound( ex1 );
//     }
}

//AI oct24
/** [[deprecated("Parameter is_log is not used.")]]
*/
public void freeProcessResources( String partition, String computerID, int pid, boolean is_log)
        throws RMException
{
   freeProcessResources(partition, computerID, pid);
}

/**
 * Requests resources for software object.
 * Creation date: (15.10.2006 18:40:54)
 * @return int  RM handle identifier as positive value.
 * @param partition java.lang.String Name of the partition. Partition where resources to be allocated.
 * @param swobjID java.lang.String Software object identifier. Software object that has association with required resources.
 * @param computerID  java.lang.String Computer name. Host on which application to be started.
 * @param client java.lang.String User that will be the owner of the requested resources. This name is stored in the Resource Manager server as the owner of according resources.
 * @param application  Name of the application for which resources are being requested. In case of default value "" application name is set to $swobjectid@$computer.
 * @param pid ID of the process for which resources are being requested. Default value is 0.
 * @exception daq.rmgr.SoftwareNotFound  Software object was not found in the RM server.
 * @exception daq.rmgr.UnavailableResources   There are no enough resources for the application.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public int requestResources(String partition, String swobjID, String computerID, String client,
		String application, int pid 
		) throws 
              SoftwareNotFound, UnavailableResources
              ,RMException {
       getDDB();
       String user = client == null || client.length() == 0 ? System.getProperty("user.name", "unknown") : client;
       int hndl;
   // int process_id = getMyPid();
    long tm = System.currentTimeMillis();
    tm = (tm << 5) >> 5;
    int time = (int) tm;
    try{
       daq.rmgr.ResOwnerInfo owner = new daq.rmgr.ResOwnerInfo( user, computerID, pid, application, partition);
       //hndl = m_rmdb.get().requestResources (partition, swobjID, computerID, user));
       hndl = m_rmdb.get().requestResources( owner, swobjID );
       if(m_verbose.get() > 0)
           if(hndl <= 0) {
              ers.Logger.debug( 2, "***RMClientImpl.requestResources called with pname = "
                       + partition + ", SW object ID  = " + swobjID + " , Computer ID = " + computerID + " and client name " + client);
           }
       return (int) hndl;
    }
    catch( daq.rmgr.SoftwareNotFoundE exo ) {
       throw new SoftwareNotFound( exo );
    }
    catch( daq.rmgr.UnavailableResourcesE exr) {
       throw new UnavailableResources( exr );
    }
}

/**
 * Requests resources for software object.
 * Creation date: (15.10.2006 18:40:54)
 * @return int  RM handle identifier as positive value.
 * @param partition java.lang.String Name of the partition. Partition where resources to be allocated.
 * @param swobjID java.lang.String Software object identifier. Software object that has association with required resources.
 * @param computerID  java.lang.String Computer name. Host on which application to be started.
 * @param client java.lang.String User that will be the owner of the requested resources. This name is stored in the Resource Manager server as the owner of according resources.
 * @exception daq.rmgr.SoftwareNotFound  Software object was not found in the RM server.
 * @exception daq.rmgr.UnavailableResources   There are no enough resources for the application.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public int requestResources(String partition, String swobjID, String computerID, String client  ) throws
              SoftwareNotFound, UnavailableResources
              ,RMException {
    return requestResources( partition, swobjID, computerID, client, "", 0);
	      
}

/**
 *  Requests resources for software object.
 *  @return int  RM handle identifier as positive value.
 *  @param partition java.lang.String Name of the partition
 *  @param swobjID java.lang.String Software object UID.
 *  @param computerID  java.lang.String Computer object UID.
 *  @exception daq.rmgr.SoftwareNotFound  Software object was not found in the RM server.
 *  @exception daq.rmgr.UnavailableResources   There are no enough resources for the application.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public int requestResources(String partition, String swobjID, String computerID ) throws 
            SoftwareNotFound, UnavailableResources
              ,RMException 
{
      return requestResources(partition, swobjID, computerID, System.getProperty("user.name", "unknown"));
}

/**
 * Requests resources for software object. The same as requestResources(String partition, String swobjID, String computerID) but for computerID just " " as value used. Can be used for binaries when only resources of software resource type linked to binary.
 */
public int requestResources(String partition, String swobjID) throws 
              SoftwareNotFound, UnavailableResources
              , RMException
{
        return requestResources(partition, swobjID, getMyHost() , System.getProperty("user.name", "unknown"));
}


/**
 * Frees resources that were granted with the RM handle.
 * @param int handle  RM handle identifier. 
 * @param boolean test
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 *
 */
public void freeResources(int handle) 
     throws RMException
{
    getDDB();
    if(m_rmdb.get() == null) {
	if(m_verbose.get() > 0)
            ers.Logger.debug(2, "rmdb == null");
    }
////    try {
       m_rmdb.get().freeResources(handle, getCallerInfo());
////    }
////    catch( HandleIdNotFoundE exh ) {
////       throw new HandleIdNotFound( exh );
////    }
}

//AI oct24
/** [[deprecated("Parameter is_log is not used.")]]
*/
public void freeResources(int handle, boolean is_log)
     throws RMException
{
   freeResources(handle);
}

//forIGUI

/** Checks if resource exists in the RM server.
 * @return true if resource exists and false if no. 
 * @param java.lang.String partitionName Name of the partition.
 * @param java.lang.String rname Name of the resource.
 */
public boolean isResourceExists(String partitionName, String rname) {
    try{
        getDDB();
       Allocated_Resource[] test = getResourceInfoE(partitionName, rname); 
       if (test == null || test.length == 0)
	   return false;
       else
           return true;
    }
    catch(RMException ex) { 
       return false;
    }
}

private Handle_Info getHandleInfoE(long handle) throws RMException {
	getDDB();
           return m_rmdb.get().getHandleInfo((int)  handle);
}
/**
 * Gets information about resources that were granted with requested handle indentifier.
 * @return daq.rmgr.HandleInfo  Information about resources that were granted with requested handle indentifier.
 * @param long handle   Handle identifier.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public HandleInfo getHandleInfo(long handle) throws  RMException {
       return getHandleInfo( getHandleInfoE( handle ) );
}

private daq.rmgr.Allocated_Resource[] getPartitionInfoE(String partition) throws 
      AcceessAnyDenied, CannotVerifyToken, RMException
{
	    getDDB();
            if(m_rmdb.get() == null) {
		if(m_verbose.get() > 0) 
		    ers.Logger.debug(2, "RMDB is null: incorrect connection with RM_SEVER" );
	    }
            try {
                return m_rmdb.get().getPartitionInfo(partition, getCallerInfo( true ));
            }
            catch( daq.rmgr.AcceessAnyDeniedE exd ) {
                throw new AcceessAnyDenied( exd );
            }
            catch( daq.rmgr.CannotVerifyTokenE ex ) {
                throw new CannotVerifyToken( ex );
            }
}
/**
 * Gets information about granted resources in the partition.
 * @return daq.rmgr.AllocatedResource[]  Array of granted resources.
 * @param partition java.lang.String partition  Name of the partition. Value "*" means in all partitions.
 * @exception daq.rmgr.AcceessAnyDenied Access denied for user for action getPartitionInfo. User should have excpert role for this action.
 * @exception daq.rmgr.CannotVerifyToken Failed the token verification on server.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public daq.rmgr.AllocatedResource[] getPartitionInfo(String partition) throws AcceessAnyDenied, CannotVerifyToken, RMException {
   return getAllocatedResourceArray( getPartitionInfoE( partition ) );
}

private daq.rmgr.Allocated_Resource[] getResourceInfoE(String partition, String rname) throws  RMException
{
            getDDB();
            if(m_rmdb.get() == null) {
                if(m_verbose.get() > 0)
                    ers.Logger.debug(2, "RMDB is null: incorrect connection with RM_SEVER" );
            }
               return m_rmdb.get().getResourceInfo(partition, rname);
}

/**
 * Gets information about granted resource in the partition.
 * @return  daq.rmgr.AllocatedResource[]  Array of granted resources.
 * @param partition java.lang.String partition  Name of the partition. Value "*" means in all partitions.
 * @param rname java.lang.String  rname Name of the resource. Value "*" means for all resources.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
*/
public daq.rmgr.AllocatedResource[] getResourceInfo(String partition, String rname) throws  RMException
{
     return getAllocatedResourceArray( getResourceInfoE( partition, rname ) );
}

private Allocated_Resource[] getComputerResourceInfoE(String partition, String rname, String computer) 
        throws RMException
{
            getDDB();
            if(m_rmdb.get() == null) {
                if(m_verbose.get() > 0)
                    ers.Logger.debug(1,"RMDB is null: incorrect connection with RM_SEVER" );
            }
            return m_rmdb.get().getComputerResourceInfo(computer,partition, rname);
}
/**
 * Gets information about granted resource in the partition on computer.
 * @return  AllocatedResource[]  Array of  granted resources
 * @param partition java.lang.String partition  Name of the partition. Value "*" means in all partitions.
 * @param rname java.lang.String  rname Name of the resource. Value "*" means for all resources.
 *  @param java.lang.String  Computer for which resources where allocated
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 **/
public AllocatedResource[] getComputerResourceInfo(String partition, String rname, String computer)
        throws RMException
{
    return getAllocatedResourceArray( getComputerResourceInfoE( partition, rname, computer) );
}


/**
 * Gets list of the partitions.
 * @return String[] Array of partitions that were registered in the RM server.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public String[] getPartitionsList() throws RMException {
	getDDB();
	if(m_rmdb.get() == null) {
	  return new String[]{""};
	}
//	if(m_rmdb.get() == null){
//            plist = new  RM_str[0];
//	    return plist;
//        }
	String[] list =  m_rmdb.get().getPartitionsList();
	return list;
}

/**
 *  Gets information about configuration
 *  @return  daq.rmgr.Config_update
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
**/
public Config_update getConfigInfo()  throws RMException {
        getDDB();
        Config_update[] cinfo = null;
        if(m_rmdb.get() == null) {
           return null;   
        }
        return m_rmdb.get().getConfigInfo();
} 


/**
 * Gets list of running RDBs.
 * @return java.util. ArrayList<String> list of strings with running RDBs.
 */
public ArrayList<String> getRDBList() throws RMInvalidPartitionException {
    ArrayList<String> rdbList = new  ArrayList<String>();

   try {
   PartitionEnumeration pit = new PartitionEnumeration();
   while ( pit.hasMoreElements() ){
       ipc.Partition p = pit.nextElement();
       TypeEnumeration tit = new TypeEnumeration( p );
       while ( tit.hasMoreElements() ){
	   String type = tit.nextElement();
	   ObjectEnumeration oit = new ObjectEnumeration( p, type );
	   if(type.equals("rdb/cursor")){
	       while( oit.hasMoreElements() ){
                   ipc.Element obj = oit.nextElement();
                   String st = "rdbconfig:"+p.getName()+"::"+obj.name;
                   rdbList.add(st);
	       }
	   }
       }
   }
   }
   catch(InvalidPartitionException ex) {
        throw new RMInvalidPartitionException( ex.getMessage(), ex);
   }
   return rdbList;
}
    
/**
 *  Reload on RM server the configuration from the DB that were used during RM server start.
 * @param dbname java.lang.String  Partition in frame of which the update of configuration  in RM server was initiated.
 * @exception daq.rmgr.ConfigurationException Exception due too problems when loads on client, send configuration to server, updates configuration server data be new ones
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 **/

public void rmConfigurationUpdate ( String partition ) throws daq.rmgr.ConfigurationException
         ,RMException
{
    String user = System.getProperty("user.name", "unknown");
    try { 
      getDDB();
      m_rmdb.get().rmConfigUpdate( partition, user );
    }
    catch( daq.rmgr.ConfigExceptionE ex ) {
         throw new daq.rmgr.ConfigurationException( ex );
    } 
}

public String getMyHost() {
   String host;

    try {
        java.net.InetAddress addr = java.net.InetAddress.getLocalHost();
        host = addr.getHostName();
    }
    catch (java.net.UnknownHostException e) {
      host = " ";
    }
  return host;  
}

/**
 * Free all resources in the partition.
 * @return java.lang.String String representation of the action status.
 * @param java.lang.String Name of the partition.
 * @exception daq.rmgr.CannotVerifyToken Failed the token verification on server.
 * @exception daq.rmgr.AcceessDeniedE Access Manager denied for user to free resources
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public void freePartition( String p ) throws AcceessAnyDenied, CannotVerifyToken, RMException
{
   getDDB();
   try {
       m_rmdb.get().freeAllInPartition(  p, getCallerInfo( true ) );
   }
   catch( daq.rmgr.AcceessAnyDeniedE exd ) {
          throw new AcceessAnyDenied( exd );
   }
   catch( daq.rmgr.CannotVerifyTokenE exvt ) {
          throw new CannotVerifyToken( exvt );
   }

}

/**
 * Gets those client names to which according resources were granted.
 * @return java.lang.String[] Array of string with cleint names
 * @param rname java.lang.String  Name of the resource.
 * @param p java.lang.String Name of the partition.
 * @exception RMException  RM server parent exception. The likely cause is a CORBA or ipc exception.
 */
public String[] getResourceOwners( String rname, String pname ) throws RMException
{
    getDDB();
    return  m_rmdb.get().getResourceOwners(rname, pname);
}


/**
 * Checks if RM server running.
 * @return boolean true if RM server is running or false otherwise. 
 */
public boolean rmServerExists() {
   try {
     getDDB();
   }
   catch (RMException ex) {
      ers.Logger.error( ex );
      m_rmdb.set(null);
   }
   return m_rmdb.get() != null;
}

private CallerInfo getCallerInfo() throws RMAcquireTokenException {
  return getCallerInfo( false );
}

private CallerInfo getCallerInfo( boolean useDaqTok ) throws RMAcquireTokenException {
     daq.rmgr.CallerInfo caller = new daq.rmgr.CallerInfo();
     caller.host = getMyHost();
     caller.user = useDaqTok ? getDaqToken() : System.getProperty("user.name", "unknown");
     return caller;
}

private String getDaqToken() throws RMAcquireTokenException {
  if( daq.tokens.JWToken.enabled() )
    try {
      return daq.tokens.JWToken.acquire(daq.tokens.JWToken.MODE.REUSE);
    }
    catch (daq.tokens.AcquireTokenException ex) {
      ers.Logger.error( ex );
      throw new RMAcquireTokenException( ex );//// + ex.getMessage());
    }
  else
    return System.getProperty("user.name", "unknown");
}


private ResInfo getResInfo(daq.rmgr.Res_Inf in ) {
  ResInfo ri = new ResInfo();
  ri.resource = in.resource;
  ri.type = in.type;
  ri.maxPP = in.maxPP;
  ri.maxTotal = in.maxTotal;
  ri.allocPP = in.allocPP;
  ri.allocTotal = in.allocTotal;
  return ri;     
}

private AllocatedResource getAllocatedResource( daq.rmgr.Allocated_Resource in ) {
   AllocatedResource ares = new AllocatedResource();
   ares.handle = in.handle;
   ares.resource = in.resource;
   ares.type = in.type;
   ares.maxPP = in.maxPP;
   ares.maxTotal = in.maxTotal;
   ares.hwClass = in.hwClass;
   ares.swObj = in.swObj;
   ares.application = in.application;
   ares.partition = in.partition;
   ares.client = in.client;
   ares.computer = in.computer;
   ares.pid = in.pid;
   ares.hwId = in.hwId;
   ares.allocPP = in.allocPP;
   ares.allocTotal = in.allocTotal;
   ares.allocTime = in.allocTime;
   return ares;
}

private AllocatedResource[] getAllocatedResourceArray( Allocated_Resource[] in ) {
   AllocatedResource[] retv = new AllocatedResource[ in.length ];
   for( int i=0; i< in.length; i++ )
       retv[i] = getAllocatedResource( in[i]);
   return retv;
}

private HandleInfo getHandleInfo( daq.rmgr.Handle_Info in ) {
   HandleInfo retv = new HandleInfo();
   retv.handle = in.handle;
   retv.swObj = in.swObj;
   retv.application = in.application;
   retv.computer = in.computer;
   retv.partition = in.partition;
   retv.client = in.client;
   retv.pid = in.pid;
   retv.allocTime = in.allocTime;
   retv.resDetails = new ResInfo[ in.resDetails.length ];
   for( int i=0; i< in.resDetails.length; i++)
       retv.resDetails[i] = getResInfo( in.resDetails[i] );
   return retv;
}


}
