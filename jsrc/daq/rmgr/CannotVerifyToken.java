package daq.rmgr;

/**
 *  CannotVerifyToken exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.rmgr.CannotVerifyTokenE;

public class CannotVerifyToken extends RMException {

  public CannotVerifyToken( CannotVerifyTokenE  ex ) {
   // super( "Can not verify token. " + cause.reason, cause);
    super("remote method failed with exception " + daq.rmgr.CannotVerifyTokenEHelper.id() 
//+ " exception"
, ers2idl.Converter.idl2ersIssue(ex.issue));
  }


}

