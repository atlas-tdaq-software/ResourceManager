 package daq.rmgr;

 import java.net.InetAddress;
 import daq.rmgr.*;
 import java.io.*;
 import java.util.Vector;

public class TestRmClient {
  public RMClientImpl client;
  final String[] types = { "SW", "HW" };
/**
 * TestRmClient constructor comment.
 */
public TestRmClient() {
	super();
	client = new RMClientImpl();
}
public TestRmClient(String server_name) {
	super();
	client = new RMClientImpl(server_name);
}

public static void main(String[] argv) {
   System.out.println("main of TestRmClient started");

   TestRmClient tester = null;
   String server_name = null;
   String p_name = null; //mandatory
   String swobjID = null;
   String compID = null;
   String applicationID = null;
   String ddbPath = null;
   String resID = null;
   boolean testFreeAll = false;
   int verbose = 0;

   boolean freeProcByHandle = false;
   boolean iguiShort = false;

   for(int i = 0; i < argv.length; i+=2)
      if ("-n".equalsIgnoreCase(argv[i]))
	       server_name = argv[i+1]; //just for debug and tests
      else if("-p".equalsIgnoreCase(argv[i]))
               p_name = argv[i+1]; //partition to load
      else if("-ddb".equalsIgnoreCase(argv[i]))
               ddbPath =  argv[i+1]; 
      else if("-b".equalsIgnoreCase(argv[i]))
               swobjID = argv[i+1]; // sowftwareID to request resources
      else if("-c".equalsIgnoreCase(argv[i]))
               compID = argv[i+1]; //computer to start on (for sw obj)
      else if("-r".equalsIgnoreCase(argv[i]))
               resID = argv[i+1]; 
      else if("-allfree".equalsIgnoreCase(argv[i])) {
                 testFreeAll = true;
                 i--;
      }
      else if("-short".equalsIgnoreCase(argv[i])) {
                 iguiShort = true;
                 i--;
                System.out.println(" igui methods short test.");
      }
      else if("-v".equalsIgnoreCase(argv[i])) {
                 verbose = 1;
                 i--;
      }
    
   if(server_name == null)
      tester  = new TestRmClient();
   else
      tester  = new TestRmClient(server_name);
   tester.client.setVerbose( verbose );

System.out.println("Start tests");

  boolean isRMexists = tester.client.rmServerExists();
  if(!isRMexists) {
    System.out.println("!!! tester.client.rmServerExist()==false: seems RM server with name \"" + server_name + "\" is not running! Finish java API tests.!");
    return;
  }
  Config_update confInfo = null;
try{
  if(ddbPath != null){
     System.out.println("Current configuration content:");
     confInfo = tester.client.getConfigInfo();
     tester.printConfigContent(confInfo);
     tester.client.rmConfigurationUpdate ( ddbPath );
     System.out.println("Configuration \"" + ddbPath + "\" updated");
     System.out.println("New content:");
     confInfo = tester.client.getConfigInfo();
     tester.printConfigContent(confInfo);
  }
}
catch(Exception exp0) {
  exp0.printStackTrace();
  System.out.println( exp0.getMessage() );
}

HandleInfo hinfo = null;

 if(iguiShort) {
   try{
      if(resID == null)
         resID = "sw_res1";
      if(p_name == null)
         p_name = "testPartition";
     String localHostName = InetAddress.getLocalHost().getCanonicalHostName();
     System.out.println("requestResourceForMyProcess for partition="
        + p_name + "  resID=" +  resID + " compID=" + localHostName);
     int rmHandle = tester.client.requestResourceForMyProcess( p_name, resID, localHostName); 
     System.out.println("Done. Handle=" + rmHandle);
     System.out.println( tester.client.getHandleInfo((long) rmHandle) );
     hinfo = tester.client.getHandleInfo( rmHandle );
     tester.printHandleInfo( hinfo );
     System.out.println(" Print all allocated in partition");
     AllocatedResource[] info1 = tester.client.getPartitionInfo( p_name );
     tester.printAllocatedResources( info1 );
     System.out.println("See owners of resource:");
     String[] owns =  tester.client.getResourceOwners(resID, p_name);
     if(owns != null) {
       for(int j=0;j<owns.length;j++)
          System.out.println("owner[" + j + "]=" + owns[j]);
     } 
     System.out.println(" Now free reosurce by handle=" + rmHandle);
     
     tester.client.freeResources( rmHandle );
     System.out.println("Again info by habdle=" + rmHandle);
     hinfo = tester.client.getHandleInfo( rmHandle );
     tester.printHandleInfo( hinfo );
     System.out.println("See partition");
     info1 = tester.client.getPartitionInfo( p_name );
     tester.printAllocatedResources( info1 );
   }
   catch(Exception exrm) {
            exrm.printStackTrace();
            System.out.println( exrm.getMessage() );
   }
   return;
  }

try{
System.out.println("Check alocate/free. First requestResources( testPartition,Binary2_sw2_fca32");
int handleTest = tester.client.requestResources( "testPartition", "Binary2_sw2_fca32");
System.out.println(" handle=" + handleTest);
 hinfo = tester.client.getHandleInfo( handleTest );
 tester.printHandleInfo( hinfo );
System.out.println("Now free handle " + handleTest);
 tester.client.freeResources( handleTest );
System.out.println("Result:");
 hinfo = tester.client.getHandleInfo( handleTest );
 tester.printHandleInfo( hinfo );
}
catch(Exception expTst) {
//  exp0.printStackTrace();
  System.out.println( expTst.getMessage() );
}

   if(testFreeAll) {
     System.out.println("testFreeAll");
     //if(p_name != null && swobjID != null)
     try{
      //check that have partition
     int handleId = tester.client.requestResources( p_name, swobjID);
     tester.client.freeResources( handleId );
     System.out.println("requestResources for" + p_name + " and " + swobjID + " handleId=" + handleId);
     tester.printAllocatedResources( tester.client.getPartitionInfo(p_name) );
     tester.client.freePartition( p_name );
     String[] partitions = tester.client.getPartitionsList();
     if(partitions.length == 0)
         System.out.println("NO partitions");
     else{
       System.out.println("Partitions:");
       for(int ip=0; ip<partitions.length; ip++)
           System.out.println("{"+partitions[ip]+"}");
     }
     handleId = tester.client.requestResources( p_name, swobjID);
     System.out.println("requestResources for" + p_name + " and " +  swobjID + " handleId=" + handleId);
     partitions = tester.client.getPartitionsList();
     tester.printPartitionList( partitions );
     tester.client.freePartition(  p_name );
     tester.printPartitionList( tester.client.getPartitionsList() );
     return;
    }
    catch (Exception ex) {
      System.out.println(ex.getMessage() );
      return;
    }
   }

//
//test process request/free API
  int hndl0 = -2;
if(swobjID != null)
try{
  System.out.println("requestResources for parition=" 
   + p_name + " swobjID=" + swobjID);
  int hndl = tester.client.requestResources( p_name, swobjID);
  System.out.println("requestResources finished. Handle="+ hndl);
  if(hndl >0) {
    hinfo = tester.client.getHandleInfo( hndl );
    tester.printHandleInfo( hinfo );
  }
}
catch(Exception ex00) {
  ex00.printStackTrace();
  System.out.println( ex00.getMessage() );
}

try {
System.out.println("requestResourceForMyProcess for partition="
    + p_name + "  resID=" +  resID + " compID=" + compID);

 hndl0 = tester.client.requestResourceForMyProcess( p_name, resID, compID );
 System.out.println("Done. Handle=" + hndl0);
//  System.out.println( tester.client.getHandleInfo((long) hndl0) );
 hinfo = tester.client.getHandleInfo( hndl0 );
 tester.printHandleInfo( hinfo );

  System.out.println(" Print all allocated in partition");
  AllocatedResource[] info = tester.client.getPartitionInfo( p_name );
  tester.printAllocatedResources( info );
}
catch(Exception ex) {
 
  ex.printStackTrace();
  System.out.println( ex.getMessage() );
}


try {
    int process_id = 0;

    try {
      process_id = Integer.parseInt( ( new java.io.File("/proc/self")).getCanonicalFile().getName() );
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
    catch (java.io.IOException e) {
      e.printStackTrace();
    }
    if( !freeProcByHandle ) {
        tester.client.freeProcessResources( p_name, compID, process_id);
        System.out.println( "freeProcessResources for  partition =" + p_name
           +  " compID=" + compID + " pid=" + process_id + " done. Get partition info to see results (should not be lines with computer==comp1):");
       AllocatedResource[] info = tester.client.getPartitionInfo( p_name );
       tester.printAllocatedResources( info );
    }
    else {
        System.out.println( "TRY to free resources of process by handle for handle=" + hndl0);
        tester.client.freeResources( hndl0 );
        System.out.println(" Free Resource Done. Check according handle info.");
        hinfo = tester.client.getHandleInfo( hndl0 );
        tester.printHandleInfo( hinfo );        
    }
}
catch(Exception ex2) {
  System.out.println( ex2.getMessage() );
}
///   String hasPartition = p_name != null ? "SUCCESS" : "UNSUCCESS";
///   if(p_name != null && ddbPath != null) {
///      hasPartition =  tester.loadPartition(p_name, ddbPath);
///   }

///   if(hasPartition.equalsIgnoreCase("SUCCESS"))
///     tester.testAllFacilities(p_name, resID, applicationID, compID, swobjID);
}

public void printPartitionList(String[] partitions) {
     if(partitions.length == 0)
         System.out.println("NO partitions");
     else{
       System.out.println("Partitions:");
       for(int ip=0; ip<partitions.length; ip++)
           System.out.println("{"+partitions[ip]+"}");
       }
}


public String loadMyPartition(String p, String ddb) {
  try {
      System.out.println("loadMyPartition start");
      client.rmConfigurationUpdate( ddb ); //  registerMyPartition( p, ddb );
      System.out.println("loadMyPartition successfully  done ");
      return "SUCCESS";
  }
  catch (Exception ex) {
      System.out.println("Exception: " + ex.getMessage());
  }
  catch (Throwable thr) {
       System.out.println("Throwable: " + thr.getMessage());
       thr.printStackTrace();
  }
  return "UNSUCCESS";
}



public String loadPartition(String p, String ddb) {
  try {
      client.rmConfigurationUpdate( ddb ); //   registerMyPartition( p, ddb );
      System.out.println("loadPartition successfully  done ");
      return "SUCCESS";
  }
  catch (Exception ex) {
      System.out.println("Exception: " + ex.getMessage());
  }
  return "UNSUCCESS";
}

public void testAllFacilities(String p, String res, String app, String compId, String swobjId) {
 if(p == null) {
    System.out.println("Can not test ficility due to partition absent in input");
    return;
 }

 if(compId != null && swobjId != null)
   try {
       int handleId = client.requestResources( p, swobjId, compId, "");
       System.out.println("requestResources handleID=" + handleId);
   }
   catch(Exception ex2) {
       System.out.println("Exception in requestResources" + ex2.getMessage());
   }

}

void printConfigContent(  Config_update info ) {
   if( info == null) {
      System.out.println(" Info is null");
      return;
   }
   int i;
    System.out.println("Config update={");
    System.out.println("Sofware resources={");
   for( i=0; i<info.swr_lst.length; i++) {
      System.out.println(" { id=" + info.swr_lst[i].rid + " max_total=" + info.swr_lst[i].max_total + " max_pp=" + info.swr_lst[i].max_pp + " }");
   }
   System.out.println("  }");
   System.out.println("Hardware resources=");
   for( i=0; i<info.hwcr_lst.length; i++) {
       System.out.println(" { id=" + info.hwcr_lst[i].rid + " max_total=" + info.hwcr_lst[i].max_total + " max_pp=" + info.hwcr_lst[i].max_pp + " hwClass=" + info.hwcr_lst[i].hwClass + " }");
   }
   System.out.println("  }");
   System.out.println("Software objects= {");
   for( i=0; i<info.swobj_lst.length; i++) {
      System.out.print(" { swid=" + info.swobj_lst[i].swid + "  Resources={ ");
      for(int j=0; j< info.swobj_lst[i].res_list.length; j++)
           System.out.print(" {" + info.swobj_lst[i].res_list[j] + "}");
      System.out.println( "  }");
   }
   System.out.println( "   }");
   System.out.println("} end of Config_update");
}

void printAllocatedResources( AllocatedResource[] info ) {
    if( info == null) {
       System.out.println("AllocatedResource are empty");
       return;
    }
    System.out.println("AllocatedInfo= ");
    for(int i=0; i<info.length; i++) {
       System.out.println(" {  handle=" + info[i].handle
          + ",  resource=" + info[i].resource
          + ", type=" +  types[info[i].type.value()]
          + ", maxPP=" + info[i].maxPP
          + ", maxTotal=" + info[i].maxTotal
          + ", hwClass=" + info[i].hwClass
          + ", swObj=" + info[i].swObj
	  + ", application=" + info[i].application
          + ", partition=" + info[i].partition
          + ", client=" + info[i].client
          + ", computer=" + info[i].computer
          + ", pid=" + info[i].pid
          + ", hwId=" + info[i].hwId
          + ", allocPP=" + info[i].allocPP
          + ", allocTotal=" + info[i].allocTotal + " }");
    }
}

void printHandleInfo( HandleInfo info ) {
  if(info == null) {
    System.out.println("HandleInfo={ empty }");
    return;
  }
  System.out.println("HandleInfo={ handle=" + info.handle + ", SWObj="
    + info.swObj + "application=" + info.application + " computer=" + info.computer + ", partition="
    + info.partition + ", client=" + info.client
    + ", pid=" + info.pid);
  System.out.println("  Resources:");
  for(int i=0;i<info.resDetails.length; i++)
    System.out.println("  {  resource=" + info.resDetails[i].resource +
         ", type=" + types[info.resDetails[i].type.value()] +
         ",  maxPP=" + info.resDetails[i].maxPP 
       + ", maxTotal=" + info.resDetails[i].maxTotal
       + ", allocPP=" +  info.resDetails[i].allocPP
       + ", allocTotal=" + info.resDetails[i].allocTotal + " }");
  System.out.println(" } end Handle Info");
}

}

