package daq.rmgr;

 import java.net.InetAddress;
 import daq.rmgr.*;
 import java.io.*;
 import java.util.Vector;
 import java.text.SimpleDateFormat;
 import java.util.Date;

public class TestRmClient2 {
  public RMClientImpl client;
  final String[] types = { "SW", "HW" };
/**
 *  * TestRmClient2 constructor comment.
 *   */
public TestRmClient2() {
        super();
        client = new RMClientImpl();
}
public TestRmClient2(String server_name) {
        super();
        client = new RMClientImpl(server_name);
}

public void requestResources(String partition, String swobjID) throws Exception {
    int hndl = client.requestResources( partition, swobjID );
    System.out.println("requestResources for partition " + partition
             + " swobjID=" + swobjID + ". Result handle="+hndl); 
}
public void freePartition( String p ) throws Exception{
   client.freePartition( p );
   System.out.println("freePartition for partition " + p + " done");
}
public void printPartitionInfo(String partition) throws Exception{
   daq.rmgr.AllocatedResource[] arr = client.getPartitionInfo( partition );
   System.out.println(" Allocated resources for partition:");
   System.out.println(" handle,  resource, type, maxPP, maxTotal, hwClass, swObj, pplication, partition, client, computer, pid, hwId, allocPP, allocTotal, allocTime");
   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   for( int i = 0; i<arr.length; i++) {
     Date date = new Date(arr[i].allocTime);
     String myDate = format.format(date);
     System.out.println(arr[i].handle+"  " + arr[i].resource +"  " +  arr[i].type
          +"  " +  arr[i].maxPP+"  " + 
          arr[i].maxTotal+"  " +  arr[i].hwClass+"  " + 
          arr[i].swObj+"  " + arr[i].application+"  " +  arr[i].partition+"  " + 
          arr[i].client+"  " +  arr[i].computer+"  " +  arr[i].pid+"  " +  arr[i].hwId+"  " +  arr[i].allocPP+"  " + 
          arr[i].allocTotal + " " + myDate);  //arr[i].allocTime);
   }
   arr = null;
}
public void freeResources(int handle) throws Exception {
   client.freeResources(handle);
   System.out.println(" freeResources for handle="+handle+" done.");
}
public void printHelp() {
 try{
  System.out.println(" Perform request to the Resource manager server. Parameters:");
  System.out.println(" -a Action(request) to be done. Possible values:");
  System.out.println("     1 - requestResources,  set partition and swobjid in input.");
  System.out.println("     2 - freePartition, set partition in input.");
  System.out.println("     3 - getPartitionInfo, set partition in input.");
  System.out.println("     4 - freeResources, set handle in input");
  System.out.println(" -p partition");
  System.out.println(" -b software object");
  System.out.println(" -hndl  Resource handle, that should be released.");
  System.out.println(" -h  This messages");
 }
 catch(Exception ex) {
    System.out.println(ex.getMessage());
 }
}

public static void main(String[] argv) {
   System.out.println("main of TestRmClient2 started");

   TestRmClient2 tester = new TestRmClient2();
   String server_name = null;
   String p_name = null; //mandatory
   String swobjID = null;
   int handle = 0;
//action(request) to be processed
// 1 - requestResources, set in input partition and swobjid
// 2 - freePartition, set on input partition
// 3 - getPartitionInfo, set on input partition
// 4 - freeResources, set handle on input
   int irequest = 0;

//daq.rmgr.AllocatedResource[] allocArr;

//   int verbose = 0;

   for(int i = 0; i < argv.length; i+=2)
      if ("-n".equalsIgnoreCase(argv[i]))
               server_name = argv[i+1]; //just for debug and tests
      else if("-p".equalsIgnoreCase(argv[i]))
               p_name = argv[i+1]; //partition
      else if("-b".equalsIgnoreCase(argv[i]))
               swobjID = argv[i+1]; // sowftwareID to request resources
      else if("-hndl".equalsIgnoreCase(argv[i]))
               handle =  Integer.parseInt(argv[i+1]); 
      else if("-a".equalsIgnoreCase(argv[i]))
               irequest = Integer.parseInt(argv[i+1]);
      else if("-h".equalsIgnoreCase(argv[i])) 
               i--;
  try{
    switch( irequest) {
      case 1:
        tester.requestResources( p_name, swobjID );
        break;
      case 2:
        tester.freePartition( p_name );
        break;
      case 3:
        tester.printPartitionInfo( p_name );
        break;
      case 4:
        tester.freeResources( handle );
        break;
      case 0:
      default:
System.out.println("irequest=0");
        tester.printHelp();
        break;
    }
  }
  catch(Exception ex) {
    System.out.println(ex.getMessage());
 }
 tester.client = null;

}

}
