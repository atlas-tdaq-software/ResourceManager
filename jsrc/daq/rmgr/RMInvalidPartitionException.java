package daq.rmgr;

public class RMInvalidPartitionException extends RMException {

  public RMInvalidPartitionException( String mess, Exception  cause ) {
    super( mess, cause);
  }

}

