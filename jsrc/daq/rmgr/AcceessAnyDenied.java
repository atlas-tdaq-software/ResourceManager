package daq.rmgr;

/**
 *   AcceessAnyDenied exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.rmgr.AcceessAnyDeniedE;

public class AcceessAnyDenied extends RMException {

  public AcceessAnyDenied( AcceessAnyDeniedE  cause ) {
    super( "Access denied for function " + cause.functionName + "activated on host " 
                + cause.host + " by user " + cause.user, cause);
  }

}

