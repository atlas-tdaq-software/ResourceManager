package daq.rmgr;
/**
 * This class describe resource data.
 * Creation date: (07.06.2023)
 * @author: Igor Alexandrov
 */

public class ResInfo {
        public ResInfo(){}
        public java.lang.String resource = "";
        public daq.rmgr.ResourceType type;
        public int maxPP;
        public int maxTotal;
        public int allocPP;
        public int allocTotal;
        public ResInfo(java.lang.String resource, daq.rmgr.ResourceType type, int maxPP, int maxTotal, int allocPP, int allocTotal) {
           this.resource = resource;
           this.type = type;
           this.maxPP = maxPP;
           this.maxTotal = maxTotal;
           this.allocPP = allocPP;
           this.allocTotal = allocTotal;
        }
        public ResInfo( ResInfo in ) {
            this.resource = in.resource;
            this.type = in.type;
            this.maxPP = in.maxPP;
            this.maxTotal = in.maxTotal;
            this.allocPP = in.allocPP;
            this.allocTotal = in.allocTotal;
        }

}
