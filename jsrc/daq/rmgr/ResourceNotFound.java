package daq.rmgr;

/**
 *   ResourceNotFound exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.rmgr.ResourceNotFoundE;

public class ResourceNotFound extends RMException {

  public ResourceNotFound( ResourceNotFoundE  rid ) {
    super( "Resource did not found in the RM server for id: "   , rid);
  }

}

