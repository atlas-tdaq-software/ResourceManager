package daq.rmgr;

/**
 *  RMAcquireTokenException
 *  exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.tokens.AcquireTokenException;

public class RMAcquireTokenException extends RMException {

  public RMAcquireTokenException( daq.tokens.AcquireTokenException tex ) {
    super( "Can not acquire token in RMClient.", tex);
  }

}

