package daq.rmgr;

/**
 * RMException class thrown 
 * in the Resource manager client.
 * Creation date: (07.06.2023)
 * @author: Igor Alxeandrov
 */

public class RMException extends ers.Issue { //Exception { // ers.Issue {
   public RMException( String mess, Exception cause ) {
       super("Exception in RM_Client. "+mess, cause);
   }
}

