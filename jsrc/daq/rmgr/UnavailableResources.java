package daq.rmgr;

/**
 *  UnavailableResources exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.rmgr.UnavailableResourcesE;

public class UnavailableResources extends RMException {

  public UnavailableResources( UnavailableResourcesE  cause ) {
    super( "Unavailable resources.  " + cause.refused_msg, cause);
  }

}

