package daq.rmgr;

/**
 *   SoftwareNotFound exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */


import daq.rmgr.SoftwareNotFoundE;

public class SoftwareNotFound extends RMException {

  public SoftwareNotFound( SoftwareNotFoundE  swobj ) {
    super( "Software onbject = did not found in the RM server for id: "   , swobj);
  }

}

