package daq.rmgr;

/**
 *  CannotVerifyToken exception thrown
 *  in the Resource manager client.
 *  Creation date: (07.06.2023)
 *  @author: Igor Alxeandrov
 */

import config.Configuration;
import daq.rmgr.ConfigExceptionE;

public class ConfigurationException extends RMException {

  public ConfigurationException( ConfigExceptionE  ex ) {
    super("remote method failed with exception " + daq.rmgr.ConfigExceptionEHelper.id() 
, ers2idl.Converter.idl2ersIssue(ex.issue));
  }

  public ConfigurationException( config.ConfigException ex ) {
    super("RM client load configuration failed with exception ", ex);
  }
}

