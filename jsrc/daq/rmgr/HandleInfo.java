package daq.rmgr;
/**
 * This class describe data relative to resource handle.
 * Creation date: (07.06.2023)
 * @author: Igor Alexandrov
 */

public class HandleInfo {
        public HandleInfo(){}
        public int handle;
        public java.lang.String swObj = "";
	public java.lang.String application;
        public java.lang.String computer = "";
        public java.lang.String partition = "";
        public java.lang.String client = "";
        public int pid;
	public long allocTime;
        public ResInfo[] resDetails;
        public HandleInfo(int handle, java.lang.String swObj, java.lang.String computer, java.lang.String partition, java.lang.String client, int pid, ResInfo[] resDtls) {
                this.handle = handle;
                this.swObj = swObj;
		this.application = "";
                this.computer = computer;
                this.partition = partition;
                this.client = client;
                this.pid = pid;
		this.allocTime = 0;
                this.resDetails = new ResInfo[ resDtls.length ];
                for( int i=0; i< resDtls.length; i++)
                    this.resDetails[i] = new ResInfo( resDtls[i] );
        }
	public HandleInfo(int handle, java.lang.String swObj, java.lang.String application, java.lang.String computer, java.lang.String partition, java.lang.String client, int pid, ResInfo[] resDtls) {
                this.handle = handle;
                this.swObj = swObj;
                this.application = application;
                this.computer = computer;
                this.partition = partition;
                this.client = client;
                this.pid = pid;
		this.allocTime = 0;
                this.resDetails = new ResInfo[ resDtls.length ];
                for( int i=0; i< resDtls.length; i++)
                    this.resDetails[i] = new ResInfo( resDtls[i] );
        }
	public HandleInfo(int handle, java.lang.String swObj, java.lang.String application, java.lang.String computer, java.lang.String partition, java.lang.String client, int pid, long allocTime, ResInfo[] resDtls) {
                this.handle = handle;
                this.swObj = swObj;
                this.application = application;
                this.computer = computer;
                this.partition = partition;
                this.client = client;
                this.pid = pid;
                this.allocTime = allocTime;
                this.resDetails = new ResInfo[ resDtls.length ];
                for( int i=0; i< resDtls.length; i++)
                    this.resDetails[i] = new ResInfo( resDtls[i] );
	}
}
