package daq.rmgr;
/**
 * This class describe allocated resource data.
 * Creation date: (07.06.2023)
 * @author: Igor Alexandrov
 */

public class  AllocatedResource {
        public  AllocatedResource(){}
        public int handle;
        public java.lang.String resource = "";
        public daq.rmgr.ResourceType type;
        public int maxPP;
        public int maxTotal;
        public java.lang.String hwClass = "";
        public java.lang.String swObj = "";
	public java.lang.String application = "";
        public java.lang.String partition = "";
        public java.lang.String client = "";
        public java.lang.String computer = "";
        public int pid;
        public java.lang.String hwId = "";
        public int allocPP;
        public int allocTotal;
	public long allocTime;
	public AllocatedResource(int handle, java.lang.String resource, daq.rmgr.ResourceType type, int maxPP, int maxTotal, java.lang.String hwClass, java.lang.String swObj, java.lang.String application, java.lang.String partition, java.lang.String client, java.lang.String computer, int pid, java.lang.String hwId, int allocPP, int allocTotal, long allocTime)
        {
                this.handle = handle;
                this.resource = resource;
                this.type = type;
                this.maxPP = maxPP;
                this.maxTotal = maxTotal;
                this.hwClass = hwClass;
                this.swObj = swObj;
		this.application = application;
                this.partition = partition;
                this.client = client;
                this.computer = computer;
                this.pid = pid;
                this.hwId = hwId;
                this.allocPP = allocPP;
                this.allocTotal = allocTotal;
		this.allocTime = allocTime;
        }
        public AllocatedResource(int handle, java.lang.String resource, daq.rmgr.ResourceType type, int maxPP, int maxTotal, java.lang.String hwClass, java.lang.String swObj, java.lang.String partition, java.lang.String client, java.lang.String computer, int pid, java.lang.String hwId, int allocPP, int allocTotal, long allocTime)
        {
                        this.handle = handle;
                this.resource = resource;
                this.type = type;
                this.maxPP = maxPP;
                this.maxTotal = maxTotal;
                this.hwClass = hwClass;
                this.swObj = swObj;
                this.application = "";
                this.partition = partition;
                this.client = client;
                this.computer = computer;
                this.pid = pid;
                this.hwId = hwId;
                this.allocPP = allocPP;
                this.allocTotal = allocTotal;
		this.allocTime = allocTime;
        }
}
