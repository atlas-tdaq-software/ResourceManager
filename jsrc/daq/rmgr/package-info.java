/**
 *  RMClientImpl class should be used in order to make requests to RM server.
 *  This class implements all public API functionality for
 *  the Resource manager client.
 *  <p> 
 *  @see daq.rmgr.RMClientImpl
 *  <p>
 */
package daq.rmgr;
