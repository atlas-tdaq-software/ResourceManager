#!/bin/sh
#echo rm_gui $1 $2 start
if [ -n "$1" ]; then
  echo rmgr_test_java_api.sh $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13} ${14}  starts...
else
  $TDAQ_JAVA_HOME/bin/java -classpath $CLASSPATH daq.rmgr.TestRmClient &
  exit
fi
if [ "$1" = "-h" ]; then
   echo "Usage: "
   echo "              -h this message"
   echo "              -n server_name"
   echo "                      request will be to rm_server with this name"
   echo "                       or RM_Server if this parameter is absent"
   echo "              -ddb configuration to be loaded. Update config is produced first if this parameter exists. Example parameter value: oksconfig:../data/daq/pp1.data.xml"
   echo "              -p partition"
   echo "              -b binary (sowtware object)" 
   echo "              -c computer"
   echo "              -short test 3 RM methods that are used in UGUI"
   echo "              -allfree if exists - test different free actions and exit"
   echo "              -v set verbosity"
   exit
fi
$TDAQ_JAVA_HOME/bin/java -classpath $CLASSPATH daq.rmgr.TestRmClient $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13} ${14}
