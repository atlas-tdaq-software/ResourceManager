#!/bin/sh
export TDAQ_IPC_INIT_REF="file:/afs/cern.ch/user/a/alexand/public/ipc/ipc_root.ref"

d=`date '+%F %T'`
echo "Start RM server stop/restart tests on $d"
  count=0
  while true; do 
     count=`expr $count + 1`
     echo start rm_server with $1 
#tmp
#     rm_server -l $1 -r &
rmgr_server -n RM_Server_test -l $1 -r &
     rmpid=$!
#     mykill9="kill -9 $rmserid"
     sleep $2
#     kill -15 $rmserid
     echo "Kill and restart rmserver..."
     kill -9 $rmpid
  done 
