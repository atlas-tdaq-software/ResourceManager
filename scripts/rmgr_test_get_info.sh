#!/bin/sh
# Author Igor Soloviev
  #start infinite loop
  count=0
  while true; do
    d=`date '+%F %T'`
    count=`expr $count + 1`
    echo "test number $count on $d"
    # get list of partitions
    pp=`rmgr_get_partitions_list -n RM_Server_test | grep -v 'The following partitions
are registred in the RM' | sed 's/(//;s/;.*//'`
    res=$?
    if [ $res -ne 0 ] ; then
      echo "ERROR: rmgr_get_partitions_list -n RM_Server_test failed with code $res"
      sleep 1
    else
      for p in $pp ; do
        # get resources of partition
        rmgr_get_partition_info -p $p -d -n RM_Server_test > /tmp/rm_test.$$.out
        num_of_lines=`cat /tmp/rm_test.$$.out | grep 'HANDLE  ID' |
wc -l`
        if [ $num_of_lines -eq 0 ] ; then
          echo "partition $p is empty"
        else
          echo "partition $p contains $num_of_lines RM handles"
        fi
        res=$?
        if [ $res -ne 0 ] ; then
          echo "ERROR: rmgr_get_partition_info -n RM_Server_test -p $p -d failed with
code $res"
          cat /tmp/rm_test.$$.out
        fi
      done
      echo ' '
    fi
  done
