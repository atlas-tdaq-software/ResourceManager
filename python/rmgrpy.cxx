#include <boost/algorithm/string.hpp>
#include <boost/python.hpp>
#include <boost/python/exception_translator.hpp>

#include "ResourceManager/RM_Client.h"
#include "src/RM_ClientImpl.h"

namespace daq   {
namespace rmgr  {
boost::python::list RM_Client_partitions(daq::rmgr::RM_Client & cl) {
         std::vector<std::string> lst =  cl.getPartitionsList();
         boost::python::list retv;
         for( auto& j : lst )
            retv.append(j);
         return retv;
}

boost::python::list RM_Client_owners(daq::rmgr::RM_Client & cl, std::string const& rname,
        std::string const& partition) {
         std::vector<std::string> lst =  cl.getResourceOwners(rname,partition);
         boost::python::list retv;
         for( auto& j : lst )
            retv.append(j);
         return retv;
}

boost::python::list RM_Client_handle(daq::rmgr::RM_Client & cl, long handleId) {
         daq::rmgr::HandleInfo hinfo =  cl.getHandleInfo(handleId);
         boost::python::list retv;
         const std::string types[] = { "SW", "HW" };
         const std::string  tit[] = {"handle","swObj", "computer", "partition", "client", "pid", "type","maxPP","maxTotal","allocPP","allocTotal"};
         boost::python::list titles;
         for(int ii=0; ii<11; ii++)
           titles.append(tit[ii]);
         retv.append( titles );
         int j;
         for( const auto& ri : hinfo._resDetails ) {
           boost::python::list line;
           line.append(hinfo._handle);
           line.append((const std::string) hinfo._swObj);
           line.append((const std::string) hinfo._computer);
           line.append((const std::string) hinfo._partition);
           line.append((const std::string) hinfo._client);
           line.append(hinfo._pid);
           line.append((const std::string) ri._resource);
           j = ri._isHWR ? 1 : 0;
           line.append(types[j]);
           line.append(ri._maxPP);
           line.append(ri._maxTotal);
           line.append(ri._allocPP);
           line.append(ri._allocTotal);
           retv.append(line);
          }
         return retv;
}

boost::python::str RM_Client_handlestr(daq::rmgr::RM_Client & cl, long handleId) {
        daq::rmgr::HandleInfo hinfo =  cl.getHandleInfo(handleId);
        std::ostringstream text;
        text << hinfo;
        return text.str().c_str();
}

boost::python::dict RM_Client_getconf(daq::rmgr::RM_Client & cl) {
////         daq::rmgr::Config_updateHelper* hinfo = cl.getConfigInfo();
         daq::rmgr::ConfigUpdate hinfo = cl.getConfigInfo();
         boost::python::dict dic;
         boost::python::list swrl;
         ////unsigned int i;
         ////for(i=0; i < hinfo->cinfo->swr_lst.length(); i++) {
         for( const auto& ri : hinfo.swr ) {
            boost::python::list swr;
            swr.append( (const std::string) ri._rid); // hinfo->cinfo->swr_lst[i].rid);
            swr.append(  ri._maxTotal ); //hinfo->cinfo->swr_lst[i].max_total);
            swr.append( ri._maxPP ); //hinfo->cinfo->swr_lst[i].max_pp);
            swrl.append(swr);
         }
         
         dic["SW_Resources"] = swrl;
         boost::python::list hwrl;
////         for(i=0; i < hinfo->cinfo->hwcr_lst.length(); i++) {
         for( const auto& hr : hinfo.hwr ) {
            boost::python::list hwr;
            hwr.append( (const std::string) hr._rid); //hinfo->cinfo->hwcr_lst[i].rid);
            hwr.append( hr._maxTotal ); //hinfo->cinfo->hwcr_lst[i].max_total);
            hwr.append( hr._maxPP ); //hinfo->cinfo->hwcr_lst[i].max_pp);
            hwr.append( (const std::string) hr._hwClass); // hinfo->cinfo->hwcr_lst[i].hwClass);
            hwrl.append(hwr);
         }
         dic["HW_Resources"] = hwrl;
         boost::python::list swol;
////         for(i=0; i < hinfo->cinfo->swobj_lst.length();  i++) {
         for( const auto& sw : hinfo.swo ) {
             boost::python::list swo;
             swo.append( (const std::string) sw._swid ); // hinfo->cinfo->swobj_lst[i].swid);
             ////for(unsigned int j=0; j < hinfo->cinfo->swobj_lst[i].res_list.length(); j++)
             for( const auto& rid : sw._rlist )
                swo.append( (const std::string) rid ); // hinfo->cinfo->swobj_lst[i].res_list[j].id);
             swol.append(swo);
         }
         dic["SWObjects"] = swol;
         ////delete hinfo; // oct
         return dic;
}

boost::python::str RM_Client_confstr(daq::rmgr::RM_Client & cl) {
////        daq::rmgr::Config_updateHelper* hinfo 
        daq::rmgr::ConfigUpdate cinfo = cl.getConfigInfo();
        std::ostringstream text;
        text << cinfo;
        ////delete hinfo; // oct
        return text.str().c_str();
}

boost::python::list allocatedToList( daq::rmgr::AllocatedInfo hinfo) {
         const std::string types[] = { "SW", "HW" };
         const std::string tit[] = {"handle","resource","type","maxPP","maxTotal","hwClass","swObj","partition","client","computer","pid","hwId","allocPP","allocTotal"};
         boost::python::list nlst;
         for(int ii=0; ii<14; ii++)
            nlst.append(tit[ii]);
         boost::python::list retvList;
         retvList.append(nlst);
         for( auto& r : hinfo.list ) {
              boost::python::list rlist;
              rlist.append(r._handle);
              rlist.append(r._resource);
              rlist.append(types[ r._isHWR ? 1 : 0 ]);
              rlist.append(r._maxPP);
              rlist.append(r._maxTotal);
              rlist.append(r._hwClass);
              rlist.append(r._swObj);
              rlist.append(r._partition);
              rlist.append(r._client);
              rlist.append(r._computer);
              rlist.append(r._pid);
              rlist.append(r._hwId);
              rlist.append(r._allocPP);
              rlist.append(r._allocTotal);
              retvList.append(rlist);
         }
         return retvList;
}

boost::python::str allocatedToStr( daq::rmgr::AllocatedInfo hinfo) {
         std::ostringstream text;
         text << hinfo;
        return text.str().c_str();
}

boost::python::list RM_Client_pinfo(daq::rmgr::RM_Client & cl, std::string const& partition) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getPartitionInfo( partition );
         boost::python::list lst =  allocatedToList( hinfo );
         return lst;
}

boost::python::str RM_Client_pinfostr(daq::rmgr::RM_Client & cl, std::string const& partition) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getPartitionInfo( partition );
         boost::python::str retv =  allocatedToStr( hinfo );
         return retv;
}

boost::python::list RM_Client_prinfo(daq::rmgr::RM_Client & cl, std::string const& partition, std::string const& rname ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getResourceInfo( partition, rname );
         boost::python::list lst =  allocatedToList( hinfo );
         return lst;
}

boost::python::str RM_Client_prinfostr(daq::rmgr::RM_Client & cl, std::string const& partition, std::string const& rname ) {

         daq::rmgr::AllocatedInfo hinfo =  cl.getResourceInfo( partition, rname );
         boost::python::str retv =  allocatedToStr( hinfo );
         return retv;
}

boost::python::list RM_Client_rinfo(daq::rmgr::RM_Client & cl, std::string const& rname ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getFullResInfo( rname );
         boost::python::list lst =  allocatedToList( hinfo );
         return lst;
}

boost::python::str RM_Client_rinfostr(daq::rmgr::RM_Client & cl, std::string const& rname ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getFullResInfo( rname );
         boost::python::str retv =  allocatedToStr( hinfo );
         return retv;
}

boost::python::list RM_Client_cinfo(daq::rmgr::RM_Client & cl, std::string const& computer ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getComputerInfo( computer );
         boost::python::list lst =  allocatedToList( hinfo );
         return lst;
}

boost::python::str RM_Client_cinfostr(daq::rmgr::RM_Client & cl, std::string const& computer ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getComputerInfo( computer );
         boost::python::str retv =  allocatedToStr( hinfo );
         return retv;
}

boost::python::list RM_Client_cprinfo(daq::rmgr::RM_Client & cl, std::string const& computer, std::string const& partition, std::string const& resource ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getComputerResourceInfo( computer, partition, resource );
         boost::python::list lst =  allocatedToList( hinfo );
         return lst;
}

boost::python::str RM_Client_cprinfostr(daq::rmgr::RM_Client & cl, std::string const& computer, std::string const& partition, std::string const& resource ) {
         daq::rmgr::AllocatedInfo hinfo =  cl.getComputerResourceInfo( computer, partition, resource );
         boost::python::str retv =  allocatedToStr( hinfo );
         return retv;
}


}
}

using namespace boost::python;
namespace {
    struct IPCInitializer {
        IPCInitializer()
        {
            setenv("TDAQ_IPC_CLIENT_INTERCEPTORS", "", 0);
            if(!IPCCore::isInitialised()) {
                static char *argv[] = { 0 };
                int argc = 0;
                IPCCore::init(argc, argv);
            }
        }
    };
}

BOOST_PYTHON_MODULE(librmgrpy)
{
   static IPCInitializer ipc_initialize;
   class_<daq::rmgr::RM_Client> ( "RM_Client" )
     .def( init<char const*> ( args( "sid" ) ) )
     .def( init<>() )
     //AI 030624 .def( "freeResources", static_cast< void (daq::rmgr::RM_Client::*)(long, bool) > ( &daq::rmgr::RM_Client::freeResources ), args( "handleId","log_this_call" ) )
     .def( "freeResources", static_cast< void (daq::rmgr::RM_Client::*)(long) > ( &daq::rmgr::RM_Client::freeResources ), args( "handleId" ) )
     .def( "freePartition", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&) > ( &daq::rmgr::RM_Client::freeAllInPartition ), args( "p" ) )
//rmConfigurationUpdate works ok but commented by Giuseppe request. Will uncomment if user's needs in future
//     .def( "rmConfigurationUpdate", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&) > ( &daq::rmgr::RM_Client::rmConfigurationUpdate), args( "dbname" ) )
//     AI 030624
//     .def( "requestResources", static_cast< long (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,std::string const&) > ( &daq::rmgr::RM_Client::requestResources), args( "partition","swobjectid","computerName","clientName" ) )
     .def( "requestResources", static_cast< long (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,std::string const&,std::string const&,unsigned long) > ( &daq::rmgr::RM_Client::requestResources), args( "partition","swobjectid","computerName","clientName","application","pid" ) )
     .def( "requestResources", static_cast< long (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,unsigned long,std::string const&) > ( &daq::rmgr::RM_Client::requestResources), args( "partition","swobjectid","computerName","mypid","clientName" ) )
     .def( "requestResourceForMyProcess", static_cast< long (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,std::string const&) > ( &daq::rmgr::RM_Client::requestResourceForMyProcess), args( "partition","resource","computer","clientName" ) )
     //AI 030624 .def( "freeComputer", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,bool) > ( &daq::rmgr::RM_Client::freeComputer ), args( "computer","log_this_call" ) )
     .def( "freeComputer", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&) > ( &daq::rmgr::RM_Client::freeComputer ), args( "computer" ) )
     //AI 030624 .def( "freeResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,bool) > ( &daq::rmgr::RM_Client::freeResource ), args( "resource","log_this_call" ) )
     .def( "freeResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&) > ( &daq::rmgr::RM_Client::freeResource ), args( "resource" ) )
     //AI 030624 .def( "freePartitionResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,bool) > ( &daq::rmgr::RM_Client::freePartitionResource), args( "p","resource","log_this_call" ) )
     .def( "freePartitionResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&) > ( &daq::rmgr::RM_Client::freePartitionResource), args( "p","resource" ) )
     //AI 030624 .def( "freeComputerResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,bool) > ( &daq::rmgr::RM_Client::freeComputerResource), args( "resource","computer","p","log_this_call" ) )
     .def( "freeComputerResource", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&) > ( &daq::rmgr::RM_Client::freeComputerResource), args( "resource","computer","p" ) )
     //AI 030624 .def( "freeApplicationResources", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&,bool) > ( &daq::rmgr::RM_Client::freeApplicationResources), args( "p","swObj","computer","log_this_call" ) )
     .def( "freeApplicationResources", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,std::string const&) > ( &daq::rmgr::RM_Client::freeApplicationResources), args( "p","swObj","computer" ) )
     //AI 030624 .def( "freeAllProcessResources", static_cast< void (daq::rmgr::RM_Client::*)(long,std::string const&,std::string const&,unsigned long,bool) > ( &daq::rmgr::RM_Client::freeAllProcessResources), args( "handleId","partition","computer","pid","log_this_call" ) )
     .def( "freeAllProcessResources", static_cast< void (daq::rmgr::RM_Client::*)(long,std::string const&,unsigned long) > ( &daq::rmgr::RM_Client::freeAllProcessResources), args( "handleId","computer","pid" ) )
     //AI 030624 .def( "freeProcessResources", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,unsigned long,bool) > ( &daq::rmgr::RM_Client::freeProcessResources), args( "partition","computer","pid","log_this_call" ) )
     .def( "freeProcessResources", static_cast< void (daq::rmgr::RM_Client::*)(std::string const&,std::string const&,unsigned long) > ( &daq::rmgr::RM_Client::freeProcessResources), args( "partition","computer","pid" ) )
     .def("__partitions__",daq::rmgr::RM_Client_partitions)
     .def("__owners__",daq::rmgr::RM_Client_owners, args( "rname","partition") )
     .def("__handle__", daq::rmgr::RM_Client_handle, args("handleId") )
     .def("__pinfo__", daq::rmgr::RM_Client_pinfo, args("partition") )
     .def("__prinfo__", daq::rmgr::RM_Client_prinfo, args("partition","rname") )
     .def("__rinfo__", daq::rmgr::RM_Client_rinfo, args("rname") )
     .def("__cinfo__", daq::rmgr::RM_Client_cinfo, args("computer") )
     .def("__cprinfo__", daq::rmgr::RM_Client_cprinfo, args("computer","partition","resource") )
     .def("__getconf__", daq::rmgr::RM_Client_getconf)
     .def("__confstr__", daq::rmgr::RM_Client_confstr)
     .def("__handlestr__",daq::rmgr::RM_Client_handlestr)
     .def("__pinfostr__", daq::rmgr::RM_Client_pinfostr, args("partition") )
     .def("__prinfostr__", daq::rmgr::RM_Client_prinfostr, args("partition","rname") )
     .def("__rinfostr__", daq::rmgr::RM_Client_rinfostr, args("rname") )
     .def("__cinfostr__", daq::rmgr::RM_Client_cinfostr, args("computer") )
     .def("__cprinfostr__", daq::rmgr::RM_Client_cprinfostr, args("computer","partition","resource") )
  ;
}
