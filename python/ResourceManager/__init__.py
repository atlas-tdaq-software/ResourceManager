from __future__ import print_function

from librmgrpy import *
# Resource manager initialized and command definitions
#

cl = RM_Client()

def request_res(partition, binary, computer, owner, application, pid=0, isprint=False):
    """Request resources in partition for binary on computer for application and process pid. 
       Input parameters:
            partition - partition name
            binary - binary resources of which should be allocated
            computer - computer name. Resources should be allocated on this computer.
            owner - user that run applciation for which resources are allocated
            application - application for which resources are allocated
           isprint - print message about action if value is True. Default value is False.
       Return value:
           Handle of resoures, just some unique number.
    """
    try:
        if isprint:
            print("Request resources in ", partition, " for binary ", binary, " on computer ", computer, " as owner ", owner)
        else:
            pass

        retv = cl.requestResources(partition,binary,computer,owner,application, pid)
        if isprint:
            print(" Handle=" , retv)
        else:
            pass

        return retv
    except RuntimeError as err:
        print(err)
        return -1

def request_res(partition, binary, computer, owner, isprint=False):
    """Request resources in partition for binary on computer. Owner
       is mostly statistical parameter. 
       Input parameters:
            partition - partition name
            binary - binary resources of which should be allocated
            computer - computer name. Resources should be allocated on this computer.
            owner - name of author of requester
           isprint - print message about action if value is True. Default value is False.
       Return value:
           Handle of resoures, just some unique number.
    """
    try:
        if isprint:
            print("Request resources in ", partition, " for binary ", binary, " on computer ", computer, " as owner ", owner)
        else:
            pass

        retv = cl.requestResources(partition,binary,computer,owner,"unknown", 0)
        if isprint:
            print(" Handle=" , retv)
        else:
            pass

        return retv
    except RuntimeError as err:
        print(err)
        return -1

def request_res_proc(partition, binary, computer, pid, owner, isprint=False):
    """Request resources in partition for binary and pid on computer. Owner
       usualy is the user account name. 
       Input parameters:
            partition - partition name
            binary - binary resources of which should be allocated
            computer - computer name. Resources should be allocated for this computer.
            pid - process identifier. Resources are requested for this process.
            owner - name of author of requester
           isprint - print message about action if value is True. Default value is False.
       Return value:
           Handle of resoures, just some unique number.
    """
    try:
        if isprint:
            print("Request resources for ", partition, " for binary ", binary, " and process ", pid, " on computer ", computer, " as owner ", owner)
        retv = cl.requestResources(partition, binary, computer, pid, owner)
        if isprint:
            print(" Handle=" , retv)
        else:
            pass

        return retv
    except RuntimeError as err:
        print(err)
        return -1

def request_res4my_process(partition, resource, computer, owner, isprint=False):
    """Request resource in partition on computer by owner.
      Input parameters:
            partition - partition name
            resource  - resource ID
            computer - computer name. Resources should be allocated for this computer.
            owner - name of author of requester
           isprint - print message about action if value is True. Default value is False.
       Return value:
           Handle of resoures, just some unique number.
    """
    try:
        if isprint:
            print("Request resource ", resource, " for partition", partition, "for computer ", computer, " by owner ", owner)
        retv = cl.requestResourceForMyProcess(partition, resource, computer, owner)
        if isprint:
            print(" Handle=" , retv)
        else:
            pass

        return retv
    except RuntimeError as err:
        print(err)
        return -1

def free_partition(partition, isprint=False):
    """Free all resources in partition
       Input parameters:
           partition - partition name
           isprint - print message about action if value is True. Default value is False.
    """
    try:
        if isprint:
            print("Free all resources in partition ", partition)
        else:
           pass

        cl.freePartition(partition)
    except RuntimeError as err:
        print(err)

def free_computer(computer, isprint=False):
    """Free all resources that were allocated on computer
       Input parameters:
           computer - computer name
           isprint - print message about action if value is True. Default value is False.
    """
    try:
        if isprint:
            print("Free all resources that is used on computer ", computer)
        else:
            pass

        cl.freeComputer(computer, True)
    except RuntimeError as err:
        print(err)

def free_resources( handle,isprint=False ):
    """Free resources by handle
       Input parameters:
           handle - handle indentifier that was produced during resources allocation
           isprint - print message about action if value is True. Default value is False.
    """
    try:
        if isprint:
            print("Free all resources that belong to hanle ", handle)
        else:
            pass

        cl.freeResources(handle,True)
    except RuntimeError as err:
        print(err)

def free_resource( resource, isprint=False):
    """Free resource with ID $resource
       Input parameters:
           resource - resource identifier
          isprint - print message about action if value is True. Default value is False.
    """
    try:
        if isprint:
            print("Free resource with id=",resource)
        else:
            pass

        cl.freeResource(resource,True)
    except RuntimeError as err:
        print(err)

def partitions_prt():
    """ Print list of partitions that have allocated resource(s)
    """
    print(cl.__partitions__())

def partitions():
    """ Get list of partitions that have allocated resource(s)
        Return value:
            list with partition names
    """
    return cl.__partitions__()

def owners(resource, partition):
    """ Get list of all owners who allocated resource for partition
        Input parameters:
             resource - resource identifier
             partition - name of partition
        Return value:
             list with names of different owners of resource
    """
    return cl.__owners__(resource, partition)

def owners_prt(resource, partition):
    """ Print list of all owners who allocated resource for partition
        Input parameters:
             resource - resource identifier
             partition - name of partition
    """
    print(cl.__owners__(resource, partition))

def handle_info(handle):
    """ Get handle info as dictionary
        Input parameter:
             handle - handle of some allocated resource (some number)
        Return value: List with details of allocated resources that have requested handle. This list has colomns:
['handle', 'swObj', 'computer', 'partition', 'client', 'pid', 'type', 'maxPP', 'maxTotal', 'allocPP', 'allocTotal']
    """
    return cl.__handle__(handle)

def handle_info_str(handle):
    """ Print handle info
        Input parameter:
              handle - handle of some allocated resource (some int number)
    """
    print(cl.__handlestr__(handle))

def partition_info(partition):
    """ Get information about resources that allocated for partition
        Input parameter: 
              partition - partition name
        Return value: list with partition info with colomns:
        ['handle', 'resource', 'type', 'maxPP', 'maxTotal', 'hwClass', 'swObj', 
        'partition', 'client', 'computer', 'pid', 'hwId', 'allocPP', 'allocTotal']
    """
    return  cl.__pinfo__(partition)

def partition_info_str(partition):
    """ Print information about resources that allocated for partition
    """
    print("Resources in partition","'"+partition+"':")
    print(cl.__pinfostr__(partition))

#def show_config():
#    print cl.__getconf__()

def config_str():
    """ Print content of configuration that is currectly used by Resource Manager
    """
    print(cl.__confstr__())

from operator import itemgetter
def partition_info_sorted(partition,colomn,revers=False,isprint=True):
    """ Get list with partition info that sorted according to input parameters.
        Input parameters:
           partition - partition name
           colomn - number of colomn in range 0:len(list)-1. 
                  If range is wrong - last colomn as key for sort will be used.
           revers - order of sort, when false - sort in reverse order
           isprint - print info if value of parametr is True (default is True)
        Return: list with sorted partition info with colomns:
        ['handle', 'resource', 'type', 'maxPP', 'maxTotal', 'hwClass', 'swObj', 
        'partition', 'client', 'computer', 'pid', 'hwId', 'allocPP', 'allocTotal']
    """
    info = partition_info(partition)
    print(info[0])
    info2 = info[1:]
    if colomn < 0 or colomn > len(info2):
       print("Wrong colomn number. Last colom will be used for sort")
       colomn = len(info2) -1

    srt1 = sorted(info2,key=itemgetter(colomn),reverse=revers)
    if isprint:
        for l in srt1:
            print(l)
    else:
        pass
    return info2




