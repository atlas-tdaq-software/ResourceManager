from __future__ import print_function

#from rmgrpy import *
from ResourceManager import *

#for i in dir(): print i.__doc__()
print((free_partition.__doc__))
print("******* Free both partitions before tests**********")
free_partition("part1")
free_partition("part2")
#show_config()
print("____________________________________________________________")
config_str()
print("____________________________________________________________")
print("    Warning: Binary1_hw1_fca33 has resource hw_res1 that contain max_total=1 max_pp=1 !!!!!!!")
request_res("part1","Binary1_hw1_fca33","fca33","test_python1")
request_res("part1","Binary1_hw1_fca33","fca33","test_python1")
handle_info(1)
print("    Warning: Binary1_hw2_fca33 has resource hw_res2 that contains max_total=2 max_pp=1  !!!!!")
request_res("part2","Binary1_hw2_fca33","fca33","test_python2")
request_res("part2","Binary1_hw2_fca33","comp1","test_python")
request_res("part1","Binary1_hw2_fca33","comp1","test_python")
print("    Warning: Binary1_sw1_fca29 has resource sw_res1 that contains max_total=2 max_pp=2  !!!")
request_res("part2","Binary1_sw1_fca29","fca29","test_python2")
print("    Warning: Binary1_sw1_fca30 has resource sw_res2 that contains max_total=4 max_pp=2 !!!!")
request_res("part2","Binary1_sw2_fca30","fca30","test_python2")

partition_info("part2")
print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
partition_info_str("part2")
print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print(partitions())
lst = owners("hw_res2","part1")
lst
partitions_prt()
owners_prt("hw_res2","part2")

print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("See how handle info looks like")
j=1
info1 = partition_info("part2")
print(info1)
if len(info1)>1:
    hndl = info1[1][0]
    print("Print handle info fir handle", hndl)
    handle_info_str(hndl)
else:
    print("Partition part2 is empty()")
from operator import itemgetter
print(info1)
print("==========================================")
print("Info sorted by resource:")
print(info1[0])
info2 = info1[1:]
srt1 = sorted(info2,key=itemgetter(1))
for l in srt1:
  print(l)

partition_info_sorted("part2", 6)
print("Another sort")
partition_info_sorted("part2", 0,True,True)
print("Print info about all partitions")
title = partition_info("part2")[0]
p1 = partition_info_sorted("part1", 0, False, False)
p2 = partition_info_sorted("part2", 0, False, False)
p3 = p1 + p2
srt1 = sorted(p3,key=itemgetter(7),reverse=False)
for l in p3:
   print(l)

print("FINISH tests")
#for 
#for h in  
#dic1 = cl.__handle__(2)
#lst3 = cl.__pinfo__("part1")
#lst3
#lst4 = cl.__pinfo__("part2")
#lst4
#lst5 = cl.__prinfo__("part2","sw_res1")
#lst5

